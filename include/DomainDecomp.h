#ifndef DOMAINDECOMP_H
#define DOMAINDECOMP_H

#include "sys.h"
#include <mpi.h>

enum class DecompOpt {gSizes, pSizes, lRanges, lSizes, comm, nThread, rank, 
                      faces, edges, corners};

enum class Neighbor {star, box};

class DomainDecomp
{
  public:
  DomainDecomp(const int n[3],    const int np[3], const bool isPrds[3], 
               const int nThread, const MPI_Comm comm, const Neighbor shape);

  void set_from_option();

  void setup();

  void config_thread(const int nThreads[3]);
  /* set #threads in each dir */

  bool is_root() const
  { return _rank == 0; }

  int rank() const
  { return _rank; }

  void get_global_size(int sizes[3]) const;
  /* get the global sizses */

  void get_local_start(int starts[3]) const;
  /* get the local start indices (global index) */

  void get_local_size(int sizes[3]) const;

  void get_thread_range(const int tid, int thrdRange[6]) const;
  /* threadrange is using local index */

  void get_info(DecompOpt opt, void* ptVal) const;
  /* return attributes */

  bool is_neighbor_star() const
  { return _shape == Neighbor::star; }

  int  num_thread() const
  { return _nThreads[0]*_nThreads[1]*_nThreads[2]; }

  int num_proc() const
  { return _pSizes[0] * _pSizes[1] * _pSizes[2]; }

  int num_node() const
  { return _pSizes[0] * _pSizes[1] * _pSizes[2] / _pSizesNode[0] \
           / _pSizesNode[1] / _pSizesNode[2]; }

  MPI_Comm comm() const
  { return _comm; }

  int neighbor(const int i) const
  { return _nbrs[i];}

  void debug_view() const;

  private:
  void _set_neighbors();
  void _coords_to_rank(const int coords[3], int* ptRank) const;
  void _rank_to_coords(const int rank, int coords[3]) const;

  private:
  int       _gSizes[3];      // global sizes
  int       _pSizes[3];      // # processes
  int       _pSizesNode[3];  // # proc per node
  bool      _isPrds[3];      // periodic ?
  int       _rank;
  int       _coords[3];
  int       _lRanges[6];     // local ranges, global index
  int       _nbrs[26];
  int       _nThreads[3];    // #threads in each direction
  bool      _isThreadSet;    // if #threads per dir is set
  bool      _isUsingMPICart; // if set coords using mpi_cart
  MPI_Comm  _comm;
  Neighbor  _shape;
};

int decompose_threads_2d(int nThread, int len0, int len1, int*ptNThrd0, int*ptNThrd1);
/*----------------------------------------------------------
Decompose a domain into threads in two directions '0' and '1'
according to the direction's length.

IN:  nthread             -  total # threads
     len0, len1          -  two direction's length
OUT: ptNThrd0, ptNThrd1  -  pointer to two dirs' # threads
----------------------------------------------------------*/

int compute_thread_range(const int range[6], const int nThread, const int threadID,\
                         int threadRange[6]);
/*----------------------------------------------------------
Decompose a domain into threads and set the index range assigned
to the given thread.

IN:  range[6]        -  range of the domain
     nThread         -  total number of threads
     threadID        -  ID of current thread
OUT: threadRange[6]  -  index range of thread (threadID)
----------------------------------------------------------*/

int compute_thread_range(const int range[6], const int nThreads[3], const int threadID,\
                         int threadRange[6]);
/*----------------------------------------------------------
Decompose a domain into threads and set the index range assigned
to the given thread.

IN:  range[6]        -  range of the domain
     nThreads[3]     -  # threads in each direction
     threadID        -  ID of current thread
OUT: threadRange[6]  -  index range of thread (threadID)
----------------------------------------------------------*/

#endif
