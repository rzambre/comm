#ifndef HALOARRAY_H
#define HALOARRAY_H

#include "DomainDecomp.h"

#include <vector>
#include <memory>

enum class HaloShape {star, box};
enum class CommMethod {rmaLock, rmaWeakSync, p2p};

class HaloArray 
{
  public: /*-------- public member functions --------*/
  HaloArray(DomainDecomp& dd, const int nHalo, const CommMethod method);

  ~HaloArray();

  int free_mpi_resouce();

  int get_size(int sizes[3]) const;

  int stride_i() const
  { return (_sizes[1] + 2*_nHalo) * (_sizes[2] + 2*_nHalo); }

  int stride_j() const
  { return _sizes[2] + 2*_nHalo; }

  int num_halo() const
  { return _nHalo; }

  double& operator[](const int ijk)
  { return f[ijk]; }

  void get_local_range(int rngs[6]) const;
  /* index start from 0 at the beginning of halo */

  int get_thread_range(const int tid, int tRngs[6]) const;
  /* using nthread from dd, get thread tid's range */

  int get_thread_range(const int nThread, const int tid, int tRngs[6]);
  /* Input #thread, decompose local domain and return thread tid' range */

  void get_thread_range_dh(const int tid, const int nh, int tRngs[6]);
  /* Input #halo, decompose local domain, add halo and return thread tid' range */

  int get_halo_range(const int i, int rngs[6]) const ;
  /* get face(i:0-5), edge(i:6-17), corner(i:18-25) halo ranges */

  int pack_halo();
  /* call specific pack function regarding neighbor type */

  int unpack_halo();
  /* call specific unpack function regarding neighbor type */

  int pack_face_halo();

  int unpack_face_halo();

  int update_halo_begin();

  int update_halo_end();

  int update_face_halo();
  int update_face_halo_begin();
  int update_face_halo_end();

  int debug_view();

  private:
  void _set_face_halo_range_seg(const int tid);
  void _pack_face_halo_seg();
  void _unpack_face_halo_seg();

  public: /*-------- public arrtributes      --------*/
  double* f;

  private:
  DomainDecomp*                 _ptDd;
  int                           _nHalo;    // number of halo
  int                           _sizes[3]; // local sizes
  std::vector<MPI_Request>      _reqs; 
  double                        *_sBuf, *_rBuf;
  CommMethod                    _method;
  HaloShape                     _shape;
  std::vector<std::vector<int>> _faceRngs, _faceHaloRngs; 
  std::vector<int>              _bufStarts;
};

#endif
