#ifndef JACOBI_H
#define JACOBI_H

#include "Smoother.h"
#include "HaloArray.h"

class Jacobi: public Smoother
{
  public:
  Jacobi(const DomainDecomp& dd, const UniMesh& mesh):
    Smoother(dd, mesh)
  {};

  private:
  int _smooth_bsp_mpi(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
                      HaloArray& b, HaloArray& p);

  int _smooth_bsp_hybrid(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
                         HaloArray& b, HaloArray& p);

  int _smooth_funneled(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
                       HaloArray& b, HaloArray& p);
};

int _smooth_range(const int rngs[6], const double h, HaloArray& rhox, \
                  HaloArray& rhoy, HaloArray& rhoz, HaloArray& b, HaloArray& p);
/*----------------------------------------------------------
 smooth vsp

IN:  rngs - [6], range in local domain, start from 0
     c    - red or black
     h    - spacial step
     b, rhox, rhoy, rhoz - rhs and variable coefs
OUT: p    - unkown to be solved
----------------------------------------------------------*/

int _smooth_range(const int rngs[6], const double h, HaloArray& b, HaloArray& p);
/*----------------------------------------------------------
 smooth ccp

IN:  rngs - [6], range in local domain, start from 0
     c    - red or black
     h    - spacial step
     b    - rhs
OUT: p    - unkown to be solved
----------------------------------------------------------*/


#endif
