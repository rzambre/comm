#ifndef UTILS_H
#define UTILS_H

void mpi_busy_wait(double time);
void omp_busy_wait(double time);

#endif
