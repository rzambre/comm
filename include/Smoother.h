#ifndef SMOOTHER_H
#define SMOOTHER_H

#include "DomainDecomp.h"
#include "UniMesh.h"
#include "HaloArray.h"
#include "HaloTransfer.h"
#include "Timer.h"
#include <vector>

#ifdef MIRA_CETUS
  #include "rdtsc.h"
  #define mpi_time() rdtsc()/1.6e9
  #define omp_time() rdtsc()/1.6e9
#else
  #define mpi_time() MPI_Wtime()
  #define omp_time() omp_get_wtime()
#endif

using std::vector;

enum class ParallelModel {BSP_MPI, BSP_Hybrid, Overlap, Fused, WaveFront, \
  WaveFrontOverlap, Pipeline, WaveFrontPipeline};

const int SMOOTHER_N_TIME = 16;
enum SmootherTime {Comp, Comm, Inner, Outer, Pack, Unpack, Total};

class Smoother
{
  public:/*---------------- public member function ----------------*/
  Smoother(const DomainDecomp& dd, const UniMesh& mesh);

  void set_from_option();

  int setup_overlap_range(const int rngs[6]);

  int setup_funneled_outer_range();
  /*----------------------------------------------------------
  Decompose the outer range among threads

  Pre: outer range must already be set
  ----------------------------------------------------------*/

  int setup_funneled_inner_range();
  /*----------------------------------------------------------
  Decompose the inner range among threads

  Pre: inner range must already be set
  ----------------------------------------------------------*/

  int setup();

  int setup_pipeline(int nPipe);

  int set_parallel_model(const ParallelModel model)
  { _model  = model; return 0; }

  int smooth(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
             HaloArray& b, HaloArray& p);
  /*----------------------------------------------------------
  Smooth vcp, choose smoother inside based on model

  IN:  b, rhox, rhoy, rhoz
  OUT: p
  ----------------------------------------------------------*/

  int debug_view();

  virtual ~Smoother();

  int clear_history();

  private:
  virtual int _smooth_bsp_mpi(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
                              HaloArray& b, HaloArray& p) =0;
  /*----------------------------------------------------------
  bsp model with flat mpi
  > computation 
  > communication
  ----------------------------------------------------------*/
  virtual int _smooth_bsp_hybrid(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
                                 HaloArray& b, HaloArray& p) =0;
  /*----------------------------------------------------------
  bsp model with mpi+openmp
  > all threads do computation 
  > master thread communication, all other threads idle
  ----------------------------------------------------------*/
  virtual int _smooth_funneled(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
                               HaloArray& b, HaloArray& p) =0;
  /*----------------------------------------------------------
  funnededl model with mpi+openmp
  > all threads do outer computation
  > master do communicaiton, others do inner computation
  ----------------------------------------------------------*/

  virtual int _smooth_fused(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
                            HaloArray& b, HaloArray& p) =0;
  /*----------------------------------------------------------
  fused model with mpi+openmp
  > all threads to computation including deep halo layers
  > master do communicaiton
  ----------------------------------------------------------*/
  virtual int _smooth_wavefront(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
                                HaloArray& b, HaloArray& p) =0;
  /*----------------------------------------------------------
  wavefront model with mpi+openmp, based on fused
  The computation is blocked with wavefront.
  ----------------------------------------------------------*/

  public:/*---------------- public attributes ----------------*/
  int nMargin;      // # outer layer in overlap

  protected:/*---------------- protected attributes ----------------*/
  const DomainDecomp*  _ptDd;
  const UniMesh*       _ptMesh;
  ParallelModel        _model;  
  int                  _inRngs[6];     // inner domain range
  int                  _outRngs[6][6]; // outer domain range
  vector<vector<int>>  _thrdOutRngs;   // thread's outer range
  vector<vector<int>>  _thrdInRngs;    // thread's outer range
  HaloTransfer         _ht;
  Timer                _timer;
  bool                 _isSyncComm;    // put barrier before comm ?
  std::string          _fTime;         // times' file name
  double**             _t;
  int                  _nPipe;
  HaloTransfer*        _hts;
  double*              _buf;
  int                  _cbSizes[3];
};

#endif
