#ifndef SOLVER_H
#define SOLVER_H

#include "DomainDecomp.h"
#include "UniMesh.h"
#include "HaloStore.h"
#include "HaloTransfer.h"
#include "Timer.h"
#include <algorithm>

using std::vector;

enum SolverTime {Comp, Comm, Inner, Outer, Pack, Unpack, Total};
enum SolverMode {MPI_BSP, Funneled, Overlap, Pipeline, DeepHalo, DeepHaloTile};

const int TIMER_MAX_ITEM = 16;

class Solver
{
  public:
  Solver(DomainDecomp& dd, UniMesh& mesh, int nh);
  virtual ~Solver();

  void set_from_option();

  int setup();

  int setup_parallel_mode(int mode);

  int solve(int nIter);

  int analyze_time();

  void clear_history();

  void copy_var(int i, double *var);

  private:
  int _setup_overlap_range();
  int _setup_thread_inner_range(int  inRngs[6]);
  int _setup_thread_outer_range(int outRngs[6][6]);

  virtual int _solve_mpi_bsp(int nIter) =0;
  virtual int _solve_funneled(int nIter) =0;
  virtual int _solve_overlap(int nIter) =0;
  virtual int _solve_pipeline(int nIter) =0;
  virtual int _solve_deephalo(int nIter) =0;
  virtual int _solve_deephalo_tile(int nIter) =0;

  int _analyze_time_mpi();
  int _analyze_time_hybrid();
  int _analyze_time_overlap();

  protected:
  DomainDecomp*          _ptDd;
  double                 _h;
  double**               _d;
  HaloStore              _hs;
  HaloTransfer*          _hts;
  int                    _nHalo;
  int                    _nMargin;
  int                    _mode;
  vector<vector<int>>    _thrdOutRngs;   // thread's outer range
  vector<vector<int>>    _thrdInRngs;    // thread's outer range
  vector<vector<double>> _t;
  bool                   _isSyncComm;
#ifdef PAPI
  double *_llcm;
#endif
};

#endif
