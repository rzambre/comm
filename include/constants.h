#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <mpi.h>
#include <cmath>

const double PI    = 4.0 * atan(1.0);
const double SMALL = 1.0e-12;

#endif
