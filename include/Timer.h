#ifndef TIMER_H
#define TIMER_H

#include <mpi.h>
#include <omp.h>

#ifdef MIRA_CETUS
  #include "rdtsc.h"
  #define mpi_time() rdtsc()/1.6e9
  #define omp_time() rdtsc()/1.6e9
#else
  #define mpi_time() MPI_Wtime()
  #define omp_time() omp_get_wtime()
#endif

#endif
