#ifndef WJSolver_H
#define WJSolver_H

#include "Solver.h"

class WJSolver: public Solver
{
  public:
  WJSolver(DomainDecomp& dd, UniMesh& mesh, int nh): Solver(dd, mesh, nh) {};
  ~WJSolver();
  int setup();
  int init();
  int init_numa();
  int nullify();

  private:
  int _solve_mpi_bsp(int nIter); 
  int _solve_funneled(int nIter);
  int _solve_overlap(int nIter);
  int _solve_pipeline(int nIter);
  int _solve_deephalo(int nIter);
  int _solve_deephalo_tile(int nIter);
};

void _compute_range(int rngs[6], int is, int js, double* p, double* a, double *b, double h2);

#endif
