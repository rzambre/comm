#ifndef UniMesh_H
#define UniMesh_H

#include "DomainDecomp.h"
#include <memory>

class UniMesh
{
  public:
  // constructor
  UniMesh(const DomainDecomp& dd);

  // constructor with origin coordes and domain length
  UniMesh(const DomainDecomp& dd, const double x0[3], const double l[3]);

  void set_from_option();
  double step() const;
  void view() const;
  int compute_coord(const int indices[3], double coords[3]) const;
  int compute_index(const double coords[3], int indices[3]) const;

  private:
  double _h;           // space step
  double _x0[3];       // origin coordinates
  double _l[3];        // domain length
  const DomainDecomp* _ptDd; // pointer to decomp
};

#endif
