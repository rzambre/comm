#ifndef HALOTRANSFER_H
#define HALOTRANSFER_H

#include "DomainDecomp.h"
#include "HaloStore.h"
#include <vector>

using std::vector;

class HaloTransfer
{
  public:
  HaloTransfer(): _nHalo(1), _cacheLineLen(8), _ptDd(NULL), _cBegin(0), _cEnd(6) {}
  HaloTransfer(const DomainDecomp& dd, const int nHalo);

  HaloTransfer(const HaloTransfer& rhs);

  const HaloTransfer& operator=(const HaloTransfer& rhs);

  void init(DomainDecomp* dd, int nHalo)
  { _ptDd = dd; _nHalo = nHalo; }

  void set_from_option();

  void get_local_range(int rngs[6]);
    
  int num_halo() {return _nHalo;}

  int pack_halo(double** d, vector<int>& vs, HaloStore& hs);

  int unpack_halo(double** d, vector<int>& vs, HaloStore& hs);

  int update_halo_begin(HaloStore& hs);

  int update_halo_end();

  int update_halo(HaloStore& hs);

  void debug_view();

  int set_halo_range(HaloStore& hs, int cBegin, int cEnd, int nThrd);

  int setup_pack_pipeline(int p, HaloStore& hs, int nThrd);

  int setup_unpack_pipeline(int p, HaloStore& hs, int nThrd);

  int pack_subhalo(double** d, vector<int>& vs, HaloStore& hs);

  int unpack_subhalo(double** d, vector<int>& vs, HaloStore& hs);

  private:
  int                 _nHalo;
  int                 _cacheLineLen;
  const DomainDecomp* _ptDd;
  vector<vector<int>> _haloRngs;    // thread's chunk range in halo
  vector<vector<int>> _bodyRngs;    // thread's chunk range in body
  vector<int>         _bufStarts;
  vector<MPI_Request> _reqs;
  int                 _cBegin, _cEnd;
  vector<vector<vector<int>>> _unpackJobs;
};


#endif
