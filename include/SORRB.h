#ifndef SORRB_H
#define SORRB_H

#include "Smoother.h"
#include "HaloArray.h"
#include <memory>

enum Color {Red, Black};

class SORRB : public Smoother
{
  public:
  SORRB(const DomainDecomp& dd, const UniMesh& mesh):
    Smoother(dd, mesh)
  {};

  int analyze_time();

  int smooth_fused_cb(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
                      HaloArray& b, HaloArray& p, int nIter);

  int smooth_pipeline(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
                      HaloArray& b, HaloArray& p, int nIter);

  int smooth_fused_cb(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
                      HaloArray& b, HaloArray& p);

  private:
  int _smooth_bsp_mpi(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
                         HaloArray& b, HaloArray& p);

  int _smooth_bsp_hybrid(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
                         HaloArray& b, HaloArray& p);

  int _smooth_funneled(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
                       HaloArray& b, HaloArray& p);

  int _smooth_fused(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
                    HaloArray& b, HaloArray& p);

  int _smooth_wavefront(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
                        HaloArray& b, HaloArray& p);

  int _smooth_range_fused_cb(const int rngs[6], const int cbRngs[6], const double h, 
      HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, HaloArray& b, HaloArray& p, double *cb);

  int _analyze_time_bsp_mpi();

  int _analyze_time_bsp_hybrid();

  int _analyze_time_funneled();

};

int _smooth_color_range(const int rngs[6], const Color c, const double h,\
                        HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz,\
                        HaloArray& b, HaloArray& p);
/*----------------------------------------------------------
 smooth vsp

IN:  rngs - [6], range in local domain, start from 0
     c    - red or black
     h    - spacial step
     b, rhox, rhoy, rhoz
OUT: p
----------------------------------------------------------*/

int _smooth_color_range(const int rngs[6], const Color c, const double h,\
                        HaloArray& b, HaloArray& p);
/*----------------------------------------------------------
 smooth ccp

IN:  rngs - [6], range in local domain, start from 0
     c    - red or black
     h    - spacial step
     b
OUT: p
----------------------------------------------------------*/

#endif
