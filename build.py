import subprocess, os, shutil, sys

if __name__ == '__main__':
  # environment vars for compiler, etc
  os.environ['CC']  = 'mpicc'
  os.environ['CXX'] = 'mpicxx'

  # command for cmake, acceptable options are
  # -Dlikwid_dir=
  # -Dpapi_dir=
  # -Dmetis_dir=
  options = [
    'cmake', '..',
  ]

  # build dir
  buildDir = 'build'
  if len(sys.argv) == 2: buildDir = sys.argv[1]
  buildDir = './' + buildDir + '/'
  if os.path.isdir(buildDir): shutil.rmtree(buildDir) # remove dir if existed
  os.mkdir(buildDir)

  subprocess.run(options, cwd=buildDir)
