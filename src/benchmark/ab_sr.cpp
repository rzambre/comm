#include <iostream>
#include <cmath>
#include <iomanip>
#include <fstream>
#include "Timer.h"
#include "Utils.h"
#include <mpi.h>

using namespace std;

const int N    = 2097152;
const int nRun = 1024;

int main()
{
  double *sbuf = new double [N];
  double *rbuf = new double [N];
  int    nSend = (int)log2((double)N)+1;
  double *t    = new double [nSend];
  double *tMax = new double [nSend];
  double tTmp;
  int    rank, nProc;

  MPI_Request req0, req1;

  MPI_Init(0,0);

  MPI_Comm_size(MPI_COMM_WORLD, &nProc);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  if(nProc != 2 && rank == 0)
    cerr << "ERROR: The test should be excuted with 2 processes only" << endl;

  //init buffer
  for(int i=0; i<N; i++){
    sbuf[i] = (double)(rank + 1);
    rbuf[i] = 0.0;
  }

  for(int j=0; j<nRun; j++){
    for(int i=0; i<nSend; i++){
      mpi_busy_wait(0.01);
      MPI_Barrier(MPI_COMM_WORLD);
      if(rank == 0){
        tTmp = mpi_time();
        MPI_Send(sbuf, (int)pow(2, i), MPI_DOUBLE, 1, 0, MPI_COMM_WORLD);
        MPI_Recv(rbuf, (int)pow(2, i), MPI_DOUBLE, 1, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        t[i] += mpi_time() - tTmp;
      }
      else{
        tTmp = mpi_time();
        MPI_Recv(rbuf, (int)pow(2, i), MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Send(sbuf, (int)pow(2, i), MPI_DOUBLE, 0, 1, MPI_COMM_WORLD);
        t[i] += mpi_time() - tTmp;
      }
    }//end for i
  }//end for j

  // max time
  MPI_Allreduce(t, tMax, nSend, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);

  if(rank == 0){
    ofstream output;
    output.open("time_sr.txt");
    output << "#Btypes\tTime Root\tTime Max" << endl;
    for(int i=0; i<nSend; i++)
      output << right << setw(8) << (int)pow(2, i)*sizeof(double)
             << right << setw(16) << t[i]/nRun
             << right << setw(16) << tMax[i]/nRun << endl;
    output.close();
  }

  MPI_Finalize();

  return(0);
}
