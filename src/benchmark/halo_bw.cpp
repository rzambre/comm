#include "DomainDecomp.h"
#include "Timer.h"
#include "Utils.h"
#include "HaloStore.h"
#include "HaloTransfer.h"

#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>

using namespace std;

int main(int argc, char* argv[])
{
  sys_init(argc, argv, MPI_THREAD_FUNNELED);

  int  n[3] = {64,64,64}, np[3] = {2, 2, 2}, nHalo = 2;
  bool isPrds[3] = {1, 1, 1};
  int  nIter = 16, nWarm = 0;
  bool isSync = false, isSet;
  double tSpin = 0.001;
  vector<double> tBegin(nIter, 0.0), tEnd(nIter, 0.0);

  get_option("nwarm", CmdOption::Int,    1, &nIter,  &isSet);
  get_option("nhalo", CmdOption::Int,    1, &nHalo,  &isSet);
  get_option("niter", CmdOption::Int,    1, &nIter,  &isSet);
  get_option("sync",  CmdOption::Bool,   1, &isSync, &isSet);
  get_option("tspin", CmdOption::Double, 1, &tSpin,  &isSet);

  DomainDecomp dd(n, np, isPrds, 1, MPI_COMM_WORLD, Neighbor::star);
  dd.set_from_option();
  dd.setup();

  HaloStore hs(dd, nHalo, 1);
  HaloTransfer ht(dd, nHalo);
  if (dd.is_neighbor_star()) {
    hs.setup_star();
    ht.set_halo_range(hs, 0, 6, 1);
  }
  else {
    hs.setup_box();
    ht.set_halo_range(hs, 0, 26, 1);
  }

  // allocate halo array
  int szs[3];
  dd.get_local_size(szs);
  double **d = new double*;
  d[0] = new double [(szs[0]+2*nHalo)*(szs[1]+2*nHalo)*(szs[2]+2*nHalo)];

  // init halo array
  int jStrd = szs[2] + 2*nHalo;
  int iStrd = jStrd * (szs[1] + 2*nHalo);
  for (int i=nHalo; i<szs[0]+nHalo; ++i)
    for (int j=nHalo; j<szs[1]+nHalo; ++j)
      for (int k=nHalo; k<szs[2]+nHalo; ++k)
        d[0][k+j*jStrd+i*iStrd] = 1.0 + dd.rank();

  vector<int> vs(1,0);
  ht.pack_halo(d, vs, hs);
  // warm up, not sure needed
  for (int i=0; i<nWarm; ++i) {
    if (isSync) MPI_Barrier(MPI_COMM_WORLD);
    ht.update_halo(hs);
    mpi_busy_wait(tSpin);
  }
  // actual test
  for (int i=0; i<nIter; ++i) {
    if (isSync) MPI_Barrier(MPI_COMM_WORLD);
    tBegin[i] = mpi_time();
    ht.update_halo(hs);
    tEnd[i] = mpi_time();
    mpi_busy_wait(tSpin);
  }
  ht.unpack_halo(d, vs, hs);

  // analyze time
  // find max and avg
  vector<double> tAvg(nIter, 0.0), tMax(nIter, 0.0);
  for (unsigned int i=0; i<tBegin.size(); ++i) {
    tMax[i] = tEnd[i] - tBegin[i];
    tAvg[i] = tEnd[i] - tBegin[i];
  }
  MPI_Allreduce(MPI_IN_PLACE, &tMax[0], (int)tMax.size(), MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE, &tAvg[0], (int)tAvg.size(), MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  for (unsigned int i=0; i<tAvg.size(); ++i) tAvg[i] /= dd.num_proc();
  // find difference in beginning time
  vector<double> tBeginMin(nIter, 0.0), tBeginMax(nIter, 0.0);
  MPI_Allreduce(&tBegin[0], &tBeginMax[0], (int)tBegin.size(), MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  MPI_Allreduce(&tBegin[0], &tBeginMin[0], (int)tBegin.size(), MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
  //.. use tBeginMax to store difference
  for (unsigned int i=0; i<tBeginMin.size(); ++i) tBeginMax[i] -= tBeginMin[i];
  
  // print time
  if (dd.is_root()) {
    cout << "# avg, max, delay over all tests " << endl;
    cout << accumulate(tAvg.begin(), tAvg.end(), 0.0)/nIter << "\t"
         << *max_element(tMax.begin(), tMax.end()) << "\t"
         << *max_element(tBeginMax.begin(), tBeginMax.end()) << endl;
    cout << "# iter\ttAvg\ttMax\tdelay" << endl;
    for (int i=0; i<nIter; ++i)
      cout << i << "\t" << tAvg[i] << "\t" << tMax[i] << "\t" << tBeginMax[i] << endl;
    // dummy usage of d to provide compiler from realsizing d is useless
    double tmp = accumulate(d[0], d[0]+ (szs[0]+2*nHalo)*(szs[1]+2*nHalo)*(szs[2]+2*nHalo), 0.0);
    cout << "dummy " << tmp << endl;
  }

  sys_finalize();

  return 0;
}
