#include "Utils.h"
#include "Timer.h"

void mpi_busy_wait(double tSpin)
{
  double t0 = mpi_time();
  while (mpi_time() - t0 < tSpin) {}
}


void omp_busy_wait(double tSpin)
{
  double t0 = omp_time();
  while(omp_time() - t0 < tSpin) {}
}
