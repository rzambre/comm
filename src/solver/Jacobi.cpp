#include "Jacobi.h"

#include <omp.h>

int Jacobi::_smooth_bsp_mpi(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
                            HaloArray& b, HaloArray& p)
{
  // range work on
  int rngs[6];
  p.get_local_range(rngs);

  _timer.mark("comp");
  _smooth_range(rngs, _ptMesh->step(), rhox, rhoy, rhoz, b, p);
  _timer.mark("pack");
  p.pack_face_halo();
  _timer.mark("comm");
  p.update_face_halo();
  _timer.mark("unpack");
  p.unpack_face_halo();
  _timer.mark("end");

  return 0;
}


#ifdef _OPENMP
int Jacobi::_smooth_bsp_hybrid(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
                               HaloArray& b, HaloArray& p)
{
  // range work on
  int rngs[6], tid=omp_get_thread_num();
  p.get_thread_range(tid, rngs);

  _timer.begin(tid, "total");
  _timer.begin(tid, "comp");
  _smooth_range(rngs, _ptMesh->step(), rhox, rhoy, rhoz, b, p);
  _timer.end(tid, "comp");
  #pragma omp barrier
  _timer.begin(tid, "pack");
  p.pack_face_halo();
  _timer.begin(tid, "upack");
  #pragma omp barrier
  #pragma omp master
  _timer.begin(tid, "comm");
  p.update_face_halo();
  _timer.end(tid, "comm");
  #pragma omp barrier
  _timer.begin(tid, "unpack");
  p.unpack_face_halo();
  _timer.end(tid, "unpack");
  #pragma omp barrier
  _timer.end(tid, "total");

  return 0;
}


int Jacobi::_smooth_funneled(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
                             HaloArray& b, HaloArray& p)
{
  int tid = omp_get_thread_num();

  // exit if single thread
  if (_ptDd->num_thread() < 2) {
    if (_ptDd->is_root())
      std::cout << "Error: funneled only works with >1 threads" << std::endl;
    MPI_Finalize();
  }

  _timer.begin(tid, "total");
  _timer.begin(tid, "outer");
  for (unsigned int i=0; i<_thrdOutRngs.size(); i+=6) {
    _smooth_range(&_thrdOutRngs[tid][i], _ptMesh->step(), rhox, rhoy, rhoz, b, p);
  }
  _timer.end(tid, "outer");
  #pragma omp barrier
  _timer.begin(tid, "pack");
  p.pack_face_halo();
  _timer.end(tid, "pack");
  #pragma omp barrier
  if (tid == 0) {
    _timer.begin(tid, "comm");
    p.update_face_halo();
    _timer.end(tid, "comm");
  }
  else {
    _timer.begin(tid, "inner");
    _smooth_range(&_thrdInRngs[tid][0],  _ptMesh->step(), rhox, \
                  rhoy, rhoz, b, p);
    _timer.end(tid, "inner");
  }
  #pragma omp barrier
  _timer.begin(tid, "unpack");
  p.unpack_face_halo();
  _timer.end(tid, "unpack");
  #pragma omp barrier
  _timer.end(tid, "total");

  return 0;
}
#endif


int _smooth_range(const int rngs[6], const double h, HaloArray& rhox, \
                  HaloArray& rhoy, HaloArray& rhoz, HaloArray& b, HaloArray& p)
{
  // stride
  int is = p.stride_i();
  int js = p.stride_j();

  for (int i=rngs[0]; i<rngs[3]; ++i) {
    for (int j=rngs[1]; j<rngs[4]; ++j) {
      for (int k=rngs[2]; k<rngs[5]; ++k) {
        int ijk = i*is + j*js + k;
        p[ijk] = ( p[ijk- 1]/rhoz[ijk-1]  + p[ijk+ 1]/rhoz[ijk] 
                 + p[ijk-js]/rhoy[ijk-js] + p[ijk+js]/rhoy[ijk]
                 + p[ijk-is]/rhox[ijk-is] + p[ijk+is]/rhox[ijk] - h*h*b[ijk])
               / ( 1.0/rhoz[ijk-1] + 1.0/rhoz[ijk]    + 1.0/rhoy[ijk-js]
                 + 1.0/rhoy[ijk]   + 1.0/rhox[ijk-is] + 1.0/rhox[ijk]);
      }//end for k
    }//end for j
  }//end for i

  return 0;
}


int _smooth_range(const int rngs[6], const double h, HaloArray& b, HaloArray& p)
{
  // stride
  int is = p.stride_i();
  int js = p.stride_j();

  for (int i=rngs[0]; i<rngs[3]; ++i) {
    for (int j=rngs[1]; j<rngs[4]; ++j) {
      for (int k=rngs[2]; k<rngs[5]; ++k) {
        int ijk = i*is + j*js + k;
        p[ijk] = ( p[ijk- 1] + p[ijk+ 1] + p[ijk-js] + p[ijk+js]
                 + p[ijk-is] + p[ijk+is] - h*h*b[ijk]) / 6.0;
      }//end for k
    }//end for j
  }//end for i

  return 0;
}
