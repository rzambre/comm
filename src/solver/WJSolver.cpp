#include "WJSolver.h"
#include "constants.h"
#include <cassert>


WJSolver::~WJSolver()
{
  for (int i=0; i<3; ++i) delete [] _d[i];
  delete [] _d;
}


int WJSolver::setup()
{
  // set halo store, transfer, etc, nVar, pipe are set from option
  Solver::setup();

  // allocate data
  int szs[3];
  _ptDd->get_local_size(szs);
  _d = new double* [3];
  for (int i=0; i<3; ++i) 
    _d[i] = new double [(szs[0]+2*_nHalo) *(szs[1]+2*_nHalo)*(szs[2]+2*_nHalo)];

  return 0;
}


int WJSolver::init()
{
  int szs[3], starts[3];
  _ptDd->get_local_size(szs);
  _ptDd->get_local_start(starts);
  int jStrd = szs[2] + 2*_nHalo;
  int iStrd = jStrd * (szs[1] + 2*_nHalo);
  int totSz = (szs[0]+2*_nHalo) * iStrd; 
  std::fill(_d[0], _d[0]+totSz, 0.0);
  std::fill(_d[1], _d[1]+totSz, 0.0);
//  std::fill(_d[2], _d[2]+totSz, 2.0);

  for (int i=0; i<szs[0]+2*_nHalo; ++i) {
    for (int j=0; j<szs[1]+2*_nHalo; ++j) {
      for (int k=0; k<szs[2]+2*_nHalo; ++k) {
        int ijk = i*iStrd + j*jStrd + k;
        _d[2][ijk] = -12.0*PI*PI*sin(2*PI*(i-_nHalo+starts[0]+0.5)*_h)
                   * sin(2*PI*(j-_nHalo+starts[1]+0.5)*_h) 
                   * sin(2*PI*(k-_nHalo+starts[2]+0.5)*_h);
      }
    }
  }

  return 0;
}


int WJSolver::init_numa()
{
  int tid = omp_get_thread_num();
  int rngs[6], tRngs[6], starts[3];
  _ptDd->get_local_size(rngs);
  _ptDd->get_local_start(starts);
  int jStrd = rngs[2] + 2*_nHalo, iStrd = jStrd * (rngs[1] + 2*_nHalo);
  for (int i=0; i<3; ++i) {
    rngs[i+3] = rngs[i] + _nHalo;
    rngs[i]   = _nHalo;
  }
  compute_thread_range(rngs, _ptDd->num_thread(), tid, tRngs);

  for (int i=0; i<3; ++i) {
    if (tRngs[i] == _nHalo)      tRngs[i]  = 0;
    if (tRngs[i+3] == rngs[i+3]) tRngs[i+3] += _nHalo;
  }
  for (int i=tRngs[0]; i<tRngs[1]; ++i) {
    for (int j=tRngs[1]; j<tRngs[4]; ++j) {
      for (int k=tRngs[2]; k<tRngs[5]; ++k) {
        _d[0][i*iStrd+j*jStrd+k] = 0.0;
        _d[1][i*iStrd+j*jStrd+k] = 0.0;
      }
    }
  }
  for (int i=tRngs[0]; i<tRngs[3]; ++i) {
    for (int j=tRngs[1]; j<tRngs[4]; ++j) {
      for (int k=tRngs[2]; k<tRngs[5]; ++k) {
        int ijk = i*iStrd + j*jStrd + k;
        _d[2][ijk] = -12.0*PI*PI*sin(2*PI*(i-_nHalo+starts[0]+0.5)*_h)
                   * sin(2*PI*(j-_nHalo+starts[1]+0.5)*_h) 
                   * sin(2*PI*(k-_nHalo+starts[2]+0.5)*_h);
      }
    }
  }
  //for (int i=0; i<rngs[0]+2*_nHalo; ++i) {
  //  #pragma omp for
  //  for (int j=0; j<rngs[1]+2*_nHalo; ++j) {
  //    for (int k=0; k<rngs[2]+2*_nHalo; ++k) {
  //      int ijk = i*iStrd + j*jStrd + k;
  //      _d[0][ijk] = 0.0;
  //      _d[1][ijk] = 0.0;
  //      _d[2][ijk] = -12.0*PI*PI*sin(2*PI*(i-_nHalo+starts[0]+0.5)*_h)
  //                 * sin(2*PI*(j-_nHalo+starts[1]+0.5)*_h) 
  //                 * sin(2*PI*(k-_nHalo+starts[2]+0.5)*_h);
  //    }
  //  }
  //}

  return 0;
}


int WJSolver::nullify()
{
  int szs[3];
  _ptDd->get_local_size(szs);
  int totSz = (szs[0]+2*_nHalo) * (szs[1]+2*_nHalo) * (szs[2]+2*_nHalo);

  std::fill(_d[0], _d[0]+totSz, 0.0);
  std::fill(_d[1], _d[1]+totSz, 0.0);

  return 0;
}


int WJSolver::_solve_mpi_bsp(int nIter)
{
  int rngs[6]; _ptDd->get_local_size(rngs);
  int jStrd = rngs[2] + 2*_nHalo;
  int iStrd = jStrd * (rngs[1] + 2*_nHalo);
  for (int i=0; i<3; ++i) {
    rngs[i+3] = rngs[i] + _nHalo;
    rngs[i]   = _nHalo;
  }
  double h2 = _h * _h;
  vector<int> vs(1,0);

  for (int m=0; m<nIter; ++m) {
    // comp
    _t[0][SolverTime::Comp] -= mpi_time();
    _compute_range(rngs, iStrd, jStrd, _d[0], _d[1], _d[2], h2);
    std::swap(_d[0], _d[1]);
    _t[0][SolverTime::Comp] += mpi_time();
    // pack
    _t[0][SolverTime::Pack] -= mpi_time();
    _hts[0].pack_halo(_d, vs, _hs);
    _t[0][SolverTime::Pack] += mpi_time();
    // comm
    if (_isSyncComm) MPI_Barrier(MPI_COMM_WORLD);
    _t[0][SolverTime::Comm] -= mpi_time();
    _hts[0].update_halo(_hs);
    _t[0][SolverTime::Comm] += mpi_time();
    // unpack
    _t[0][SolverTime::Unpack] -= mpi_time();
    _hts[0].unpack_halo(_d, vs, _hs);
    _t[0][SolverTime::Unpack] += mpi_time();
  }

  return 0;
}


int WJSolver::_solve_funneled(int nIter)
{
  // range work on
  int szs[6], rngs[6], tid=omp_get_thread_num();
  _ptDd->get_local_size(szs);
  _ptDd->get_thread_range(tid, rngs);
  int jStrd = szs[2] + 2*_nHalo, iStrd = jStrd * (szs[1] + 2*_nHalo);
  for (int i=0; i<6; ++i) rngs[i] += _nHalo;
  double      h2 = _h * _h;
  vector<int> vs(1,0);

  for (int m=0; m<nIter; ++m) {
    _t[tid][SolverTime::Comp] -= omp_time();
    if (m % 2 == 0) {
      _compute_range(rngs, iStrd, jStrd, _d[0], _d[1], _d[2], h2);
      vs[0] = 1;
    }
    else {
      _compute_range(rngs, iStrd, jStrd, _d[1], _d[0], _d[2], h2);
      vs[0] = 0;
    }
    _t[tid][SolverTime::Comp] += omp_time();
    #pragma omp barrier
    _t[tid][SolverTime::Pack] -= omp_time();
    _hts[0].pack_halo(_d, vs, _hs);
    _t[tid][SolverTime::Pack] += omp_time();
    #pragma omp barrier
    if (tid == _ptDd->num_thread()-1) {
      if (_isSyncComm)  MPI_Barrier(MPI_COMM_WORLD);
      _t[tid][SolverTime::Comm] -= omp_time();
      _hts[0].update_halo(_hs);
      _t[tid][SolverTime::Comm] += omp_time();
    }
    #pragma omp barrier
    _t[tid][SolverTime::Unpack] -= omp_time();
    _hts[0].unpack_halo(_d, vs, _hs);
    _t[tid][SolverTime::Unpack] += omp_time();
    #pragma omp barrier
  }

  return 0;
}


int WJSolver::_solve_overlap(int nIter)
{
  int tid = omp_get_thread_num();
  int szs[3]; _ptDd->get_local_size(szs);
  int jStrd = szs[2] + 2*_nHalo;
  int iStrd = jStrd * (szs[1] + 2*_nHalo);
  double h2 = _h * _h;
  vector<int> vs(1,0);

  for (int m=0; m<nIter; ++m) {
    // outer
    _t[tid][SolverTime::Outer] -= omp_time();
    if (m % 2 == 0) {
      for (unsigned int i=0; i<_thrdOutRngs[tid].size(); i+=6)
        _compute_range(&_thrdOutRngs[tid][i], iStrd, jStrd, _d[0], _d[1], _d[2], h2);
      vs[0] = 1;
    }
    else {
      for (unsigned int i=0; i<_thrdOutRngs[tid].size(); i+=6)
        _compute_range(&_thrdOutRngs[tid][i], iStrd, jStrd, _d[1], _d[0], _d[2], h2);
      vs[0] = 0;
    }
    _t[tid][SolverTime::Outer] += omp_time();
    #pragma omp barrier
    _t[tid][SolverTime::Pack] -= omp_time();
    _hts[0].pack_halo(_d, vs, _hs);
    _t[tid][SolverTime::Pack] += omp_time();
    #pragma omp barrier
    if (tid == _ptDd->num_thread()-1) {
        if (_isSyncComm)  MPI_Barrier(MPI_COMM_WORLD);
        _t[tid][SolverTime::Comm] -= omp_time(); 
        _hts[0].update_halo(_hs);
        _t[tid][SolverTime::Comm] += omp_time();
    }
    else {
      _t[tid][SolverTime::Inner] -= omp_time();
      if (m % 2 == 0)
        _compute_range(&_thrdInRngs[tid][0], iStrd, jStrd, _d[0], _d[1], _d[2], h2);
      else
        _compute_range(&_thrdInRngs[tid][0], iStrd, jStrd, _d[1], _d[0], _d[2], h2);
      _t[tid][SolverTime::Inner] += omp_time();
    }
    #pragma omp barrier
    _t[tid][SolverTime::Unpack] -= omp_time();
    _hts[0].unpack_halo(_d, vs, _hs);
    _t[tid][SolverTime::Unpack] += omp_time();
    #pragma omp barrier
  }

  return 0;
}


int WJSolver::_solve_pipeline(int nIter)
{
  int nPipe = _hs.num_pipe(), pDir = _hs.pipe_dir();
  assert(nPipe > 3);

  int tid   = omp_get_thread_num();
  int nThrd = _ptDd->num_thread();
  // range
  int rngs[6], tRngs[6];
  _ptDd->get_local_size(rngs);
  int jStrd = rngs[2] + 2*_nHalo, iStrd = jStrd * (rngs[1] + 2*_nHalo);
  double h2 = _h * _h;
  for (int i=0; i<3; ++i) {
    rngs[i+3] = rngs[i] + _nHalo;
    rngs[i]   = _nHalo;
  }
  int pipeLen = (rngs[5] - rngs[2]) / nPipe;
  vector<int> vs(1,1);

  // init: 0->last
  for (int l=0; l<nPipe; ++l) {
    if (tid == _ptDd->num_thread()-1) {
      _t[tid][SolverTime::Comm] -= omp_time();
      if (l > 0) _hts[l-1].update_halo(_hs);
      _t[tid][SolverTime::Comm] += omp_time();
    }
    else {
      _t[tid][SolverTime::Comp] -= omp_time();
      rngs[pDir]   = _nHalo + l * pipeLen;
      rngs[pDir+3] = rngs[pDir] + pipeLen;
      compute_thread_range(rngs, nThrd-1, tid, tRngs);
      _compute_range(tRngs, iStrd, jStrd, _d[0], _d[1], _d[2], h2);
      _t[tid][SolverTime::Comp] += omp_time();
      _t[tid][SolverTime::Pack] -= omp_time();
      _hts[l].pack_subhalo(_d, vs, _hs);
      _t[tid][SolverTime::Pack] += omp_time();
    }
    #pragma omp barrier
  }

  // pipelined solve: 1->0->2 ... ->last
  vector<int> pipes(1,nPipe-1);
  for (int l=0; l<nPipe; ++l) pipes.push_back(l);
  pipes[1] = 1;
  pipes[2] = 0;
  //
  for (int m=1; m<nIter; ++m) {
    for (int l=0; l<nPipe; ++l) {
      if (tid == nThrd-1) {
        _t[tid][SolverTime::Comm] -= omp_time();
        _hts[pipes[l]].update_halo(_hs);
        _t[tid][SolverTime::Comm] += omp_time();
      }
      else {
        _t[tid][SolverTime::Unpack] -= omp_time();
        vs[0] = m % 2;
        _hts[pipes[l+1]].unpack_subhalo(_d, vs, _hs);
        _t[tid][SolverTime::Unpack] += omp_time();
        _t[tid][SolverTime::Comp] -= omp_time();
        rngs[pDir]   = _nHalo + pipes[l+1]*pipeLen;
        rngs[pDir+3] = rngs[pDir] + pipeLen;
        compute_thread_range(rngs, nThrd-1, tid, tRngs);
        if (m % 2 == 0) {
          vs[0] = 1;
          _compute_range(tRngs, iStrd, jStrd, _d[0], _d[1], _d[2], h2);
        }
        else {
          vs[0] = 0;
          _compute_range(tRngs, iStrd, jStrd, _d[1], _d[0], _d[2], h2);
        }
        _t[tid][SolverTime::Comp] += omp_time();
        _t[tid][SolverTime::Pack] -= omp_time();
        _hts[pipes[l+1]].pack_subhalo(_d, vs, _hs);
        _t[tid][SolverTime::Pack] += omp_time();
      }
      #pragma omp barrier
    }
  }
  
  return 0;
}


int WJSolver::_solve_deephalo(int nIter)
{
  int szs[3], rngs[6], tRngs[6];
  _ptDd->get_local_size(szs);
  int jStrd = szs[2] + 2*_nHalo;
  int iStrd = jStrd * (szs[1] + 2*_nHalo);
  double h2 = _h * _h;
  vector<int> vs(1,0);
  int tid = omp_get_thread_num(), nThrd = _ptDd->num_thread();

  for (int r=0; r<nIter/_nHalo; ++r) {
    for (int m=0; m<_nHalo; ++m) {
      // thread index range with halos not disgarded
      for (int i=0; i<3; ++i) {
        rngs[i]   = m+1;
        rngs[i+3] = szs[i] + 2*_nHalo - (m+1); 
      }
      compute_thread_range(rngs, nThrd, tid, tRngs);
      // Comp
      _t[tid][SolverTime::Comp] -= omp_time();
      if (m%2 == 0) 
        _compute_range(tRngs, iStrd, jStrd, _d[0], _d[1], _d[2], h2);
      else
        _compute_range(tRngs, iStrd, jStrd, _d[1], _d[0], _d[2], h2);
      _t[tid][SolverTime::Comp] += omp_time();
      #pragma omp barrier
    }// end for m
    // pack
    _t[tid][SolverTime::Pack] -= omp_time();
    _hts[0].pack_halo(_d, vs, _hs);
    _t[tid][SolverTime::Pack] += omp_time();
    #pragma omp barrier
    // comm
    if (tid == _ptDd->num_thread()-1) {
      if (_isSyncComm)  MPI_Barrier(MPI_COMM_WORLD);
      _t[tid][SolverTime::Comm] -= omp_time();
      _hts[0].update_halo(_hs);
      _t[tid][SolverTime::Comm] += omp_time();
    }
    #pragma omp barrier
    // unpack
    _t[tid][SolverTime::Unpack] -= omp_time();
    _hts[0].unpack_halo(_d, vs, _hs);
    _t[tid][SolverTime::Unpack] += omp_time();
    #pragma omp barrier
  }// end for r
  return 0;
}


/*int WJSolver::_solve_deephalo_tile(int nIter)
{
  int szs[3], rngs[6], tRngs[6];
  _ptDd->get_local_size(szs);
  _ptDd->get_thread_range(tid, tRngs);
  int jStrd = szs[2] + 2*_nHalo, iStrd = jStrd * (szs[1] + 2*_nHalo);
  double h2 = _h * _h, w0 = 2.0/3.0, w1 = 1.0/3.0;
  double *p = _d[0], *a = _d[1];
  vector<int> vs(1,0);
  int tid = omp_get_thread_num();

  for (int r=0; r<nIter/_nHalo; ++r) {
    // comp, wavefront
    for (int i=1; i<=szs[0]+_nHalo-1+2*(_nHalo-1); ++i) {
      _t[tid][SolverTime::Comp] -= omp_time();
      for (int m=0; m<_nHalo; ++m) {
        if (i-2*m >= m+1 && i-2*m <= szs[0]+2*_nHalo-m-2) {// if i fit
          int ii   = (i-2*m-1) % 4 + (m/2)*4; // plane in a
          if (m%2 == 0) {
            #pragma omp for nowait
            for (int j=m+1; j<szs[1]+2*_nHalo-(m+1); ++j) {
              int ij  = (i-2*m)*iStrd + j * jStrd;
              int iij = ii*iStrd + j*jStrd;
              #pragma omp simd
              for (int k=m+1; k<szs[2]+2*_nHalo-(m+1); ++k) {
                int ijk = ij + k;
                a[iij+k] = w0 * (( p[ijk-1] + p[ijk+1] + p[ijk-jStrd] + p[ijk+jStrd] 
                                 + p[ijk-iStrd] + p[ijk+iStrd])/6.0 - h2*_d[2][ijk])
                         + w1 * p[ijk];

              }// end for k
            }// end for j
          }
          else {// a -> p
            int iim1 = (i-2*m+2) % 4 + (m/2)*4;
            int iip1 = (i-2*m)   % 4 + (m/2)*4;
            #pragma omp for nowait
            for (int j=m+1; j<szs[1]+2*_nHalo-(m+1); ++j) {
              int ij    = (i-2*m) * iStrd + j * jStrd;
              int iij   =   ii * iStrd + j * jStrd;
              int iim1j = iim1 * iStrd + j * jStrd;
              int iip1j = iip1 * iStrd + j * jStrd;
              #pragma omp simd
              for (int k=m+1; k<szs[2]+2*_nHalo-(m+1); ++k) {
                int iijk = iij + k;
                p[ij+k] = w0 * (( a[iijk-1] + a[iijk+1] + a[iijk-jStrd] + a[iijk+jStrd] 
                                + a[iim1j+k] + a[iip1j+k])/6.0 - h2*_d[2][ij+k])
                        + w1 * a[iijk];

              }// end for k
            }// end else
          }// end else
        }// end if i fit
      }// end for m
      _t[tid][SolverTime::Comp] += omp_time();
      #pragma omp barrier
    }// end for i
    // pack
    _t[tid][SolverTime::Pack] -= omp_time();
    _hts[0].pack_halo(_d, vs, _hs);
    _t[tid][SolverTime::Pack] += omp_time();
    #pragma omp barrier
    // comm
    if (tid == _ptDd->num_thread()-1) {
      if (_isSyncComm)  MPI_Barrier(MPI_COMM_WORLD);
      _t[tid][SolverTime::Comm] -= omp_time();
      _hts[0].update_halo(_hs);
      _t[tid][SolverTime::Comm] += omp_time();
    }
    #pragma omp barrier
    // unpack
    _t[tid][SolverTime::Unpack] -= omp_time();
    _hts[0].unpack_halo(_d, vs, _hs);
    _t[tid][SolverTime::Unpack] += omp_time();
    #pragma omp barrier
  }//end for r

  return 0;
}*/


int WJSolver::_solve_deephalo_tile(int nIter)
{
  int szs[3], tRngs[6], tid = omp_get_thread_num();
  _ptDd->get_local_size(szs);
  _ptDd->get_thread_range(tid, tRngs);
  for (int i=0; i<6; ++i) tRngs[i] += _nHalo;
  int jStrd = szs[2] + 2*_nHalo, iStrd = jStrd * (szs[1] + 2*_nHalo);
  int jStrdL = tRngs[5] - tRngs[2] + 2*_nHalo;
  int iStrdL = jStrdL * (tRngs[4] - tRngs[1] + 2*_nHalo);
  double h2 = _h * _h, w0 = 2.0/3.0, w1 = 1.0/3.0;
  double *p = _d[0], *a = _d[1], *b = _d[2];
  vector<int> vs(1,0);

  // aux array
  double *c  = new double [3*(_nHalo-1) * iStrdL];
//  if (_ptDd->is_root()) {
//#pragma omp critical 
//  std::cout << tid << " " << tRngs[0] << " "<< tRngs[1] << " "<< tRngs[2] << " "<< tRngs[3] << " "<< tRngs[4] << " "<< tRngs[5] << " " << std::endl;
//}
  for (int r=0; r<nIter/_nHalo; ++r) {
    // comp, wavefront
    _t[tid][SolverTime::Comp] -= omp_time();
    if (r % 2 == 0) {
      p = _d[0];
      a = _d[1];
      vs[0] = 1;
    }
    else {
      p = _d[1];
      a = _d[0];
      vs[0] = 0;
    }
    for (int i=1; i<szs[0]+2*_nHalo-1; ++i) {
      // p -> c
      int ij  = i * iStrd + (tRngs[1]-_nHalo+1) * jStrd;
      int ijL = (i-1)%3 * iStrdL + jStrdL;
      for (int j=tRngs[1]-_nHalo+1; j<tRngs[4]+_nHalo-1; ++j) {
        #pragma omp simd
        for (int k=tRngs[2]-_nHalo+1; k<tRngs[5]+_nHalo-1; ++k) {
          int ijk = ij + k;
          c[ijL+k-tRngs[2]+_nHalo]
            = w0 * (( p[ijk-1] + p[ijk+1] + p[ijk-jStrd] + p[ijk+jStrd] 
                    + p[ijk-iStrd] + p[ijk+iStrd])/6.0 - h2*b[ijk])
            + w1 * p[ijk];
        }
        ij  += jStrd;
        ijL += jStrdL;
      }
      // c -> c
      for (int m=1; m<_nHalo-1; ++m) {
        if (i-m >= m+1 && i-m <= szs[0]+2*_nHalo-m-2) {
          int ijL   = ((i-m-1)%3 + (m-1)*3) * iStrdL + (m+1)*jStrdL;
          int im1jL = ((i-m+1)%3 + (m-1)*3) * iStrdL + (m+1)*jStrdL;
          int ip1jL = ((i-m  )%3 + (m-1)*3) * iStrdL + (m+1)*jStrdL;
          int ijLTo = ijL + 3*iStrdL;
          int ij    = (i-m)*iStrd + (tRngs[1]-_nHalo+m+1)*jStrd;
          for (int j=m+1; j<tRngs[4]-tRngs[1]+2*_nHalo-(m+1); ++j) {
            #pragma omp simd
            for (int k=m+1; k<tRngs[5]-tRngs[2]+2*_nHalo-(m+1); ++k) {
              int ijkL = ijL + k;
              c[ijLTo+k] = w0 * (( c[ijkL-1] + c[ijkL+1] + c[ijkL-jStrdL] + c[ijkL+jStrdL] 
                                 + c[im1jL+k] + c[ip1jL+k])/6.0 - h2*b[ij+k+tRngs[2]-_nHalo])
                         + w1 * c[ijkL];
            }// end for k
            ijL   += jStrdL;
            im1jL += jStrdL;
            ip1jL += jStrdL;
            ijLTo += jStrdL;
            ij    += jStrd;
          }// end for j
        }// end if           
      }
      // c -> a
      if (i-_nHalo+1 >= _nHalo && i-_nHalo+1 <= szs[0]+_nHalo-1) {
        int ij    = (i-_nHalo+1)*iStrd + tRngs[1]*jStrd;
        int ijL   = ((i-_nHalo  )%3 + (_nHalo-2)*3)*iStrdL + _nHalo*jStrdL;
        int im1jL = ((i-_nHalo+2)%3 + (_nHalo-2)*3)*iStrdL + _nHalo*jStrdL;
        int ip1jL = ((i-_nHalo+1)%3 + (_nHalo-2)*3)*iStrdL + _nHalo*jStrdL;
        for (int j=_nHalo; j<tRngs[4]-tRngs[1]+_nHalo; ++j) {
          #pragma omp simd
          for (int k=_nHalo; k<tRngs[5]-tRngs[2]+_nHalo; ++k) {
            int ijkL = ijL + k;
            int ijk  = ij  + k + tRngs[2] - _nHalo;
            a[ijk] = w0 * (( c[ijkL-1] + c[ijkL+1] + c[ijkL-jStrdL] + c[ijkL+jStrdL]
                           + c[im1jL+k] + c[ip1jL+k])/6.0 - h2*b[ijk])
                   + w1 * c[ijkL];
          }// end for k
          ij    += jStrd;
          ijL   += jStrdL;
          im1jL += jStrdL;
          ip1jL += jStrdL;
        }// end for j
      }// end if      
    }// end for i
    _t[tid][SolverTime::Comp] += omp_time();
    #pragma omp barrier
    // pack
    _t[tid][SolverTime::Pack] -= omp_time();
    _hts[0].pack_halo(_d, vs, _hs);
    _t[tid][SolverTime::Pack] += omp_time();
    #pragma omp barrier
    // comm
    if (tid == _ptDd->num_thread()-1) {
      if (_isSyncComm)  MPI_Barrier(MPI_COMM_WORLD);
      _t[tid][SolverTime::Comm] -= omp_time();
      _hts[0].update_halo(_hs);
      _t[tid][SolverTime::Comm] += omp_time();
    }
    #pragma omp barrier
    // unpack
    _t[tid][SolverTime::Unpack] -= omp_time();
    _hts[0].unpack_halo(_d, vs, _hs);
    _t[tid][SolverTime::Unpack] += omp_time();
    #pragma omp barrier
  }//end for r

  delete [] c;

  return 0;
}


//int WJSolver::_solve_deephalo_overlap(int nIter)
//{
//  int szs[3];
//  _ptDd->get_local_size(szs);
//  int jStrd = szs[2] + 2*_nHalo, iStrd = jStrd * (szs[1] + 2*_nHalo);
//  double h2 = _h*_h, w0 = 2.0/3.0, w1 = 1.0/3.0;
//  double *p = _d[0], *a = _d[1], b = _d[2], c =_d[3];
//
//  for (int i=1; i<szs[0]+2*_nHalo-1; ++i)   {
//    for (int j=1; j<szs[1]+2*_nHalo-1; ++j) {
//      for (int k=1; k<szs[2]+2*_nHalo-1; ++k) {
//        if ( (i < _nHalo + _nMargin || i >= szs[0] + _nHalo - _nMargin) \
//          && (j < _nHalo + _nMargin || j >= szs[1] + _nHalo - _nMargin) \
//          && (k < _nHalo + _nMargin || k >= szs[2] + _nHalo - _nMargin)){
//          int ijk = i*iStrd + j*jStrd + k;
//          a[ijk] = w0 * (( p[ijk-1] + p[ijk+1] + p[ijk-js] + p[ijk+js] 
//                         + p[ijk-is] + p[ijk+is])/6.0 - h2*b[ijk])
//                 + w1 * p[ijk];
//
//        }// end if
//      }
//    }
//  }
//  // note that cannot write to p, since  inner update requies margin data
//  for (int m=1; m<_nHalo; ++m) {
//    for (int i=m+1; i<szs[0]+2*_nHalo-(m+1); ++i) {
//      for (int j=m+1; j<szs[1]+2*_nHalo-(m+1); ++j) {
//        for (int k=m+1; k<szs[2]+2*_nHalo-(m+1); ++k) {
//          if ( (i < _nHalo + _nMargin || i >= szs[0] + _nHalo - _nMargin) \
//            && (j < _nHalo + _nMargin || j >= szs[1] + _nHalo - _nMargin) \
//            && (k < _nHalo + _nMargin || k >= szs[2] + _nHalo - _nMargin)){
//            int ijk = i*iStrd + j*jStrd + k;
//            c[ijk] = w0 * (( a[ijk-1] + a[ijk+1] + a[ijk-js] + a[ijk+js] 
//                           + a[ijk-is] + a[ijk+is])/6.0 - h2*b[ijk])
//                   + w1 * a[ijk];
//          }
//        }
//      }
//    }// end for i
//    std::swap(a, c);
//  }
//
//  return 0;
//}


void _compute_range(int rngs[6], int is, int js, double *p, double *a, double *b, double h2)
/* p - input, a - output, b - rhs */
{
  double w0 = 2.0/3.0, w1 = 1.0/3.0;
  int    base=rngs[0]*is + rngs[1]*js;
  for (int i=rngs[0]; i<rngs[3]; ++i) {
    int ij = base;
    for (int j=rngs[1]; j<rngs[4]; ++j) {
      #pragma omp simd
      for (int k=rngs[2]; k<rngs[5]; ++k) {
        int ijk = ij + k;
        a[ijk] = w0 * (( p[ijk-1] + p[ijk+1] + p[ijk-js] + p[ijk+js] 
                       + p[ijk-is] + p[ijk+is])/6.0 - h2*b[ijk])
               + w1 * p[ijk];
      }
      ij += js;
    }
    base += is;
  }
}
