#include "Smoother.h"
#include <vector>
#include <algorithm>
#include <iostream>

#include <omp.h>
#include <cmath>


Smoother::Smoother(const DomainDecomp& dd, const UniMesh& mesh):
  nMargin(2),
  _ptDd(&dd),
  _ptMesh(&mesh),
  _model(ParallelModel::BSP_MPI),
  _ht(dd, 2),
  _timer(dd.num_thread()),
  _isSyncComm(false)
{ 
  int nThrd = dd.num_thread();
  int tid   = 0;
  _t = new double* [nThrd];
  #pragma omp parallel num_threads(nThrd), private(tid)
  {
    tid     = omp_get_thread_num();
    _t[tid] = new double [SMOOTHER_N_TIME];
    std::fill(_t[tid], _t[tid]+SMOOTHER_N_TIME, 0.0);
  }
}


void Smoother::set_from_option()
{
  bool isSet;
  get_option("synccomm", CmdOption::Bool, 1, &_isSyncComm, &isSet);
  get_option("ftime",    CmdOption::Str,  1, &_fTime,      &isSet);
  get_option("cbsizes",  CmdOption::Int,  3, &_cbSizes,    &isSet);

  _ht.set_from_option();
}


int Smoother::setup_overlap_range(const int rngs[6])
{
  // inner range
  for (int i=0; i<3; ++i) {
    _inRngs[i]   = rngs[i]   + nMargin;
    _inRngs[i+3] = rngs[i+3] - nMargin;
  }

  // outer range
  // x-
  _outRngs[0][0] = rngs[0];            _outRngs[0][3] = rngs[0] + nMargin;
  _outRngs[0][1] = rngs[1];            _outRngs[0][4] = rngs[4];
  _outRngs[0][2] = rngs[2];            _outRngs[0][5] = rngs[5];
  // y-
  _outRngs[1][0] = rngs[0] + nMargin;  _outRngs[1][3] = rngs[3] - nMargin;
  _outRngs[1][1] = rngs[1];            _outRngs[1][4] = rngs[1] + nMargin;
  _outRngs[1][2] = rngs[2];            _outRngs[1][5] = rngs[5];
  // z-
  _outRngs[2][0] = rngs[0] + nMargin;  _outRngs[2][3] = rngs[3] - nMargin;
  _outRngs[2][1] = rngs[1] + nMargin;  _outRngs[2][4] = rngs[4] - nMargin;
  _outRngs[2][2] = rngs[2];            _outRngs[2][5] = rngs[2] + nMargin;
  // x+
  _outRngs[3][0] = rngs[3] - nMargin;  _outRngs[3][3] = rngs[3];
  _outRngs[3][1] = rngs[1];            _outRngs[3][4] = rngs[4];
  _outRngs[3][2] = rngs[2];            _outRngs[3][5] = rngs[5];
  // y+
  _outRngs[4][0] = rngs[0] + nMargin;  _outRngs[4][3] = rngs[3] - nMargin;
  _outRngs[4][1] = rngs[4] - nMargin;  _outRngs[4][4] = rngs[4];
  _outRngs[4][2] = rngs[2];            _outRngs[4][5] = rngs[5];
  // z+
  _outRngs[5][0] = rngs[0] + nMargin;  _outRngs[5][3] = rngs[3] - nMargin;
  _outRngs[5][1] = rngs[1] + nMargin;  _outRngs[5][4] = rngs[4] - nMargin;
  _outRngs[5][2] = rngs[5] - nMargin;  _outRngs[5][5] = rngs[5];
  
  return 0;
}


int Smoother::setup_funneled_outer_range()
{
  int lx           = (_inRngs[3] - _inRngs[0]) + 2*nMargin;
  int ly           = (_inRngs[4] - _inRngs[1]) + 2*nMargin;
  int lz           = (_inRngs[5] - _inRngs[2]) + 2*nMargin;
  int nThrd        = _ptDd->num_thread();
  int totalOutCell = 2*( ly*lz*nMargin + lz*(lx - 2*nMargin)*nMargin
                       + (lx-2*nMargin)*(ly-2*nMargin)*nMargin);

  // init #cells per thread, handle cases where not evenly divided
  std::vector<int> nThrdCells(nThrd, totalOutCell/nThrd);
  for (int i=0; i<totalOutCell%nThrd; ++i)  ++nThrdCells[i];

  // init chunk range equal outRngs
  int chunkRngs[6][6];
  for (unsigned int i=0; i<6; ++i) 
    std::copy(_outRngs[i], _outRngs[i]+6, chunkRngs[i]);

  _thrdOutRngs.clear();

  // cut direction and normal area
  int cutDirs[6]    = {1, 0, 0, 1, 0, 0};
  int chunkAreas[6] = {lz*nMargin, lz*nMargin, nMargin*(ly-2*nMargin),
                       lz*nMargin, lz*nMargin, nMargin*(ly-2*nMargin)}; 

  // set thread's outer range
  // start from first outer chunk and init cut position
  int ic = 0; // index of chunk
  int chunkCutStart = chunkRngs[ic][cutDirs[ic]];
  //
  for (int tid=0; tid<nThrd; ++tid) {
    // add dummy vector
    std::vector<int> vecNull;
    _thrdOutRngs.push_back(vecNull);

    int thrdRoom = nThrdCells[tid];
    while (thrdRoom > 0) {
      // remainder volume of chunk
      int chunkVol = (chunkRngs[ic][cutDirs[ic]+3] - chunkCutStart) * chunkAreas[ic];
      // if chunk volume fits in thread room
      if (thrdRoom >= chunkVol) {
        thrdRoom -= chunkVol;
        // add the remainding chunk range to thread
        for (int i=0; i<6; ++i) {
          if (i == cutDirs[ic])
            _thrdOutRngs[tid].push_back(chunkCutStart);
          else
            _thrdOutRngs[tid].push_back(chunkRngs[ic][i]);
        }
        // move on to next chunk or terminates
        ++ic;
        if (ic == 6) break;
        // advance cut start pos
        chunkCutStart = chunkRngs[ic][cutDirs[ic]];
      }
      // chunk volume exceeds thread room
      else {
        int cutLen = static_cast<int>( round(static_cast<double>(thrdRoom) 
                                     / chunkAreas[ic]));
        // if thread room is small, note that the last thread's room will 
        // cover remaining chunk, so tid+1 will not overflow
        if (cutLen == 0) {
          nThrdCells[tid+1] += thrdRoom;
          break;
        }
        // append part of chunk to thread space
        thrdRoom -= cutLen * chunkAreas[ic];
        for (int i=0; i<6; ++i) {
          if (i == cutDirs[ic])  
            _thrdOutRngs[tid].push_back(chunkCutStart);
          else if (i == cutDirs[ic]+3) 
            _thrdOutRngs[tid].push_back(chunkCutStart + cutLen);
          else
            _thrdOutRngs[tid].push_back(chunkRngs[ic][i]);
        }
        // advance cut start pos
        chunkCutStart +=  cutLen;
      }
      // if current thread assigned more cells than average
      if (thrdRoom < 0) nThrdCells[tid+1] += thrdRoom;
    }// end while
  }// end for
  
  return 0;
}


int Smoother::setup_funneled_inner_range()
{
  int nThrd    = _ptDd->num_thread();
  int tRngs[6] = {0};

  // first thread is not used in inner computation
  _thrdInRngs.emplace_back(tRngs, tRngs+6);
  // set the thread range of other thread
  for (int tid=0; tid<nThrd-1; ++tid) {
    compute_thread_range(_inRngs, nThrd-1, tid, tRngs);
    _thrdInRngs.emplace_back(tRngs, tRngs+6);
  }

  return 0;
}


int Smoother::setup()
{
  _ht.setup();

  int rngs[6];
  std::vector<int> chunks;
  if (_ptDd->is_neighbor_star()) {
    Halo faces[] = {FaceXm, FaceYm, FaceZm, FaceXp, FaceYp, FaceZp};
    for (int m=0; m<6; ++m) {
      _ht.get_halo_chunk_range(faces[m], rngs);
      chunks.insert(chunks.end(), rngs, rngs+6);
    }//end for m
  }//end if star
  else {
    for (int m=0; m<26; ++m) {
      _ht.get_halo_chunk_range(static_cast<Halo>(m), rngs);
      chunks.insert(chunks.end(), rngs, rngs+6);
    }//end for m
  }
  _ht.set_halo_range(chunks);

  _ht.get_local_range(rngs);
  setup_overlap_range(rngs);
  setup_funneled_inner_range();
  setup_funneled_outer_range();

  _ptDd->get_local_size(rngs);
  int nh = _ht.num_halo();
  _buf = new double [(rngs[0]+2*nh)*(rngs[1]+2*nh)*(rngs[2]+2*nh)];

  return 0;
}


int Smoother::setup_pipeline(int nPipe)
{
  vector<int> chunks;
  int         sizes[3], cRngs[6];

  _ptDd->get_local_size(sizes);

  _nPipe = nPipe;
  _hts   = new HaloTransfer [nPipe];
  for (int i=0; i<nPipe; ++i) _hts[i].init(_ptDd, _ht.num_halo());

  // cut long z axis, assume round number
  int cutAxis = 2, cutBegin = _ht.num_halo(), cutEnd = sizes[cutAxis]/nPipe + _ht.num_halo();
  //..first pipe halo 
  for (int i=0; i<26; ++i) {
    _hts[0].get_halo_chunk_range(static_cast<Halo>(i), cRngs);
    if (cRngs[cutAxis] < cutEnd) {
      cRngs[cutAxis+3] = std::min(cRngs[cutAxis+3], cutEnd);
      chunks.insert(chunks.end(), cRngs, cRngs+6);
    }
  }
  _hts[0].setup(chunks, NULL, NULL);
  //.. middle halos
  for (int i=1; i<nPipe-1; ++i) {
    cutBegin += sizes[cutAxis]/nPipe;
    cutEnd   += sizes[cutAxis]/nPipe;
    chunks.clear();
    for (int j=0; j<26; ++j) {
      _hts[0].get_halo_chunk_range(static_cast<Halo>(j), cRngs);
      if (cRngs[cutAxis+3] > cutEnd && cRngs[cutAxis] < cutBegin) {
        cRngs[cutAxis]   = cutBegin;
        cRngs[cutAxis+3] = cutEnd;
        chunks.insert(chunks.begin(), cRngs, cRngs+6);
      }
    }
    _hts[i].setup(chunks, _hts[0].send(), _hts[0].recv());
  }
  //.. last pipe
  cutBegin += sizes[cutAxis]/nPipe;
  cutEnd   += sizes[cutAxis]/nPipe;
  chunks.clear();
  for (int i=0; i<26; ++i) {
    _hts[0].get_halo_chunk_range(static_cast<Halo>(i), cRngs);
    if (cRngs[cutAxis+3] > cutBegin) {
      cRngs[cutAxis] = std::max(cRngs[cutAxis], cutBegin);
      chunks.insert(chunks.begin(), cRngs, cRngs+6);
    }
  }
  _hts[nPipe-1].setup(chunks, _hts[0].send(), _hts[0].recv());

  nMargin = 0;
  _hts[0].get_local_range(cRngs); // use cRngs to store local range
  setup_overlap_range(cRngs);
  setup_funneled_inner_range();

  return 0;
}


int Smoother::smooth(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, \
                     HaloArray& b, HaloArray& p)
{
  int tid = 0;
#ifdef _OPENMP
  tid = omp_get_thread_num();
#endif
  
  _t[tid][SmootherTime::Total] -= mpi_time();
  switch (_model) {
    case ParallelModel::BSP_MPI:
      _smooth_bsp_mpi(rhox, rhoy, rhoz, b, p);
      break;
    case ParallelModel::BSP_Hybrid:
      _smooth_bsp_hybrid(rhox, rhoy, rhoz, b, p);
      break;
    case ParallelModel::Overlap:
      _smooth_funneled(rhox, rhoy, rhoz, b, p);
      break;
    case ParallelModel::Fused:
      _smooth_fused(rhox, rhoy, rhoz, b, p);
      break;
    case ParallelModel::WaveFront:
      _smooth_wavefront(rhox, rhoy, rhoz, b, p);
      break;
  }
  _t[tid][SmootherTime::Total] += mpi_time();

  return 0;
}


int Smoother::debug_view()
{
  if (_ptDd->is_root()) {
    std::cout << "inner domain:";
    for (int i=0; i<6; i++) std::cout << " " << _inRngs[i];
    std::cout << std::endl;
    std::cout << "outer domain:" << std::endl;
    for (int i=0; i<6; ++i) {
      for (int j=0; j<6; ++j) std::cout << _outRngs[i][j] << " ";
      std::cout << std::endl;
    }
    std::cout << "thread inner range:" << std::endl;
    for (unsigned int i=0; i<_thrdInRngs.size(); ++i) {
      std::cout << "thread " << i <<" :";
      for (int j=0; j<6; ++j) std::cout << " " <<  _thrdInRngs[i][j];
      std::cout << std::endl;
    }
    std::cout << "thread outer range:" << std::endl;
    for (unsigned int i=0; i<_thrdOutRngs.size(); ++i) {
      std::cout << "thread " << i <<" :" << std::endl;
      for (unsigned int j=0; j<_thrdOutRngs[i].size(); j+=6) {
        for (unsigned int k=j; k<j+6; ++k)
          std::cout << _thrdOutRngs[i][k] << " ";
        std::cout << std::endl;
      }
    }
  }

  return 0;
}


Smoother::~Smoother()
{
  if (_t) {
    for (int i=0; i<_ptDd->num_thread(); ++i) delete _t[i];
    delete _t;
  }
  if (_hts) delete [] _hts;
  delete [] _buf;
}


int Smoother::clear_history()
{
  int tid = omp_get_thread_num();
  std::fill(_t[tid], _t[tid]+SMOOTHER_N_TIME, 0.0);
  return 0;
}
