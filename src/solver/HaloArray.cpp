#include "HaloArray.h"


#include "DomainDecomp.h"
#include <iostream>
#include <algorithm>
#include <mpi.h>
#include <omp.h>
#include <cmath>

HaloArray::HaloArray(DomainDecomp& dd, const int nHalo,\
                     const CommMethod method):
  _ptDd(&dd),
  _nHalo(nHalo),
  _method(method)
{ 
  // set local sizes
  dd.get_info(DecompOpt::lSizes, _sizes);

  // allocate data
  int jStrd =  _sizes[2] + 2*_nHalo;
  int iStrd = (_sizes[1] + 2*_nHalo) * jStrd;
  f = new double [iStrd * (_sizes[0] + 2*_nHalo)];

  // allocate send and recv buffer
  MPI_Aint bufSize = ( _sizes[0]*_sizes[1] + _sizes[1]*_sizes[2] \
      + _sizes[2]*_sizes[0]) * 2 * _nHalo;
  if (_shape == HaloShape::box)
    bufSize += 4 * (_sizes[0] + _sizes[1] + _sizes[2]) * _nHalo * _nHalo 
      + 8 * _nHalo * _nHalo * _nHalo;
  _rBuf = new double [bufSize];
  _sBuf = new double [bufSize];

  // thread face halo range
  int nThread  = dd.num_thread();
  std::vector<int> vecNull;
  _faceHaloRngs.insert(_faceHaloRngs.begin(), nThread, vecNull);
  _faceRngs = _faceHaloRngs;
  _bufStarts.insert(_bufStarts.begin(), nThread, 0);

  // numa init and set face halo range
  int tRngs[6] = {0, 0, 0, _sizes[0], _sizes[1], _sizes[2]};
#ifdef _OPENMP
#pragma omp parallel num_threads(nThread), private(tRngs)
  {
    int tid = omp_get_thread_num();
    dd.get_thread_range(tid, tRngs);
    // include halo region
    for (int i=0; i<3; ++i) {
      if (tRngs[i] == 0)  tRngs[i] = -_nHalo;
      if (tRngs[i+3] == _sizes[i])  tRngs[i+3] += _nHalo;
    }
#endif
    for (int i=tRngs[0]+_nHalo; i<tRngs[3]+_nHalo; ++i) {
      for (int j=tRngs[1]+_nHalo; j<tRngs[4]+_nHalo; ++j) {
        for (int k=tRngs[2]+_nHalo; k<tRngs[5]+_nHalo; ++k) {
          f[k + j * jStrd + i * iStrd] = 0.0;
        }
      }
    }
#ifdef _OPENMP
    _set_face_halo_range_seg(tid);
  }
#endif

  // request for p2p
  MPI_Request req;
  if (dd.is_neighbor_star())
    _reqs.insert(_reqs.begin(), 12, req);
  else
    _reqs.insert(_reqs.begin(), 52, req);
}


HaloArray::~HaloArray()
{
  delete [] f;
  delete [] _sBuf;
  delete [] _rBuf;
}


int HaloArray::get_size(int sizes[3]) const
{
  std::copy(_sizes, _sizes+3, sizes);
  return 0;
}


void HaloArray::get_local_range(int rngs[6]) const
{
  rngs[0] = _nHalo; rngs[3] = _nHalo + _sizes[0];
  rngs[1] = _nHalo; rngs[4] = _nHalo + _sizes[1];
  rngs[2] = _nHalo; rngs[5] = _nHalo + _sizes[2];
}


int HaloArray::get_thread_range(const int tid, int tRngs[6]) const
{
  _ptDd->get_thread_range(tid, tRngs);
  for (int i=0; i<6; ++i)  tRngs[i] += _nHalo;
  return 0;
}


int HaloArray::get_thread_range(const int nThrd, const int tid, int tRngs[6])
{
  // set local range, start from 0
  int lRngs[6];
  for (int i=0; i<3; ++i) {
    lRngs[i+3] = _sizes[i] + _nHalo;
    lRngs[i]   = _nHalo;
  }

  compute_thread_range(lRngs, nThrd, tid, tRngs);

  return 0;
}


void HaloArray::get_thread_range_dh(const int tid, const int nh, int tRngs[6])
{
  int sizes[3];
  _ptDd->get_local_size(sizes);

  _ptDd->get_thread_range(tid, tRngs);
  for (int i=0; i<3; ++i) {
    if (tRngs[i] == 0)  tRngs[i] = -nh;
    if (tRngs[i+3] == _sizes[i])  tRngs[i+3] += nh;
  } 
  for (int i=0; i<6; ++i)  tRngs[i] += _nHalo;
}


int HaloArray::get_halo_range(const int i, int rngs[6]) const
{
  switch (i) {
    case 0: 
      rngs[0] = 0;                  rngs[3] = _nHalo;
      rngs[1] = _nHalo;             rngs[4] = _sizes[1]+_nHalo;
      rngs[2] = _nHalo;             rngs[5] = _sizes[2]+_nHalo; break;
    case 1:
      rngs[0] = _nHalo;             rngs[3] = _sizes[0]+_nHalo;
      rngs[1] = 0;                  rngs[4] = _nHalo;
      rngs[2] = _nHalo;             rngs[5] = _sizes[2]+_nHalo; break;
    case 2:
      rngs[0] = _nHalo;             rngs[3] = _sizes[0]+_nHalo;
      rngs[1] = _nHalo;             rngs[4] = _sizes[1]+_nHalo;
      rngs[2] = 0;                  rngs[5] = _nHalo; break;
    case 3:
      rngs[0] = _sizes[0]+_nHalo;   rngs[3] = _sizes[0]+2*_nHalo;
      rngs[1] = _nHalo;             rngs[4] = _sizes[1]+  _nHalo;
      rngs[2] = _nHalo;             rngs[5] = _sizes[2]+  _nHalo; break;
    case 4:
      rngs[0] = _nHalo;             rngs[3] = _sizes[0]+  _nHalo;
      rngs[1] = _sizes[1]+_nHalo;   rngs[4] = _sizes[1]+2*_nHalo;
      rngs[2] = _nHalo;             rngs[5] = _sizes[2]+  _nHalo; break;
    case 5:
      rngs[0] = _nHalo;             rngs[3] = _sizes[0]+  _nHalo;
      rngs[1] = _nHalo;             rngs[4] = _sizes[1]+  _nHalo;
      rngs[2] = _sizes[2]+_nHalo;   rngs[5] = _sizes[2]+2*_nHalo; break;
    case 6:
      rngs[0] = _nHalo;             rngs[3] = _sizes[0] + _nHalo;
      rngs[1] = 0;                  rngs[4] = _nHalo;
      rngs[2] = 0;                  rngs[5] = _nHalo; break;
    case 7:
      rngs[0] = _nHalo;             rngs[3] = _sizes[0] + _nHalo;
      rngs[1] = _sizes[1] + _nHalo; rngs[4] = _sizes[1] + 2*_nHalo;
      rngs[2] = 0;                  rngs[5] = _nHalo; break;
    case 8:
      rngs[0] = _nHalo;             rngs[3] = _sizes[0] + _nHalo;
      rngs[1] = 0;                  rngs[4] = _nHalo;
      rngs[2] = _sizes[2] + _nHalo; rngs[5] = _sizes[2] + 2*_nHalo; break;
    case 9:
      rngs[0] = _nHalo;             rngs[3] = _sizes[0] + _nHalo;
      rngs[1] = _sizes[1] + _nHalo; rngs[4] = _sizes[1] + 2*_nHalo;
      rngs[2] = _sizes[2] + _nHalo; rngs[5] = _sizes[2] + 2*_nHalo; break;
    case 10:
      rngs[0] = 0;                  rngs[3] = _nHalo;
      rngs[1] = _nHalo;             rngs[4] = _sizes[1] + _nHalo;
      rngs[2] = 0;                  rngs[5] = _nHalo; break;
    case 11:
      rngs[0] = 0;                  rngs[3] = _nHalo;
      rngs[1] = _nHalo;             rngs[4] = _sizes[1] + _nHalo;
      rngs[2] = _sizes[2] + _nHalo; rngs[5] = _sizes[2] + 2*_nHalo; break;
    case 12:
      rngs[0] = _sizes[0] + _nHalo; rngs[3] = _sizes[0] + 2*_nHalo;
      rngs[1] = _nHalo;             rngs[4] = _sizes[1] + _nHalo;
      rngs[2] = 0;                  rngs[5] = _nHalo; break;
    case 13:
      rngs[0] = _sizes[0] + _nHalo; rngs[3] = _sizes[0] + 2*_nHalo;
      rngs[1] = _nHalo;             rngs[4] = _sizes[1] + _nHalo;
      rngs[2] = _sizes[2] + _nHalo; rngs[5] = _sizes[2] + 2*_nHalo; break;
    case 14:
      rngs[0] = 0;                  rngs[3] = _nHalo;
      rngs[1] = 0;                  rngs[4] = _nHalo;
      rngs[2] = _nHalo;             rngs[5] = _sizes[2] + _nHalo; break;
    case 15:
      rngs[0] = _sizes[0] + _nHalo; rngs[3] = _sizes[0] + 2*_nHalo;
      rngs[1] = 0;                  rngs[4] = _nHalo;
      rngs[2] = _nHalo;             rngs[5] = _sizes[2] + _nHalo; break;
    case 16:
      rngs[0] = 0;                  rngs[3] = _nHalo;
      rngs[1] = _sizes[1] + _nHalo; rngs[4] = _sizes[1] + 2*_nHalo;
      rngs[2] = _nHalo;             rngs[5] = _sizes[2] + _nHalo; break;
    case 17:
      rngs[0] = _sizes[0] + _nHalo; rngs[3] = _sizes[0] + 2*_nHalo;
      rngs[1] = _sizes[1] + _nHalo; rngs[4] = _sizes[1] + 2*_nHalo;
      rngs[2] = _nHalo;             rngs[5] = _sizes[2] + _nHalo; break;
    case 18:
      rngs[0] = 0;                  rngs[3] = _nHalo;
      rngs[1] = 0;                  rngs[4] = _nHalo;
      rngs[2] = 0;                  rngs[5] = _nHalo; break;
    case 19:
      rngs[0] = 0;                  rngs[3] = _nHalo;
      rngs[1] = 0;                  rngs[4] = _nHalo;
      rngs[2] = _sizes[2] + _nHalo; rngs[5] = _sizes[2] + 2*_nHalo; break;
    case 20:
      rngs[0] = 0;                  rngs[3] = _nHalo;
      rngs[1] = _sizes[1] + _nHalo; rngs[4] = _sizes[1] + 2*_nHalo;
      rngs[2] = 0;                  rngs[5] = _nHalo; break;
    case 21:
      rngs[0] = 0;                  rngs[3] = _nHalo;
      rngs[1] = _sizes[1] + _nHalo; rngs[4] = _sizes[1] + 2*_nHalo;
      rngs[2] = _sizes[2] + _nHalo; rngs[5] = _sizes[2] + 2*_nHalo; break;
    case 22:
      rngs[0] = _sizes[0] + _nHalo; rngs[3] = _sizes[0] + 2*_nHalo;
      rngs[1] = 0;                  rngs[4] = _nHalo;
      rngs[2] = 0;                  rngs[5] = _nHalo; break;
    case 23:
      rngs[0] = _sizes[0] + _nHalo; rngs[3] = _sizes[0] + 2*_nHalo;
      rngs[1] = 0;                  rngs[4] = _nHalo;
      rngs[2] = _sizes[2] + _nHalo; rngs[5] = _sizes[2] + 2*_nHalo; break;
    case 24:
      rngs[0] = _sizes[0] + _nHalo; rngs[3] = _sizes[0] + 2*_nHalo;
      rngs[1] = _sizes[1] + _nHalo; rngs[4] = _sizes[1] + 2*_nHalo;
      rngs[2] = 0;                  rngs[5] = _nHalo; break;
    case 25:
      rngs[0] = _sizes[0] + _nHalo; rngs[3] = _sizes[0] + 2*_nHalo;
      rngs[1] = _sizes[1] + _nHalo; rngs[4] = _sizes[1] + 2*_nHalo;
      rngs[2] = _sizes[2] + _nHalo; rngs[5] = _sizes[2] + 2*_nHalo; break;
  }

  return 0;
}


int HaloArray::pack_face_halo()
{
  _pack_face_halo_seg();
  return 0;
}


int HaloArray::unpack_face_halo()
{
  _unpack_face_halo_seg();
  return 0;
}


int HaloArray::update_halo_begin()
{
  return 0;
}


int HaloArray::update_halo_end()
{
  return 0;
}


int HaloArray::update_face_halo_begin()
{
  int fSizes[6] = {_nHalo*_sizes[1]*_sizes[2], _sizes[0]*_nHalo*_sizes[2], \
                   _sizes[0]*_sizes[1]*_nHalo, _nHalo*_sizes[1]*_sizes[2], \
                   _sizes[0]*_nHalo*_sizes[2], _sizes[0]*_sizes[1]*_nHalo};
  MPI_Comm comm =  _ptDd->comm();
  int fNbrs[6]  = {_ptDd->neighbor(4),  _ptDd->neighbor(10), _ptDd->neighbor(12), 
                   _ptDd->neighbor(13), _ptDd->neighbor(15), _ptDd->neighbor(21)};

  int offset = 0;
  for (int i=0; i<6; ++i) {
    MPI_Isend(_sBuf+offset, fSizes[i], MPI_DOUBLE, fNbrs[i], (i+3)%6, comm, &_reqs[2*i]);
    MPI_Irecv(_rBuf+offset, fSizes[i], MPI_DOUBLE, fNbrs[i], i, comm, &_reqs[2*i+1]);
    offset += fSizes[i];
  }

  return 0;
}


int HaloArray::update_face_halo_end()
{
  MPI_Waitall(12, &_reqs[0], MPI_STATUSES_IGNORE);
  return 0;
}


int HaloArray::update_face_halo()
{
  update_face_halo_begin();
  update_face_halo_end();
  return 0;
}


int HaloArray::debug_view()
{
  std::cout << "face halo range " << std::endl;
  for (unsigned int i=0; i<_faceRngs.size(); ++i) {
    std:: cout << "thread " << i << std::endl;
    for (unsigned int j=0; j<_faceRngs[i].size(); j+=6) {
      for (unsigned k=0; k<6; ++k)
        std::cout << _faceRngs[i][j+k] << "\t";
      std::cout << std::endl;
    }
  }
  return 0;
}


void HaloArray::_set_face_halo_range_seg(const int tid)
{
  // average halo cells per thread
  int totSize = ( _sizes[0]*_sizes[1] + _sizes[1]*_sizes[2] \
                + _sizes[2]*_sizes[0] ) * _nHalo * 2;
  int nThrd   = _ptDd->num_thread();
  int avgSize =  static_cast<int>(round(static_cast<double>(totSize)/nThrd));

  // face halo sizes 
  int fSizes[6][3] = {{_nHalo, _sizes[1], _sizes[2]},  {_sizes[0], _nHalo, _sizes[2]},
    {_sizes[0], _sizes[1], _nHalo}, {_nHalo, _sizes[1], _sizes[2]},  
    {_sizes[0], _nHalo, _sizes[2]}, {_sizes[0], _sizes[1], _nHalo}};
  // face halo start
  int fStarts[6][3]= {{0, _nHalo, _nHalo}, {_nHalo, 0, _nHalo}, {_nHalo, _nHalo, 0},
    {_sizes[0]+_nHalo, _nHalo, _nHalo},{_nHalo, _sizes[1]+_nHalo, _nHalo},
    {_nHalo, _nHalo, _sizes[2]+_nHalo}};

  // each thread's init copy range
  // copy start index and face
  int copyStart = tid * avgSize, sFace = 0; 
  for (int i=0; i<6; ++i) {
    // current face halo #cell
    int nFaceCell = fSizes[i][0] * fSizes[i][1] * fSizes[i][2];
    if (copyStart < nFaceCell) break;
    copyStart -= nFaceCell;
    ++sFace;
  }
  // copy end index and face
  int copyEnd = (tid == nThrd-1 ? totSize : (tid + 1) * avgSize), eFace = 0;
  for (int i=0; i<6; ++i) {
    int nFaceCell = fSizes[i][0] * fSizes[i][1] * fSizes[i][2];// current # halo cell
    if (copyEnd <= nFaceCell) break;
    copyEnd -= nFaceCell;
    ++eFace;
  }

  // align copy start, end a full slice of face halo
  // range is [copyStart, copyEnd)
  int r = copyStart % fSizes[sFace][2]; // remainder
  copyStart += r < fSizes[sFace][2]/2 ? -r : fSizes[sFace][2] - r;
  if (copyStart == fSizes[sFace][0]*fSizes[sFace][1]*fSizes[sFace][2]) {
    copyStart = 0;
    ++sFace;
  }
  r = copyEnd % fSizes[eFace][2];
  copyEnd += (r < fSizes[eFace][2]/2 ? -r : fSizes[eFace][2] - r);
  if (copyEnd == 0) {
    --eFace;
    copyEnd = fSizes[eFace][0]*fSizes[eFace][1]*fSizes[eFace][2];
  }

  // start position in buffer
  for (int i=0; i<sFace; ++i)
    _bufStarts[tid] += fSizes[i][0]*fSizes[i][1]*fSizes[i][2];
  _bufStarts[tid] += copyStart;

  // save the copy range to _fRngs
  // (x,y) for copy start and end, ie must be reached, je cannot be reached
  int is = copyStart / fSizes[sFace][2] / fSizes[sFace][1];
  int js = copyStart / fSizes[sFace][2] % fSizes[sFace][1];
  int ie = copyEnd   / fSizes[eFace][2] / fSizes[eFace][1];
  int je = copyEnd   / fSizes[eFace][2] % fSizes[eFace][1];

  // set the face halo range
  if (eFace == sFace) {
    if (is == ie) {
      _faceHaloRngs[tid].push_back(fStarts[sFace][0] + is);
      _faceHaloRngs[tid].push_back(fStarts[sFace][1] + js);
      _faceHaloRngs[tid].push_back(fStarts[sFace][2]);
      _faceHaloRngs[tid].push_back(fStarts[sFace][0] + ie + 1);
      _faceHaloRngs[tid].push_back(fStarts[sFace][1] + je);
      _faceHaloRngs[tid].push_back(fStarts[sFace][2] + fSizes[sFace][2]);
    }
    else if (is < ie) {
      // add slice i=is, if only part of slice is used i.e. js>0
      // or all used but the next slice is 'ie' and partly used.
      if (js > 0 || (js == 0 && is+1 == ie && je < fSizes[sFace][1])) {
        _faceHaloRngs[tid].push_back(fStarts[sFace][0] + is);
        _faceHaloRngs[tid].push_back(fStarts[sFace][1] + js);
        _faceHaloRngs[tid].push_back(fStarts[sFace][2]);
        _faceHaloRngs[tid].push_back(fStarts[sFace][0] + is + 1);
        _faceHaloRngs[tid].push_back(fStarts[sFace][1] + fSizes[sFace][1]);
        _faceHaloRngs[tid].push_back(fStarts[sFace][2] + fSizes[sFace][2]);
      }
      // on slices between (is, ie), include i=is if js==0 if fully used,
      // include i=ie if fully used
      if (ie - is >= 2) {
        if (js > 0)
          _faceHaloRngs[tid].push_back(fStarts[sFace][0] + is + 1);
        else // js == 0
          _faceHaloRngs[tid].push_back(fStarts[sFace][0] + is);
        _faceHaloRngs[tid].push_back(fStarts[sFace][1]);
        _faceHaloRngs[tid].push_back(fStarts[sFace][2]);
        if (je == fSizes[sFace][1])
          _faceHaloRngs[tid].push_back(fStarts[sFace][0] + ie + 1);
        else
          _faceHaloRngs[tid].push_back(fStarts[sFace][0] + ie);
        _faceHaloRngs[tid].push_back(fStarts[sFace][1] + fSizes[sFace][1]);
        _faceHaloRngs[tid].push_back(fStarts[sFace][2] + fSizes[sFace][2]);
      }
      // on slice i=ie, include i=is if it's the last slice and both
      // are fully used
      if (je > 0) {
        if (is+1 == ie && js == 0 && je == fSizes[sFace][1])
          _faceHaloRngs[tid].push_back(fStarts[sFace][0] + is);
        else
          _faceHaloRngs[tid].push_back(fStarts[sFace][0] + ie);
        _faceHaloRngs[tid].push_back(fStarts[sFace][1]);
        _faceHaloRngs[tid].push_back(fStarts[sFace][2]);
        _faceHaloRngs[tid].push_back(fStarts[sFace][0] + ie + 1);
        _faceHaloRngs[tid].push_back(fStarts[sFace][1] + je);
        _faceHaloRngs[tid].push_back(fStarts[sFace][2] + fSizes[sFace][2]);
      }
    }
  }
  else if (eFace > sFace) {
    // slice i=is, included if partly used or this is the only slice in sFace.
    if (js > 0 || is == fSizes[sFace][0] - 1) {
      _faceHaloRngs[tid].push_back(fStarts[sFace][0] + is);
      _faceHaloRngs[tid].push_back(fStarts[sFace][1] + js);
      _faceHaloRngs[tid].push_back(fStarts[sFace][2]);
      _faceHaloRngs[tid].push_back(fStarts[sFace][0] + is + 1);
      _faceHaloRngs[tid].push_back(fStarts[sFace][1] + fSizes[sFace][1]);
      _faceHaloRngs[tid].push_back(fStarts[sFace][2] + fSizes[sFace][2]);
    }
    // slices i>is, if js==0 then slice i=is is included here
    if (is < fSizes[sFace][0] - 1) {
      if (js > 0) 
        _faceHaloRngs[tid].push_back(fStarts[sFace][0] + is + 1);
      else
        _faceHaloRngs[tid].push_back(fStarts[sFace][0] + is);
      _faceHaloRngs[tid].push_back(fStarts[sFace][1]);
      _faceHaloRngs[tid].push_back(fStarts[sFace][2]);
      _faceHaloRngs[tid].push_back(fStarts[sFace][0] + fSizes[sFace][0]);
      _faceHaloRngs[tid].push_back(fStarts[sFace][1] + fSizes[sFace][1]);
      _faceHaloRngs[tid].push_back(fStarts[sFace][2] + fSizes[sFace][2]);
    }
    // faces between (sFace, eFace)
    if (eFace - sFace >= 2) {
      for (int face=sFace+1; face<=eFace-1; ++face) {
        _faceHaloRngs[tid].push_back(fStarts[face][0]);
        _faceHaloRngs[tid].push_back(fStarts[face][1]);
        _faceHaloRngs[tid].push_back(fStarts[face][2]);
        _faceHaloRngs[tid].push_back(fStarts[face][0] + fSizes[face][0]);
        _faceHaloRngs[tid].push_back(fStarts[face][1] + fSizes[face][1]);
        _faceHaloRngs[tid].push_back(fStarts[face][2] + fSizes[face][2]);
      }
    }
    // slice i<ie
    if (ie > 0) {
      _faceHaloRngs[tid].push_back(fStarts[eFace][0]);
      _faceHaloRngs[tid].push_back(fStarts[eFace][1]);
      _faceHaloRngs[tid].push_back(fStarts[eFace][2]);
      if (je == fSizes[eFace][1])
        _faceHaloRngs[tid].push_back(fStarts[eFace][0] + ie + 1);
      else
        _faceHaloRngs[tid].push_back(fStarts[eFace][0] + ie);
      _faceHaloRngs[tid].push_back(fStarts[eFace][1] + fSizes[eFace][1]);
      _faceHaloRngs[tid].push_back(fStarts[eFace][2] + fSizes[eFace][2]);
    }
    // slice i=ie, if it is partly used or the only slice in eFace
    if ( je > 0 && (je < fSizes[eFace][1] || ie == 0)) {
      _faceHaloRngs[tid].push_back(fStarts[eFace][0] + ie);
      _faceHaloRngs[tid].push_back(fStarts[eFace][1]);
      _faceHaloRngs[tid].push_back(fStarts[eFace][2]);
      _faceHaloRngs[tid].push_back(fStarts[eFace][0] + ie + 1);
      _faceHaloRngs[tid].push_back(fStarts[eFace][1] + je);
      _faceHaloRngs[tid].push_back(fStarts[eFace][2] + fSizes[eFace][2]);
    }
  }

  // set the face range
  _faceRngs[tid] = _faceHaloRngs[tid];
  for (unsigned int m=0; m<_faceHaloRngs[tid].size(); m+=6) {
    for (unsigned int i=0; i<3; ++i) {
      if (_faceHaloRngs[tid][m+i] < _nHalo) {
        _faceRngs[tid][m+i]   += _nHalo;
        _faceRngs[tid][m+i+3] += _nHalo;
      }
      else if (_faceHaloRngs[tid][m+i] >= _sizes[i]+_nHalo) {
        _faceRngs[tid][m+i]   -= _nHalo;
        _faceRngs[tid][m+i+3] -= _nHalo;
      }
    }
  }
}


void HaloArray::_pack_face_halo_seg()
{
  int jStrd    = _sizes[2] + 2*_nHalo; 
  int iStrd    = jStrd * (_sizes[1] + 2*_nHalo);
  int tid      = omp_get_thread_num();
  int bufStart = _bufStarts[tid];

  for (int m = 0; m < (int)_faceRngs[tid].size(); m += 6) {
    for (int i = _faceRngs[tid][m]; i < _faceRngs[tid][m+3]; ++i) {
      for (int j = _faceRngs[tid][m+1]; j < _faceRngs[tid][m+4]; ++j) {
        double *base = f + i*iStrd + j*jStrd;
        std::copy(base + _faceRngs[tid][m+2], base + _faceRngs[tid][m+5], \
                  _sBuf + bufStart);
        bufStart += _faceRngs[tid][m+5] - _faceRngs[tid][m+2];
      }
    }
  }
}


void HaloArray::_unpack_face_halo_seg()
{
  int jStrd    = _sizes[2] + 2*_nHalo; 
  int iStrd    = jStrd * (_sizes[1] + 2*_nHalo);
  int tid      = omp_get_thread_num();
  int bufStart = _bufStarts[tid];

  for (unsigned int m = 0; m <_faceHaloRngs[tid].size(); m += 6) {
    // length of z in face halo
    int zLen = _faceHaloRngs[tid][m+5] - _faceHaloRngs[tid][m+2];
    for (int i = _faceHaloRngs[tid][m]; i < _faceHaloRngs[tid][m+3]; ++i) {
      for (int j = _faceHaloRngs[tid][m+1]; j < _faceHaloRngs[tid][m+4]; ++j) {
        std::copy(_rBuf + bufStart, _rBuf + bufStart + zLen, \
                  f + i*iStrd + j*jStrd + _faceHaloRngs[tid][m+2]);
        bufStart += zLen;
      }
    }
  }
}
