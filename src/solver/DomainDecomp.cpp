#include "sys.h"
#include "DomainDecomp.h"

#include <cmath>
#include <algorithm>
#include <cassert>


DomainDecomp::DomainDecomp(const int n[3],    const int np[3], const bool isPrds[3],
                           const int nThread, const MPI_Comm comm, const Neighbor shape):
  _gSizes{n[0],  n[1],  n[2]},
  _pSizes{np[0], np[1], np[2]},
  _pSizesNode{1,1,1},
  _isPrds{isPrds[0], isPrds[1], isPrds[2]},
  _nThreads{1, 1, nThread},
  _isThreadSet(false),
  _comm(comm),
  _shape(shape),
  _isUsingMPICart(false)
{}


void DomainDecomp::set_from_option()
{
  bool isSet, isBox;

  get_option("dd_size",    CmdOption::Int,  3, _gSizes,          &isSet); 
  get_option("dd_proc",    CmdOption::Int,  3, _pSizes,          &isSet); 
  get_option("dd_node",    CmdOption::Int,  3, _pSizesNode,      &isSet); 
  get_option("dd_nthread", CmdOption::Int,  1, _nThreads+2,      &isSet); 
  get_option("dd_nthreads", CmdOption::Int,  3, _nThreads,      &_isThreadSet); 
  get_option("dd_mpicart", CmdOption::Bool, 1, &_isUsingMPICart, &isSet); 

  get_option("dd_box",     CmdOption::Bool, 1, &isBox,           &isSet);
  if (isSet) _shape = Neighbor::box;
}


void DomainDecomp::setup()
{
  int isPrds[3] = {(int)_isPrds[0], (int)_isPrds[1], (int)_isPrds[2]};
  // set rank, coordinates
  if (_isUsingMPICart) {
    MPI_Cart_create(MPI_COMM_WORLD, 3, _pSizes, isPrds, 1, &_comm);
    MPI_Comm_rank(_comm, &_rank);
    MPI_Cart_coords(_comm, _rank, 3, _coords);
  }
  else {
    MPI_Comm_rank(_comm, &_rank);
    _rank_to_coords(_rank, _coords);
  } 
  // local range
  for (int i=0; i<3; ++i) {
    _lRanges[i]   = _coords[i]  * _gSizes[i] / _pSizes[i];
    _lRanges[i+3] = _lRanges[i] + _gSizes[i] / _pSizes[i];
  }
  //
  _set_neighbors();
}


void DomainDecomp::config_thread(const int nThreads[3])
{
  std::copy(nThreads, nThreads+3, _nThreads);
  _isThreadSet = true;
}


void DomainDecomp::get_info(DecompOpt opt, void* ptVal) const
{
  switch (opt) {
    // glocal sizes
    case DecompOpt::gSizes:
      std::copy(_gSizes, _gSizes+3, reinterpret_cast<int*>(ptVal));
      break;
    // process sizes
    case DecompOpt::pSizes:
      std::copy(_pSizes, _pSizes+3, reinterpret_cast<int*>(ptVal));
      break;
    // local Ranges
    case DecompOpt::lRanges:
      std::copy(_lRanges, _lRanges+6, reinterpret_cast<int*>(ptVal));
      break;
    // local Ranges
    case DecompOpt::lSizes:
      *(reinterpret_cast<int*>(ptVal))   = _lRanges[3] - _lRanges[0];
      *(reinterpret_cast<int*>(ptVal)+1) = _lRanges[4] - _lRanges[1];
      *(reinterpret_cast<int*>(ptVal)+2) = _lRanges[5] - _lRanges[2];
      break;
    // #threads
    case DecompOpt::nThread:
      *reinterpret_cast<int*>(ptVal) = _nThreads[2];
      break;
  }
}


void DomainDecomp::get_global_size(int gSizes[3]) const
{
  std::copy(_gSizes, _gSizes+3, gSizes);
}


void DomainDecomp::get_local_start(int starts[3]) const
{
  std::copy(_lRanges, _lRanges+3, starts);
}


void DomainDecomp::get_local_size(int sizes[3]) const
{
  sizes[0] = _lRanges[3] - _lRanges[0];
  sizes[1] = _lRanges[4] - _lRanges[1];
  sizes[2] = _lRanges[5] - _lRanges[2];
}


void DomainDecomp::get_thread_range(const int tid, int thrdRange[6]) const
{
  if (_isThreadSet)
    compute_thread_range(_lRanges, _nThreads, tid, thrdRange);
  else
    compute_thread_range(_lRanges, _nThreads[2], tid, thrdRange);

  // shift to local index
  for (int i=0; i<3; ++i) {
    thrdRange[i]   -= _lRanges[i];
    thrdRange[i+3] -= _lRanges[i];
  }
}


void DomainDecomp::debug_view() const
{
  int nProc = _pSizes[0]*_pSizes[1]*_pSizes[2];
  for (int i=0; i<std::min(nProc, 16); ++i) {
    if (_rank == i) {
      // rank and coords
      std::cout << "rank " << _rank << " coords " << _coords[0] << " " 
                << _coords[1] << " " << _coords[2] << std::endl;
      // local range
      std::cout << "range";
      for (int j=0; j<6; ++j) std::cout << " " << _lRanges[j];
      std::cout << std::endl;
      // face nbrs
      std::cout << "nbrs";
      for (int j=0; j<26; ++j) std::cout << " " << _nbrs[j];
      std::cout << std::endl;
      std::cout << std::endl;
    }
    MPI_Barrier(MPI_COMM_WORLD);
  }
}


void DomainDecomp::_set_neighbors()
{
  int iNbr = 0;
  std::fill(_nbrs, _nbrs+26, MPI_PROC_NULL);

  for (int i=-1; i<=1; ++i) {
    for (int j=-1; j<=1; ++j) {
      for (int k=-1; k<=1; ++k) {
        // by default nbr is not null
        bool isNbrNull = false;

        // skip process itself
        if (i == 0 && j == 0 && k == 0) continue;

        // set default nbr coordinates
        int nbrCoords[3];
        nbrCoords[0] = _coords[0] + i;
        nbrCoords[1] = _coords[1] + j;
        nbrCoords[2] = _coords[2] + k;
        // traverse x, y, z to check priodicity
        for (int iDir=0; iDir<3; ++iDir) {
          if (nbrCoords[iDir] == -1) {
            if (_isPrds[iDir])
              nbrCoords[iDir] += _pSizes[iDir];
            else
              isNbrNull = true;
          }
          else if (nbrCoords[iDir] == _pSizes[iDir]) {
            if (_isPrds[iDir])
              nbrCoords[iDir] -= _pSizes[iDir];
            else
              isNbrNull = true;
          }
        }

        // set neighbor rank
        if (isNbrNull) {
          _nbrs[iNbr] = MPI_PROC_NULL;
        }
        else {
          if (_isUsingMPICart) 
            MPI_Cart_rank(_comm, nbrCoords, _nbrs+iNbr);
          else
            _coords_to_rank(nbrCoords, _nbrs+iNbr);
        }

        ++iNbr;
      }
    }//end for j
  }//end for i
}


void DomainDecomp::_rank_to_coords(const int rank, int coords[3]) const
{
  // C order, start from z
  if (_pSizesNode[0] > 1 || _pSizesNode[1] > 1 || _pSizesNode[2] > 1) {
    // node's coordinates
    int nodeID = rank / _pSizesNode[0] / _pSizesNode[1] / _pSizesNode[2];
    coords[0] = nodeID / (_pSizes[2]/_pSizesNode[2]) / (_pSizes[1]/_pSizesNode[1]);
    coords[1] = nodeID / (_pSizes[2]/_pSizesNode[2]) % (_pSizes[1]/_pSizesNode[1]);
    coords[2] = nodeID % (_pSizes[2]/_pSizesNode[2]);
    // proc's coordinates
    int pid = rank % (_pSizesNode[0]*_pSizesNode[1]*_pSizesNode[2]);
    coords[0] = coords[0] * _pSizesNode[0] +  pid / _pSizesNode[2] / _pSizesNode[1];
    coords[1] = coords[1] * _pSizesNode[1] +  pid / _pSizesNode[2] % _pSizesNode[1];
    coords[2] = coords[2] * _pSizesNode[2] +  pid % _pSizesNode[2];
  }
  else {
    coords[2] = rank % _pSizes[2];
    coords[0] = rank / _pSizes[2] / _pSizes[1];
    coords[1] = rank / _pSizes[2] - coords[0] * _pSizes[1];
  }
}


void DomainDecomp::_coords_to_rank(const int x[3], int* ptRank) const
{
  // C order, start from z
  if (_pSizesNode[0] > 1 || _pSizesNode[1] > 1 || _pSizesNode[2] > 1) {
    int coords[3];
    for (int i=0; i<3; ++i) coords[i] = x[i] % _pSizesNode[i];
    *ptRank =  coords[0] * _pSizesNode[1] * _pSizesNode[2] 
            +  coords[1] * _pSizesNode[2] + coords[2];
    *ptRank += (x[0] - coords[0]) * _pSizes[1] * _pSizes[2];
    *ptRank += (x[1] - coords[1]) * _pSizesNode[0] * _pSizes[2];
    *ptRank += (x[2] - coords[2]) * _pSizesNode[0] * _pSizesNode[1];
  }
  else {
    *ptRank = x[0] * _pSizes[1] * _pSizes[2] + x[1] * _pSizes[2] + x[2];
  }
}


int decompose_threads_2d(int nThread, int len0, int len1, int* ptNThrd0, int* ptNThrd1)
{
  // find the closest integer that decompose the domain according to length ratio
  double lenRatio = (double)len0 / len1;
  *ptNThrd0 = (int)round(sqrt(lenRatio * nThread));

  // limit the integer in [1, nThread]
  *ptNThrd0 = std::min(std::max(*ptNThrd0, 1), nThread);

  // find floor and ceil integer of *ptNThrd0 that can divide nThread
  int nThread0Floor = *ptNThrd0, nThread0Ceil  = *ptNThrd0;
  for (nThread0Floor = *ptNThrd0; nThread0Floor > 0; --nThread0Floor) 
    if (nThread % nThread0Floor == 0) break;
  for (nThread0Ceil  = *ptNThrd0; nThread0Ceil <= nThread; ++nThread0Ceil)
    if (nThread % nThread0Ceil  == 0) break;

  // choose the closest integer between floor and ceil
  *ptNThrd0 = (*ptNThrd0 - nThread0Floor < nThread0Ceil - *ptNThrd0) \
           ? nThread0Floor : nThread0Ceil;
  *ptNThrd1 = nThread / *ptNThrd0;

  return 0;
}


int compute_thread_range(const int range[6], const int nThread, const int threadID,
                         int threadRange[6])
{
  int len[3] = {range[3] - range[0], range[4] - range[1], range[5] - range[2]};
  int nThreads[3];

  // decompose in x and y
  decompose_threads_2d(nThread, len[0], len[1], nThreads, nThreads+1);
  nThreads[2] = 1;
  // check if block is too small for given # threads
  assert(nThreads[0] <= len[0] && nThreads[1] <= len[1] && nThreads[2] <= len[2]);

  compute_thread_range(range, nThreads, threadID, threadRange);

  return 0;
}


int compute_thread_range(const int range[6], const int nThreads[3], const int threadID, 
                         int threadRange[6])
{
  int len[3] = {range[3] - range[0], range[4] - range[1], range[5] - range[2]};

  int threadCoords[3];
  threadCoords[2] = threadID / (nThreads[0] * nThreads[1]);
  threadCoords[0] = threadID % nThreads[0];
  threadCoords[1] = (threadID - threadCoords[2]*nThreads[0]*nThreads[1]) / nThreads[0];

  for (int i=0; i<3; ++i) {
    // floor size of thread's range in i dirction
    int sizeFloor = len[i] / nThreads[i]; 
    // # intervels of ceiling size
    int nCeil     = len[i] % nThreads[i]; 
    // set range in i direction, first ceil interval then floor
    threadRange[i]   = range[i] + threadCoords[i] * sizeFloor + std::min(threadCoords[i], nCeil);
    threadRange[i+3] = threadRange[i] + sizeFloor + ((threadCoords[i] < nCeil) ? 1 : 0);
  }

  return 0;
}
