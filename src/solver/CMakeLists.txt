set(solverSrc
  sys.cpp
  Utils.cpp
  DomainDecomp.cpp
  UniMesh.cpp
  HaloStore.cpp
  HaloTransfer.cpp
  Solver.cpp
  WJSolver.cpp
)

add_library(solver STATIC ${solverSrc})

if (likwid_dir)
  target_link_libraries(solver ${likwidLib})
endif()

if (papi_dir)
  target_link_libraries(solver ${papiLib})
endif()
