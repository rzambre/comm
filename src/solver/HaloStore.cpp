#include "HaloStore.h"

HaloStore::HaloStore(DomainDecomp& dd, int nHalo, int nVariable): 
  nVar(nVariable),
  _ptDd(&dd),  
  _nh(nHalo), 
  _nPipe(1),
  _pipeDir(2)
{}


HaloStore::~HaloStore()
{
  delete [] sBuf;
  delete [] rBuf;
}


void HaloStore::set_from_option()
{
  bool isSet;
  get_option("hs_nvar",    CmdOption::Int, 1, &nVar, &isSet);
  get_option("hs_npipe",   CmdOption::Int, 1, &_nPipe, &isSet);
  get_option("hs_pipedir", CmdOption::Int, 1, &_pipeDir, &isSet);
}


int HaloStore::setup_star()
{
  int szs[3];
  _ptDd->get_local_size(szs);

  // allocate buffer
  int bufSize = (szs[0]*szs[1] + szs[1]*szs[2] + szs[2]*szs[0]) * _nh * 2 * nVar;
  sBuf = new double [bufSize];
  rBuf = new double [bufSize];

  // set chunks
  if (_pipeDir == 0) {
    int pipeLen = szs[0] / _nPipe;
    // x-
    chunks.insert(chunks.end(), {0, _nh, _nh, _nh, szs[1]+_nh, szs[2]+_nh});
    toChunkIDs.push_back(4*_nPipe+1);
    toRanks.push_back(_ptDd->neighbor(static_cast<int>(FaceXm)));
    for (int i=0; i<_nPipe; ++i) {
      int iBegin = _nh + i*pipeLen, iEnd = iBegin + pipeLen;
      // y-
      chunks.insert(chunks.end(), {iBegin, 0, _nh, iEnd, _nh, _nh+szs[2]});
      toChunkIDs.push_back(i*4+4);
      toRanks.push_back(_ptDd->neighbor(static_cast<int>(FaceYm)));
      // z-
      chunks.insert(chunks.end(), {iBegin, _nh, 0, iEnd, _nh+szs[1], _nh});
      toChunkIDs.push_back(i*4+3);
      toRanks.push_back(_ptDd->neighbor(static_cast<int>(FaceZm)));
      // z+
      chunks.insert(chunks.end(), {iBegin, _nh, _nh+szs[2], iEnd, _nh+szs[1], 2*_nh+szs[2]});
      toChunkIDs.push_back(i*4+2);
      toRanks.push_back(_ptDd->neighbor(static_cast<int>(FaceZp)));
      // y+
      chunks.insert(chunks.end(), {iBegin, _nh+szs[1], _nh, iEnd, 2*_nh+szs[1], _nh+szs[2]});
      toChunkIDs.push_back(i*4+1);
      toRanks.push_back(_ptDd->neighbor(static_cast<int>(FaceYp)));
    }
    // x+
    chunks.insert(chunks.end(), {_nh+szs[0], _nh, _nh, szs[0]+2*_nh, szs[1]+_nh, szs[2]+_nh});
    toChunkIDs.push_back(0);
    toRanks.push_back(_ptDd->neighbor(static_cast<int>(FaceXp)));
  }
  else if (_pipeDir == 2) {
    int pipeLen = szs[2] / _nPipe;
    // z-
    chunks.insert(chunks.end(), {_nh, _nh, 0, szs[0]+_nh, szs[1]+_nh, _nh});
    toChunkIDs.push_back(4*_nPipe+1);
    toRanks.push_back(_ptDd->neighbor(static_cast<int>(FaceZm)));
    for (int k=0; k<_nPipe; ++k) {
      int kBegin = _nh + k*pipeLen, kEnd = kBegin + pipeLen;
      // x-
      chunks.insert(chunks.end(), {0, _nh, kBegin, _nh, _nh+szs[1], kEnd});
      toChunkIDs.push_back(k*4+4);
      toRanks.push_back(_ptDd->neighbor(static_cast<int>(FaceXm)));
      // y-
      chunks.insert(chunks.end(), {_nh, 0, kBegin, _nh+szs[0], _nh, kEnd});
      toChunkIDs.push_back(k*4+3);
      toRanks.push_back(_ptDd->neighbor(static_cast<int>(FaceYm)));
      // y+
      chunks.insert(chunks.end(), {_nh, _nh+szs[1], kBegin, _nh+szs[0], 2*_nh+szs[1], kEnd});
      toChunkIDs.push_back(k*4+2);
      toRanks.push_back(_ptDd->neighbor(static_cast<int>(FaceYp)));
      // x+
      chunks.insert(chunks.end(), {_nh+szs[0], _nh, kBegin, 2*_nh+szs[0], _nh+szs[1], kEnd});
      toChunkIDs.push_back(k*4+1);
      toRanks.push_back(_ptDd->neighbor(static_cast<int>(FaceXp)));
    }
    // z+
    chunks.insert(chunks.end(), {_nh, _nh, _nh+szs[2], _nh+szs[0], _nh+szs[1], 2*_nh+szs[2]});
    toChunkIDs.push_back(0);
    toRanks.push_back(_ptDd->neighbor(static_cast<int>(FaceZp)));
  }

  // buffer map: start index of each chunk in buffer
  bufMap.push_back(0);
  for (unsigned int i=0; i<chunks.size(); i+=6)
    bufMap.push_back( (chunks[i+3]-chunks[i+0]) * (chunks[i+4]-chunks[i+1]) 
                    * (chunks[i+5]-chunks[i+2]) * nVar + bufMap.back());

  return 0;
}


int HaloStore::setup_box()
{
  int szs[3],rngs[6];
  _ptDd->get_local_size(szs);

  // allocate buffer
  int bufSize = ((szs[0]+2*_nh)*(szs[1]+2*_nh)*(szs[2]+2*_nh) - szs[0]*szs[1]*szs[2]) * nVar;
  sBuf = new double [bufSize];
  rBuf = new double [bufSize];

  // set chunks
  vector<Halo> cids; 
  if (_pipeDir == 0) {
    int pipeLen = szs[0] / _nPipe;
    // x-
    cids = {CornerXmYmZm, EdgeXmYm, CornerXmYmZp, EdgeXmZm, FaceXm, EdgeXmZp, \
            CornerXmYpZm, EdgeXmYp, CornerXmYpZp};
    for (unsigned int i=0; i<cids.size(); ++i) {
      get_halo_chunk_range(cids[i], rngs);
      chunks.insert(chunks.end(), rngs, rngs+6);
      toChunkIDs.push_back(25 - i + 8*(_nPipe-1));
      toRanks.push_back(_ptDd->neighbor((int)cids[i]));
    }
    // inbetween
    cids = {EdgeYmZm, FaceYm, EdgeYmZp, FaceZm, /*skip*/ FaceZp, EdgeYpZm, \
            FaceYp, EdgeYpZp};
    for (int i=0; i<_nPipe; ++i) {
      int iBegin = _nh + i*pipeLen, iEnd = iBegin + pipeLen;
      for (unsigned int j=0; j<cids.size(); ++j) {
        get_halo_chunk_range(cids[j], rngs);
        rngs[0] = iBegin;
        rngs[3] = iEnd;
        chunks.insert(chunks.end(), rngs, rngs+6);
        toChunkIDs.push_back(7-j + 8*i + 9);
        toRanks.push_back(_ptDd->neighbor((int)cids[j]));
      }
    }
    // x+
    cids = {CornerXpYmZm, EdgeXpYm, CornerXpYmZp, EdgeXpZm, FaceXp, EdgeXpZp, \
            CornerXpYpZm, EdgeXpYp, CornerXpYpZp};
    for (unsigned int i=0; i<cids.size(); ++i) {
      get_halo_chunk_range(cids[i], rngs);
      chunks.insert(chunks.end(), rngs, rngs+6);
      toChunkIDs.push_back(8 - i);
      toRanks.push_back(_ptDd->neighbor((int)cids[i]));
    }
  }
  else if (_pipeDir == 2) {
    int pipeLen = szs[2] / _nPipe;
    // z-
    cids = {CornerXmYmZm, EdgeXmZm, CornerXmYpZm, EdgeYmZm, FaceZm, EdgeYpZm, \
            CornerXpYmZm, EdgeXpZm, CornerXpYpZm};
    for (unsigned int i=0; i<cids.size(); ++i) {
      get_halo_chunk_range(cids[i], rngs);
      chunks.insert(chunks.end(), rngs, rngs+6);
      toChunkIDs.push_back(25 - i + 8*(_nPipe-1));
      toRanks.push_back(_ptDd->neighbor((int)cids[i]));
    }
    // inbetween
    cids = {EdgeXmYm, FaceXm, EdgeXmYp, FaceYm, /*skip*/ FaceYp, EdgeXpYm, \
            FaceXp, EdgeXpYp};
    for (int k=0; k<_nPipe; ++k) {
      int kBegin = _nh + k*pipeLen, kEnd = kBegin + pipeLen;
      for (unsigned int j=0; j<cids.size(); ++j) {
        get_halo_chunk_range(cids[j], rngs);
        rngs[2] = kBegin;
        rngs[5] = kEnd;
        chunks.insert(chunks.end(), rngs, rngs+6);
        toChunkIDs.push_back(7-j + 8*k + 9);
        toRanks.push_back(_ptDd->neighbor((int)cids[j]));
      }
    }
    // z+
    cids = {CornerXmYmZp, EdgeXmZp, CornerXmYpZp, EdgeYmZp, FaceZp, EdgeYpZp, \
            CornerXpYmZp, EdgeXpZp, CornerXpYpZp};
    for (unsigned int i=0; i<cids.size(); ++i) {
      get_halo_chunk_range(cids[i], rngs);
      chunks.insert(chunks.end(), rngs, rngs+6);
      toChunkIDs.push_back(8 - i);
      toRanks.push_back(_ptDd->neighbor((int)cids[i]));
    }
  }

  // buffer map: start index of each chunk in buffer
  bufMap.push_back(0);
  for (unsigned int i=0; i<chunks.size(); i+=6)
    bufMap.push_back( (chunks[i+3]-chunks[i+0]) * (chunks[i+4]-chunks[i+1]) 
                    * (chunks[i+5]-chunks[i+2]) * nVar + bufMap.back());
  return 0;
}


void HaloStore::get_halo_chunk_range(Halo chunk, int hRngs[6]) const
{
  int sizes[3], rngs[6];
  _ptDd->get_local_size(sizes);
  for (int i=0; i<3; ++i) {
    rngs[i]   = _nh;
    rngs[i+3] = sizes[i] + _nh;
  }
  get_halo_chunk_range(rngs, chunk, hRngs);
}


void HaloStore::get_halo_chunk_range(int rngs[6], Halo chunk, int hRngs[6]) const
{
  int sizes[3];
  _ptDd->get_info(DecompOpt::lSizes, sizes);

  std::copy(rngs, rngs+6, hRngs);

  switch (chunk) {
    case FaceXm: 
      hRngs[0] = rngs[0] - _nh;   hRngs[3] = rngs[0]; break;
    case FaceYm:
      hRngs[1] = rngs[1] - _nh;   hRngs[4] = rngs[1]; break;
    case FaceZm:
      hRngs[2] = rngs[2] - _nh;   hRngs[5] = rngs[2]; break;
    case FaceXp:
      hRngs[0] = rngs[3];         hRngs[3] = rngs[3] + _nh; break;
    case FaceYp:
      hRngs[1] = rngs[4];         hRngs[4] = rngs[4] + _nh; break;
    case FaceZp:
      hRngs[2] = rngs[5];         hRngs[5] = rngs[5] + _nh; break;
    case EdgeYmZm:
      hRngs[1] = rngs[1] - _nh;   hRngs[4] = rngs[1];
      hRngs[2] = rngs[2] - _nh;   hRngs[5] = rngs[2]; break;
    case EdgeYpZm:
      hRngs[1] = rngs[4];         hRngs[4] = rngs[4] + _nh;
      hRngs[2] = rngs[2] - _nh;   hRngs[5] = rngs[2]; break;
    case EdgeYmZp:
      hRngs[1] = rngs[1] - _nh;   hRngs[4] = rngs[1];
      hRngs[2] = rngs[5];         hRngs[5] = rngs[5] + _nh; break;
    case EdgeYpZp:
      hRngs[1] = rngs[4];         hRngs[4] = rngs[4] + _nh;
      hRngs[2] = rngs[5];         hRngs[5] = rngs[5] + _nh; break;
    case EdgeXmZm:
      hRngs[0] = rngs[0] - _nh;   hRngs[3] = rngs[0];
      hRngs[2] = rngs[2] - _nh;   hRngs[5] = rngs[2]; break;
    case EdgeXmZp:
      hRngs[0] = rngs[0] - _nh;   hRngs[3] = rngs[0];
      hRngs[2] = rngs[5];         hRngs[5] = rngs[5] + _nh; break;
    case EdgeXpZm:
      hRngs[0] = rngs[3];         hRngs[3] = rngs[3] + _nh;
      hRngs[2] = rngs[2] - _nh;   hRngs[5] = rngs[2]; break;
    case EdgeXpZp:
      hRngs[0] = rngs[3];         hRngs[3] = rngs[3] + _nh;
      hRngs[2] = rngs[5];         hRngs[5] = rngs[5] + _nh; break;
    case EdgeXmYm:
      hRngs[0] = rngs[0] - _nh;   hRngs[3] = rngs[0];
      hRngs[1] = rngs[1] - _nh;   hRngs[4] = rngs[1]; break;
    case EdgeXpYm:
      hRngs[0] = rngs[3];         hRngs[3] = rngs[3] + _nh;
      hRngs[1] = rngs[1] - _nh;   hRngs[4] = rngs[1]; break;
    case EdgeXmYp:
      hRngs[0] = rngs[0] - _nh;   hRngs[3] = rngs[0];
      hRngs[1] = rngs[4];         hRngs[4] = rngs[4] + _nh; break;
    case EdgeXpYp:
      hRngs[0] = rngs[3];         hRngs[3] = rngs[3] + _nh;
      hRngs[1] = rngs[4];         hRngs[4] = rngs[4] + _nh; break;
    case CornerXmYmZm:
      hRngs[0] = rngs[0] - _nh;   hRngs[3] = rngs[0];
      hRngs[1] = rngs[1] - _nh;   hRngs[4] = rngs[1];
      hRngs[2] = rngs[2] - _nh;   hRngs[5] = rngs[2]; break;
    case CornerXmYmZp:
      hRngs[0] = rngs[0] - _nh;   hRngs[3] = rngs[0];
      hRngs[1] = rngs[1] - _nh;   hRngs[4] = rngs[1];
      hRngs[2] = rngs[5];         hRngs[5] = rngs[5] + _nh; break;
    case CornerXmYpZm:
      hRngs[0] = rngs[0] - _nh;   hRngs[3] = rngs[0];
      hRngs[1] = rngs[4];         hRngs[4] = rngs[4] + _nh;
      hRngs[2] = rngs[2] - _nh;   hRngs[5] = rngs[2]; break;
    case CornerXmYpZp:
      hRngs[0] = rngs[0] - _nh;   hRngs[3] = rngs[0];
      hRngs[1] = rngs[4];         hRngs[4] = rngs[4] + _nh;
      hRngs[2] = rngs[5];         hRngs[5] = rngs[5] + _nh; break;
    case CornerXpYmZm:
      hRngs[0] = rngs[3];         hRngs[3] = rngs[3] + _nh;
      hRngs[1] = rngs[1] - _nh;   hRngs[4] = rngs[1];
      hRngs[2] = rngs[2] - _nh;   hRngs[5] = rngs[2]; break;
    case CornerXpYmZp:
      hRngs[0] = rngs[3];         hRngs[3] = rngs[3] + _nh;
      hRngs[1] = rngs[1] - _nh;   hRngs[4] = rngs[1];
      hRngs[2] = rngs[5];         hRngs[5] = rngs[5] + _nh; break;
    case CornerXpYpZm:
      hRngs[0] = rngs[3];         hRngs[3] = rngs[3] + _nh;
      hRngs[1] = rngs[4];         hRngs[4] = rngs[4] + _nh;
      hRngs[2] = rngs[2] - _nh;   hRngs[5] = rngs[2]; break;
    case CornerXpYpZp:
      hRngs[0] = rngs[3];         hRngs[3] = rngs[3] + _nh;
      hRngs[1] = rngs[4];         hRngs[4] = rngs[4] + _nh;
      hRngs[2] = rngs[5];         hRngs[5] = rngs[5] + _nh; break;
  }
}


void HaloStore::debug_view()
{
  if (_ptDd->is_root()) {
    std::cout << "Debug View of Halo Store Object" << std::endl;
    std::cout << bufMap.size() - 1<< " halo chunks" << std::endl;
    std::cout << _nPipe << " pipeline in " << _pipeDir << std::endl;

    std::cout << "start loc in buffer: " << std::endl;
    for (unsigned int i=0; i<bufMap.size()-1; ++i)
      std::cout << bufMap[i] << " ";
    std::cout << std::endl;

    std::cout << "chunk: range | toChunk | toRank" << std::endl;
    for (unsigned int i=0; i<chunks.size(); i+=6) {
      int c = i / 6;
      std::cout << c;
      for (unsigned int j=0; j<6; ++j) std::cout << "\t" << chunks[i+j];
      std::cout << " | " << toChunkIDs[c] << " | " << toRanks[c] << std::endl;
    }
  }
}
