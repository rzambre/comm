#include "HaloTransfer.h"

#include <iostream>
#include <vector>
#include <algorithm>
#include <mpi.h>
#include <omp.h>

using std::vector;

HaloTransfer::HaloTransfer(const DomainDecomp& dd, const int nHalo):
  _nHalo(nHalo),
  _cacheLineLen(8),
  _ptDd(&dd)
{};


HaloTransfer::HaloTransfer(const HaloTransfer& rhs)
{
  if (_ptDd->is_root())
    std::cout << "Error: HaloTransfer object should not be copied" << std::endl;
  MPI_Finalize();
}


const HaloTransfer& HaloTransfer::operator=(const HaloTransfer& rhs)
{
  if (_ptDd->is_root())
    std::cout << "Error: HaloTransfer object should not be assigned" << std::endl;
  MPI_Finalize();
  return *this;
}


void HaloTransfer::set_from_option()
{
  bool isSet;
  get_option("ht_nhalo", CmdOption::Int, 1, &_nHalo,        &isSet);
  get_option("ht_line",  CmdOption::Int, 1, &_cacheLineLen, &isSet);
}


void HaloTransfer::get_local_range(int rngs[6])
{
  _ptDd->get_local_size(rngs);
  for (int i=0; i<3; ++i) {
    rngs[i+3] = rngs[i] + _nHalo;
    rngs[i]   = _nHalo;
  }
}


int HaloTransfer::pack_halo(double** d, vector<int>& vs, HaloStore& hs)
{
  int tid = omp_get_thread_num();
  if (tid >= static_cast<int>(_haloRngs.size())) return -1;

  int szs[3];  
  _ptDd->get_local_size(szs);
  int jStrd    = szs[2] + 2*_nHalo; 
  int iStrd    = jStrd * (szs[1] + 2*_nHalo);
  int bufStart = _bufStarts[tid];
  
  for (unsigned int m = 0; m < _bodyRngs[tid].size(); m += 7) {
    int zLen = _bodyRngs[tid][m+5] - _bodyRngs[tid][m+2];
    int vid  = vs[_bodyRngs[tid][m+6]];
    // if zLen is greater when given size (cache line length?), use std copy
    if (zLen >= 8) {
      double *base  = d[vid] + _bodyRngs[tid][m]*iStrd + _bodyRngs[tid][m+1]*jStrd;
      for (int i = _bodyRngs[tid][m]; i < _bodyRngs[tid][m+3]; ++i) {
        double* base0 = base;
        for (int j = _bodyRngs[tid][m+1]; j < _bodyRngs[tid][m+4]; ++j) {
          std::copy(base + _bodyRngs[tid][m+2], base + _bodyRngs[tid][m+5], hs.sBuf + bufStart);
          bufStart += zLen;
          base     += jStrd;
        }
        base = base0 + iStrd;
      }
    }
    // else, copy by element
    else {
      int base = _bodyRngs[tid][m]*iStrd + _bodyRngs[tid][m+1]*jStrd;
      for (int i = _bodyRngs[tid][m]; i < _bodyRngs[tid][m+3]; ++i) {
        int base0 = base;
        for (int j = _bodyRngs[tid][m+1]; j < _bodyRngs[tid][m+4]; ++j) {
          for (int k = _bodyRngs[tid][m+2]; k < _bodyRngs[tid][m+5]; ++k) {
            hs.sBuf[bufStart] = d[vid][base + k];
            ++bufStart;
          }
          base += jStrd;
        }
        base = base0 + iStrd;
      }
    }//end else
  }//end for

  return 0;
}


int HaloTransfer::unpack_halo(double** d, vector<int>& vs, HaloStore& hs)
{
  int tid = omp_get_thread_num();
  if (tid >= static_cast<int>(_haloRngs.size())) return -1;

  int szs[3];
  _ptDd->get_local_size(szs);
  int jStrd    = szs[2] + 2*_nHalo; 
  int iStrd    = jStrd * (szs[1] + 2*_nHalo);
  int bufStart = _bufStarts[tid];
  
  for (int m = 0; m < (int)_haloRngs[tid].size(); m += 7) {
    int zLen = _haloRngs[tid][m+5] - _haloRngs[tid][m+2];
    int vid  = vs[_haloRngs[tid][m+6]];
    // if zLen is greater when given size (cache line length?), use std copy
    if (zLen >= 8) {
      double *base  = d[vid] + _haloRngs[tid][m]*iStrd + _haloRngs[tid][m+1]*jStrd;
      for (int i = _haloRngs[tid][m]; i < _haloRngs[tid][m+3]; ++i) {
        double* base0 = base;
        for (int j = _haloRngs[tid][m+1]; j < _haloRngs[tid][m+4]; ++j) {
          std::copy(hs.rBuf + bufStart, hs.rBuf + bufStart + zLen, base + _haloRngs[tid][m+2]);
          bufStart += zLen;
          base     += jStrd;
        }//end for j
        base = base0 + iStrd;
      }//end for i
    }
    // else, copy by element
    else {
      int base = _haloRngs[tid][m]*iStrd + _haloRngs[tid][m+1]*jStrd;
      for (int i = _haloRngs[tid][m]; i < _haloRngs[tid][m+3]; ++i) {
        int base0 = base;
        for (int j = _haloRngs[tid][m+1]; j < _haloRngs[tid][m+4]; ++j) {
          for (int k = _haloRngs[tid][m+2]; k < _haloRngs[tid][m+5]; ++k) {
            d[vid][base + k] = hs.rBuf[bufStart];
            ++bufStart;
          }
          base += jStrd;
        }//end for j
        base = base0 + iStrd;
      }//end for i
    }//end else
  }//end for

  return 0;
}


int HaloTransfer::update_halo_begin(HaloStore& hs)
{
  MPI_Comm comm = _ptDd->comm();

  for (int c=_cBegin; c<_cEnd; ++c) {
    int cTo = hs.toChunkIDs[c];
    MPI_Isend(hs.sBuf+hs.bufMap[c], hs.bufMap[c+1]-hs.bufMap[c], MPI_DOUBLE, \
              hs.toRanks[c], cTo, comm, &_reqs[2*(c-_cBegin)]);
    MPI_Irecv(hs.rBuf+hs.bufMap[cTo], hs.bufMap[cTo+1]-hs.bufMap[cTo], MPI_DOUBLE, \
              hs.toRanks[cTo], cTo, comm, &_reqs[2*(c-_cBegin)+1]);
  }

  return 0;
}


int HaloTransfer::update_halo_end()
{
  MPI_Waitall(2*(_cEnd-_cBegin), &_reqs[0], MPI_STATUSES_IGNORE);
  return 0;
}


int HaloTransfer::update_halo(HaloStore& hs)
{
  update_halo_begin(hs);
  update_halo_end();
  return 0;
}


void HaloTransfer::debug_view()
{
  if (_ptDd->is_root()) {
    std::cout << "Debug View of Halo Transfer Object" << std::endl;

    std::cout << "Set up for " << _haloRngs.size() << " threads" << std::endl;

//    std::cout << "thread start loc in buffer: " << std::endl;
//    for (unsigned int i=0; i<_bufStarts.size()-1; ++i)
//      std::cout << _bufStarts[i] << " ";
//    std::cout << std::endl;

    std::cout << "thread's halo range: " << std::endl;
    for (unsigned int tid=0; tid<_haloRngs.size(); ++tid) {
      std::cout << "thread " << tid << std::endl;
      int tVol = 0;
      for (unsigned int m=0; m<_haloRngs[tid].size(); m+=8) {
        tVol += (_haloRngs[tid][m+3] - _haloRngs[tid][m]) \
              * (_haloRngs[tid][m+4] - _haloRngs[tid][m+1]) \
              * (_haloRngs[tid][m+5] - _haloRngs[tid][m+2]);
        for (unsigned int i=0; i<8; ++i)  std::cout << _haloRngs[tid][m+i] << " ";
        std::cout << std::endl;
      }
      std::cout << "thread vol: " << tVol << std::endl;
    }
  }
}


int HaloTransfer::set_halo_range(HaloStore& hs, int cBegin, int cEnd, int nThrd)
{
  int sizes[3]; _ptDd->get_local_size(sizes);
  vector<int>         cRngs(7,0);
  vector<vector<int>> cStack;

  _cBegin = cBegin;
  _cEnd   = cEnd;
  vector<int> vecNull;
  _haloRngs.clear();
  _haloRngs.insert(_haloRngs.begin(), nThrd, vecNull);
  _bodyRngs.clear();
  _bodyRngs.insert(_bodyRngs.begin(), nThrd, vecNull);
  _bufStarts.clear();
  _bufStarts.insert(_bufStarts.begin(), nThrd+1, hs.bufMap[cBegin]);
  _reqs.clear();
  MPI_Request reqNull;
  _reqs.insert(_reqs.begin(), 2*(_cEnd-_cBegin), reqNull);

  // add chunk ranges to stack, count k rows
  int sumCacheLines = 0;
  for (int i=cEnd-1; i>=cBegin; --i) {
    for (int j=0; j<6; ++j)  cRngs[j] = hs.chunks[6*i+j];
    for (int v=hs.nVar-1; v>=0;  --v) {
      cRngs[6] = v;
      cStack.push_back(cRngs);
    }
    sumCacheLines += (cRngs[3] - cRngs[0]) * (cRngs[4] - cRngs[1])
                   * std::max(1, (cRngs[5] - cRngs[2]) / _cacheLineLen);
  }
  sumCacheLines *= hs.nVar;

  // average cache lines per thread
  vector<int> thrdRooms(nThrd, sumCacheLines/nThrd);
  for (int i=0; i<sumCacheLines%nThrd; ++i)  ++thrdRooms[i];

  // set range for each thread
  for (int tid=0; tid<nThrd; ++tid) {
    _bufStarts[tid+1] = _bufStarts[tid];
    while (thrdRooms[tid] > 0) {
      // #cache lines of top chunk
      cRngs = cStack.back();
      cStack.pop_back();
      int nLine = (cRngs[3] - cRngs[0]) * (cRngs[4] - cRngs[1]) 
                * std::max(1, (cRngs[5] - cRngs[2]) / _cacheLineLen);
      // if current chunk fit in thread, assign it
      if (nLine <= thrdRooms[tid]) {
        _haloRngs[tid].insert(_haloRngs[tid].end(), cRngs.begin(), cRngs.end());
        thrdRooms[tid] -= nLine;
        _bufStarts[tid+1] += (cRngs[3]-cRngs[0])*(cRngs[4]-cRngs[1])*(cRngs[5]-cRngs[2]);
        //std::cout << "assign " << nLine << " to " << tid << std::endl;
        if (thrdRooms[tid] > 0 && cStack.empty())
          std::cout << "Error: thread not full but chunks all assigned" << std::endl;
      }
      // else the whole chunk cannot fit in the thread room
      else {
        // adjust current and next thread room
        int nLinePerK = std::max(1, (cRngs[5] - cRngs[2]) / _cacheLineLen);
        int lineLen   = std::min(cRngs[5] - cRngs[2], _cacheLineLen);
        int remainder = thrdRooms[tid] % nLinePerK;
        if (remainder > nLinePerK/2) {
          thrdRooms[tid] += (nLinePerK - remainder);
          if (tid < nThrd-1) thrdRooms[tid+1] -= (nLinePerK - remainder);
          //std::cout << "shift " << nLinePerK - remainder << " from " << tid+1 << " to " << tid << std::endl;
        }
        else if (remainder > 0) {
          thrdRooms[tid] -= remainder;
          if (tid < nThrd-1) thrdRooms[tid+1] += remainder;
          //std::cout << "shift " << remainder << " from " << tid << " to " << tid+1 << std::endl;
        }
        // check if chunk fits in room now
        if (nLine <= thrdRooms[tid]) {
          _haloRngs[tid].insert(_haloRngs[tid].end(), cRngs.begin(), cRngs.end());
          _bufStarts[tid+1] += (cRngs[3]-cRngs[0])*(cRngs[4]-cRngs[1])*(cRngs[5]-cRngs[2]);
          break;
        }
        // split chunk and add to thread, add rest to stack
        int nPlnJK = (thrdRooms[tid] / nLinePerK) / (cRngs[4] - cRngs[1]); // # jk place
        int nRmnK  = (thrdRooms[tid] / nLinePerK) % (cRngs[4] - cRngs[1]); // # remaining k rows
        vector<int> tmpRngs = cRngs;
        // add planes to thread
        if (nPlnJK > 0) {
          tmpRngs[3] = cRngs[0] + nPlnJK;
          _haloRngs[tid].insert(_haloRngs[tid].end(), tmpRngs.begin(), tmpRngs.end());
          _bufStarts[tid+1] += nPlnJK * (cRngs[4] - cRngs[1]) * nLinePerK * lineLen;
          //std::cout << "assign " << nPlnJK * (cRngs[4] - cRngs[1]) * nLinePerK << " to room " << thrdRooms[tid] << std::endl;
          thrdRooms[tid]    -= nPlnJK * (cRngs[4] - cRngs[1]) * nLinePerK;
        }
        if (nRmnK > 0) {
          // add remaining k rows on plane
          tmpRngs[0] = cRngs[0] + nPlnJK;  tmpRngs[3] = cRngs[0] + nPlnJK + 1;
          tmpRngs[1] = cRngs[1];           tmpRngs[4] = cRngs[1] + nRmnK;
          _haloRngs[tid].insert(_haloRngs[tid].end(), tmpRngs.begin(), tmpRngs.end());
          _bufStarts[tid+1] += nRmnK * nLinePerK * lineLen;
          //std::cout << "assign " << nRmnK * nLinePerK << " to room " << thrdRooms[tid] << std::endl;
          thrdRooms[tid]    -= nRmnK * nLinePerK;
          // add rest planes of chunk to stack
          if (nPlnJK < cRngs[3] - cRngs[0]) {
            tmpRngs = cRngs; tmpRngs[0] = cRngs[0] + nPlnJK + 1;
            cStack.push_back(tmpRngs);
            //std::cout << "push " << (tmpRngs[3] - tmpRngs[0]) * (tmpRngs[4] - tmpRngs[1]) * (tmpRngs[5] - tmpRngs[2]) << std::endl;
          }
          // add remaining k rows to stack
          tmpRngs[0] = cRngs[0] + nPlnJK;  tmpRngs[3] = cRngs[0] + nPlnJK + 1;
          tmpRngs[1] = cRngs[1] + nRmnK;   tmpRngs[4] = cRngs[4];
          cStack.push_back(tmpRngs);
            //std::cout << "push " << (tmpRngs[3] - tmpRngs[0]) * (tmpRngs[4] - tmpRngs[1]) * (tmpRngs[5] - tmpRngs[2]) << std::endl;
        }
        else { // nRmnK == 0
          tmpRngs = cRngs; 
          tmpRngs[0] = cRngs[0] + nPlnJK;
          cStack.push_back(tmpRngs);
        }
        break;
      }
    }//end while

    _bodyRngs[tid] = _haloRngs[tid];
    // set the face range
    for (unsigned int m=0; m<_haloRngs[tid].size(); m+=7) {
      for (unsigned int i=0; i<3; ++i) {
        if (_haloRngs[tid][m+i] < _nHalo) {
          _bodyRngs[tid][m+i]   += _nHalo;
          _bodyRngs[tid][m+i+3] += _nHalo;
        }
        else if (_haloRngs[tid][m+i] >= sizes[i]+_nHalo) {
          _bodyRngs[tid][m+i]   -= _nHalo;
          _bodyRngs[tid][m+i+3] -= _nHalo;
        }
      }
    }
  }//end for

  return 0;
}


int HaloTransfer::setup_pack_pipeline(int p, HaloStore& hs, int nThrd)
{
  int nPipe = hs.num_pipe();
  int pDir  = hs.pipe_dir();

  _bodyRngs.clear();
  _bodyRngs.insert(_bodyRngs.end(), nThrd, vector<int>());

  // range for pipeline
  int szs[3], rngs[6], tRngs[6];
  _ptDd->get_local_size(szs);
  for (int i=0; i<3; ++i) {
    rngs[i]   = _nHalo;
    rngs[i+3] = szs[i] + _nHalo;
  }
  rngs[pDir]   = _nHalo + p * (szs[pDir]/nPipe);
  rngs[pDir+3] = rngs[pDir] + (szs[pDir]/nPipe);

  // set up subhalo ids and chunk ids for current pipeline
  vector<int>  cids;  // chunk id for current pipeline
  vector<Halo> shids; // subhalo id
  if (_ptDd->is_neighbor_star()) {
    if (p == 0)
      cids = {0,1,2,3,4};
    else if (p == nPipe-1)
      for (int i=4*(nPipe-1)+1; i<4*nPipe+2; ++i) cids.push_back(i);
    else 
      for (int i=4*p+1; i<4*(p+1)+1; ++i) cids.push_back(i);
    //
    shids = {FaceXm, FaceYm, FaceZm, FaceXp, FaceYp, FaceZp};
  }
  else {
    if (p == 0)
      for (int i=0; i<17; ++i) cids.push_back(i);
    else if (p == nPipe-1)
      for (int i=8*(nPipe-1)+9; i<8*nPipe+18; ++i) cids.push_back(i);
    else
      for (int i=8*p+9; i<8*p+17; ++i) cids.push_back(i);
    //
    for (int i=0; i<26; ++i) shids.push_back(static_cast<Halo>(i));
  }
 
  for (int tid=0; tid<nThrd; ++tid) {
    compute_thread_range(rngs, nThrd, tid, tRngs);
    int shRngs[6], hRngs[6]; // sub halo, halo range
    for (unsigned int sh=0; sh<shids.size(); ++sh) {
      hs.get_halo_chunk_range(tRngs, shids[sh], shRngs);
      // skip if sub halo is not an actual halo
      if (  shRngs[0] >= _nHalo && shRngs[0] < szs[0] + _nHalo
         && shRngs[1] >= _nHalo && shRngs[1] < szs[1] + _nHalo
         && shRngs[2] >= _nHalo && shRngs[2] < szs[2] + _nHalo) continue;
      // traverse to find the chunk that hRngs belong to
      for (unsigned int c=0; c<cids.size(); ++c) {
        // check if start point is in halo chunk range
        if (  shRngs[0] >= hs.chunks[6*cids[c]]   && shRngs[0] < hs.chunks[6*cids[c]+3]
           && shRngs[1] >= hs.chunks[6*cids[c]+1] && shRngs[1] < hs.chunks[6*cids[c]+4]
           && shRngs[2] >= hs.chunks[6*cids[c]+2] && shRngs[2] < hs.chunks[6*cids[c]+5]) {
          // map sub halo to range inside body
          for (int i=0; i<3; ++i) {
            if (shRngs[i] < _nHalo) {
              shRngs[i]   += _nHalo;
              shRngs[i+3] += _nHalo;
            }
            else if (shRngs[i] >= szs[i] + _nHalo) {
              shRngs[i]    -= _nHalo;
              shRngs[i+3]  -= _nHalo;
            }
          }
          // add body range
          for (int v=0; v<hs.nVar; ++v) {
            _bodyRngs[tid].insert(_bodyRngs[tid].end(), shRngs, shRngs+6);
            _bodyRngs[tid].push_back(v);
            _bodyRngs[tid].push_back(cids[c]);
          }
        }// end if
      }// end for c
    }// end for sh
  }// end for tid

  // set up requests and chunks to be communicated
  if (_ptDd->is_neighbor_star()) {
    if (p == 0) {
      _cBegin = 0;
      _cEnd   = 5;
    }
    else if (p == nPipe-1) {
      _cBegin = 4*(nPipe-1) + 1;
      _cEnd   = 4*nPipe+2;
    }
    else {
      _cBegin = 4*p + 1;
      _cEnd   = 4*p + 5;
    }
  }
  else {
    if (p == 0) {
      _cBegin = 0;
      _cEnd   = 17;
    }
    else if (p == nPipe-1) {
      _cBegin = 8*(nPipe-1) + 9;
      _cEnd   = 8*nPipe     + 18;
    }
    else {
      _cBegin = 8*p + 9;
      _cEnd   = 8*p + 17;
    }
  }
  //
  MPI_Request  reqNull;
  _reqs.clear();
  _reqs.insert(_reqs.begin(), 2*(_cEnd - _cBegin), reqNull);

  return 0;
}


int HaloTransfer::setup_unpack_pipeline(int p, HaloStore& hs, int nThrd)
{
  int nPipe = hs.num_pipe();
  int pDir  = hs.pipe_dir();

  _haloRngs.clear();
  _haloRngs.insert(_haloRngs.end(), nThrd, vector<int>());

  // range for pipeline
  int szs[3], rngs[6], tRngs[6];
  _ptDd->get_local_size(szs);
  for (int i=0; i<3; ++i) {
    rngs[i]   = _nHalo;
    rngs[i+3] = szs[i] + _nHalo;
  }
  rngs[pDir]   = _nHalo + p * (szs[pDir]/nPipe);
  rngs[pDir+3] = rngs[pDir] + (szs[pDir]/nPipe);

  // set up subhalo ids and chunk ids for current pipeline
  vector<int>  cids;  // chunk id for current pipeline
  vector<Halo> shids; // subhalo id
  if (_ptDd->is_neighbor_star()) {
    if (p == 0) {
      cids = {0};
      if (pDir == 0) shids = {FaceXm};
      if (pDir == 2) shids = {FaceZm};
    }
    else if (p < nPipe-1) {
      for (int i=4*p+1; i<4*(p+1)+1; ++i) cids.push_back(i);
      shids = {FaceXm, FaceYm, FaceZm, FaceXp, FaceYp, FaceZp};
    }
  }
  else {
    if (p == 0) {
      for (int i=0; i<9; ++i) cids.push_back(i);
      if (pDir == 0) 
        shids = {CornerXmYmZm, EdgeXmYm, CornerXmYmZp, EdgeXmZm, FaceXm, \
                 EdgeXmZp, CornerXmYpZm, EdgeXmYp, CornerXmYpZp};
      if (pDir == 2)
        shids = {CornerXmYmZm, EdgeXmZm, CornerXmYpZm, EdgeYmZm, FaceZm, \
                 EdgeYpZm, CornerXpYmZm, EdgeXpZm, CornerXpYpZm};
    }
    else if (p < nPipe-1) {
      for (int i=8*p+9; i<8*(p+1)+9; ++i) cids.push_back(i);
      for (int i=0; i<26; ++i) shids.push_back(static_cast<Halo>(i));
    }
  }
 
  for (int tid=0; tid<nThrd; ++tid) {
    compute_thread_range(rngs, nThrd, tid, tRngs);
    int shRngs[6], hRngs[6]; // sub halo, halo range
    for (unsigned int sh=0; sh<shids.size(); ++sh) {
      hs.get_halo_chunk_range(tRngs, shids[sh], shRngs);
      // skip if sub halo is not an actual halo
      if (  shRngs[0] >= _nHalo && shRngs[0] < szs[0] + _nHalo
         && shRngs[1] >= _nHalo && shRngs[1] < szs[1] + _nHalo
         && shRngs[2] >= _nHalo && shRngs[2] < szs[2] + _nHalo) continue;
      // traverse to find the chunk that hRngs belong to
      for (unsigned int c=0; c<cids.size(); ++c) {
        // check if start point is in halo chunk range
        if (  shRngs[0] >= hs.chunks[6*cids[c]]   && shRngs[0] < hs.chunks[6*cids[c]+3]
           && shRngs[1] >= hs.chunks[6*cids[c]+1] && shRngs[1] < hs.chunks[6*cids[c]+4]
           && shRngs[2] >= hs.chunks[6*cids[c]+2] && shRngs[2] < hs.chunks[6*cids[c]+5]) {
          // add halo range
          for (int v=0; v<hs.nVar; ++v) {
            _haloRngs[tid].insert(_haloRngs[tid].end(), shRngs, shRngs+6);
            _haloRngs[tid].push_back(v);
            _haloRngs[tid].push_back(cids[c]);
          }
        }// end if
      }// end for c
    }// end for sh
  }// end for tid

  // adjust sub halo ranges for unpacking
  int cHeight, cStrd, pipeLen = szs[pDir] / nPipe;
  if (_ptDd->is_neighbor_star()) {
    cHeight  = 4*nPipe + 1;
    cStrd = 4;
  }
  else {
    cHeight  = 8*nPipe + 9;
    cStrd = 8;
  }
  // works when pDir is z or x but only 1d decomp among threads
  if (p == 0) {
    for (int tid =0; tid<nThrd; ++tid) {
      unsigned int size0 = _haloRngs[tid].size();
      for (unsigned int i=0; i<size0; i+=8) {
        if (_haloRngs[tid][i+pDir] < _nHalo) {
          vector<int> tmp(_haloRngs[tid].begin()+i, _haloRngs[tid].begin()+i+8);
          tmp[pDir]   += szs[pDir] + _nHalo;
          tmp[pDir+3] += szs[pDir] + _nHalo;
          tmp[7]       = cHeight + _haloRngs[tid][i+7];
          _haloRngs[tid].insert(_haloRngs[tid].end(), tmp.begin(), tmp.end());
        }
      }
    }
  }
  else if (p == 1) {
    for (int tid =0; tid<nThrd; ++tid) {
      unsigned int size0 = _haloRngs[tid].size();
      for (unsigned int i=0; i<size0; i+=8) {
        vector<int> tmp(_haloRngs[tid].begin()+i, _haloRngs[tid].begin()+i+8);
        tmp[pDir]   = _haloRngs[tid][i+pDir]   - pipeLen;
        tmp[pDir+3] = _haloRngs[tid][i+pDir+3] - pipeLen;
        tmp[7]      = _haloRngs[tid][i+7]      - cStrd;
        _haloRngs[tid].insert(_haloRngs[tid].end(), tmp.begin(), tmp.end());
        tmp[pDir]   = _haloRngs[tid][i+pDir]   + pipeLen;
        tmp[pDir+3] = _haloRngs[tid][i+pDir+3] + pipeLen;
        tmp[7]      = _haloRngs[tid][i+7]      + cStrd;
        _haloRngs[tid].insert(_haloRngs[tid].end(), tmp.begin(), tmp.end());
      }
    }
  }
  else if (p < nPipe-1) {
    for (int tid =0; tid<nThrd; ++tid) {
      unsigned int size0 = _haloRngs[tid].size();
      for (unsigned int i=0; i<size0; i+=8) {
        vector<int> tmp(_haloRngs[tid].begin()+i, _haloRngs[tid].begin()+i+8);
        tmp[pDir]   = _haloRngs[tid][i+pDir]   + pipeLen;
        tmp[pDir+3] = _haloRngs[tid][i+pDir+3] + pipeLen;
        tmp[7]      = _haloRngs[tid][i+7]      + cStrd;
        _haloRngs[tid].insert(_haloRngs[tid].end(), tmp.begin(), tmp.end());
      }
      _haloRngs[tid].erase(_haloRngs[tid].begin(), _haloRngs[tid].begin() + size0);
    }
  }

  return 0;
}


int HaloTransfer::pack_subhalo(double **d, vector<int>& vs, HaloStore& hs)
{
  int tid = omp_get_thread_num();
  if (tid >= (int)_bodyRngs.size()) return -1;

  int szs[3];
  _ptDd->get_local_size(szs);
  int js = szs[2] + 2*_nHalo, is = js * (szs[1] + 2*_nHalo);

  for (unsigned int m=0; m<_bodyRngs[tid].size(); m+=8) {
    int cid   = _bodyRngs[tid][m+7];
    int cjs   = hs.chunks[6*cid+5] - hs.chunks[6*cid+2];
    int cis   = cjs * (hs.chunks[6*cid+4] - hs.chunks[6*cid+1]);
    int cVol  = cis * (hs.chunks[6*cid+3] - hs.chunks[6*cid]);
    int zLen  = _bodyRngs[tid][m+5] - _bodyRngs[tid][m+2];
    int bb    = hs.bufMap[cid]; // buf begin
    int ib  = std::min(std::max(hs.chunks[6*cid],   _nHalo), szs[0]);
    int jb  = std::min(std::max(hs.chunks[6*cid+1], _nHalo), szs[1]);
    int kb  = std::min(std::max(hs.chunks[6*cid+2], _nHalo), szs[2]);

    if (zLen >= _cacheLineLen) {
      for (int v=0; v<hs.nVar; ++v) {
        int ij  = _bodyRngs[tid][m] * is + _bodyRngs[tid][m+1] * js;
        int cij = (_bodyRngs[tid][m] - ib) * cis + (_bodyRngs[tid][m+1] - jb) * cjs;
        int vid = vs[v];
        for (int i=_bodyRngs[tid][m]; i<_bodyRngs[tid][m+3]; ++i) {
          int ij0  = ij;
          int cij0 = cij;
          for (int j=_bodyRngs[tid][m+1]; j<_bodyRngs[tid][m+4]; ++j) {
            std::copy(d[vid] + ij + _bodyRngs[tid][m+2], \
                      d[vid] + ij + _bodyRngs[tid][m+5], \
                      hs.sBuf + bb + cij + _bodyRngs[tid][m+2] - kb);
            ij += js;
            cij += cjs;
          }
          ij  = ij0  + is;
          cij = cij0 + cis;
        }
        bb += cVol;
      }// end for v
    }// end if
    else {
      for (int v=0; v<hs.nVar; ++v) {
        int ij  = _bodyRngs[tid][m] * is + _bodyRngs[tid][m+1] * js;
        int cij = (_bodyRngs[tid][m] - ib) * cis + (_bodyRngs[tid][m+1] - jb) * cjs;
        int vid = vs[v];
        for (int i=_bodyRngs[tid][m]; i<_bodyRngs[tid][m+3]; ++i) {
          int ij0  = ij;
          int cij0 = cij;
          for (int j=_bodyRngs[tid][m+1]; j<_bodyRngs[tid][m+4]; ++j) {
            for (int k=_bodyRngs[tid][m+2]; k<_bodyRngs[tid][m+5]; ++k)
              hs.sBuf[bb + cij + k - kb] = d[vid][ij + k];
            ij += js;
            cij += cjs;
          }
          ij  = ij0  + is;
          cij = cij0 + cis;
        }
        bb += cVol;
      }// end for v
    }// end else
  }// end for m

  return 0;
}


int HaloTransfer::unpack_subhalo(double **d, vector<int>& vs, HaloStore& hs)
{
  int tid = omp_get_thread_num();
  if (tid >= (int)_haloRngs.size()) return -1;

  int szs[3];
  _ptDd->get_local_size(szs);
  int js = szs[2] + 2*_nHalo, is = js * (szs[1] + 2*_nHalo);

  for (unsigned int m=0; m<_haloRngs[tid].size(); m+=8) {
    int cid   = _haloRngs[tid][m+7];
    int cjs   = hs.chunks[6*cid+5] - hs.chunks[6*cid+2];
    int cis   = cjs * (hs.chunks[6*cid+4] - hs.chunks[6*cid+1]);
    int cVol  = cis * (hs.chunks[6*cid+3] - hs.chunks[6*cid]);
    int zLen  = _haloRngs[tid][m+5] - _haloRngs[tid][m+2];
    int bb    = hs.bufMap[cid]; // buf begin

    if (zLen >= _cacheLineLen) {
      for (int v=0; v<hs.nVar; ++v) {
        int ij  = _haloRngs[tid][m] * is + _haloRngs[tid][m+1] * js;
        int cij = (_haloRngs[tid][m] - hs.chunks[6*cid]) * cis 
                + (_haloRngs[tid][m+1] - hs.chunks[6*cid+1]) * cjs;
        int vid = vs[v];
        for (int i=_haloRngs[tid][m]; i<_haloRngs[tid][m+3]; ++i) {
          int ij0  = ij;
          int cij0 = cij;
          for (int j=_haloRngs[tid][m+1]; j<_haloRngs[tid][m+4]; ++j) {
            std::copy(hs.rBuf + bb + cij + _haloRngs[tid][m+2] - hs.chunks[6*cid+2],
                      hs.rBuf + bb + cij + _haloRngs[tid][m+5] - hs.chunks[6*cid+2],
                      d[vid] + ij + _haloRngs[tid][m+2]);
            ij += js;
            cij += cjs;
          }
          ij  = ij0  + is;
          cij = cij0 + cis;
        }
        bb += cVol;
      }// end for v
    }// end if
    else {
      for (int v=0; v<hs.nVar; ++v) {
        int ij  = _haloRngs[tid][m] * is + _haloRngs[tid][m+1] * js;
        int cij = (_haloRngs[tid][m] - hs.chunks[6*cid]) * cis 
                + (_haloRngs[tid][m+1] - hs.chunks[6*cid+1]) * cjs;
        int vid = vs[v];
        for (int i=_haloRngs[tid][m]; i<_haloRngs[tid][m+3]; ++i) {
          int ij0  = ij;
          int cij0 = cij;
          for (int j=_haloRngs[tid][m+1]; j<_haloRngs[tid][m+4]; ++j) {
            for (int k=_haloRngs[tid][m+2]; k<_haloRngs[tid][m+5]; ++k)
              d[vid][ij + k] = hs.rBuf[bb + cij + k - hs.chunks[6*cid+2]];
            ij += js;
            cij += cjs;
          }
          ij  = ij0  + is;
          cij = cij0 + cis;
        }
        bb += cVol;
      }// end for v
    }// end else
  }// end for m

  return 0;
}
