#include "SORRB.h"
#include <fstream>
#include <algorithm>
#include <omp.h>

int SORRB::analyze_time()
{
  switch (_model) {
    case ParallelModel::BSP_MPI:
      _analyze_time_bsp_mpi();
      break;
    case ParallelModel::BSP_Hybrid:
      _analyze_time_bsp_hybrid();
      break;
    case ParallelModel::Overlap:
      _analyze_time_funneled();
      break;
    case ParallelModel::Fused:
      _analyze_time_bsp_hybrid();
      break;
    case ParallelModel::WaveFront:
      _analyze_time_bsp_hybrid();
      break;
  }

  return 0;
}


int SORRB::smooth_fused_cb(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
                    HaloArray& b, HaloArray& p, int nIter)
{
  // range work on
  int rngs[6], tid=omp_get_thread_num(), nh = p.num_halo()-1;
  p.get_thread_range(tid, rngs);
  // create cache block
  double *cb = new double [(_cbSizes[0] + 2*(nh-1)) * (_cbSizes[1]+2*(nh-1)) * (_cbSizes[2]+2*(nh-1))];
  // number of cache block used, the last one may not be fully used
  int nCB[3];
  for (int i=0; i<3; ++i) {
    nCB[i] = (rngs[i+3] - rngs[i]) / _cbSizes[i];
    nCB[i] = (rngs[i+3] - rngs[i]) % _cbSizes[i] ? nCB[i]+1 : nCB[i];
  }

  for (int m=0; m<nIter; ++m) {
    // smooth cache blocks
    _t[tid][SmootherTime::Comp] -= omp_time();
    int cbRngs[6];
    for (int i=0; i<nCB[0]; ++i) {
      for (int j=0; j<nCB[1]; ++j) {
        for (int k=0; k<nCB[2]; ++k) {
          for (int l=0; l<3; ++l) {
            cbRngs[l]   = rngs[l] + l* _cbSizes[l] - nh + 1;
            cbRngs[l+3] = std::min(cbRngs[l]+_cbSizes[l]+nh-1, rngs[l+3]) + nh - 1;
          }
          _smooth_range_fused_cb(rngs, cbRngs, _ptMesh->step(), rhox, rhoy, rhoz, b, p, cb);
        }
      }
    }
    _t[tid][SmootherTime::Comp] += omp_time();
    #pragma omp barrier
    #pragma omp single 
      std::swap(p.f, _buf);
    // pack
    _t[tid][SmootherTime::Pack] -= omp_time();
    _ht.pack_halo(p);
    _t[tid][SmootherTime::Pack] += omp_time();
    #pragma omp barrier
    //communication
    #pragma omp master
    {
      if (_isSyncComm)  MPI_Barrier(MPI_COMM_WORLD);
      _t[tid][SmootherTime::Comm] -= omp_time();
      _ht.update_halo();
      _t[tid][SmootherTime::Comm] += omp_time();
    }
    #pragma omp barrier
    //unpack
    _t[tid][SmootherTime::Unpack] -= omp_time();
    _ht.unpack_halo(p);
    _t[tid][SmootherTime::Unpack] += omp_time();
    #pragma omp barrier    
  }

  delete [] cb;

  return 0;
}


int SORRB::_smooth_bsp_mpi(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
                              HaloArray& b, HaloArray& p)
{
  // range work on
  int rngs[6];
  p.get_local_range(rngs);

  // red

  _t[0][SmootherTime::Comp] -= mpi_time();
  _smooth_color_range(rngs, Color::Red, _ptMesh->step(), rhox, rhoy, rhoz, b, p);
  _t[0][SmootherTime::Comp] += mpi_time();
  _t[0][SmootherTime::Pack] -= mpi_time();
  _ht.pack_halo(p);
  _t[0][SmootherTime::Pack] += mpi_time();
  if (_isSyncComm) MPI_Barrier(MPI_COMM_WORLD);
  _t[0][SmootherTime::Comm]   -= mpi_time();
  _ht.update_halo();
  _t[0][SmootherTime::Comm]   += mpi_time();
  _t[0][SmootherTime::Unpack] -= mpi_time();
  _ht.unpack_halo(p);
  _t[0][SmootherTime::Unpack] += mpi_time();

  // black
  _t[0][SmootherTime::Comp] -= mpi_time();
  _smooth_color_range(rngs, Color::Black, _ptMesh->step(), rhox, rhoy, rhoz, b, p);
  _t[0][SmootherTime::Comp] += mpi_time();
  _t[0][SmootherTime::Pack] -= mpi_time();
  _ht.pack_halo(p);
  _t[0][SmootherTime::Pack] += mpi_time();
  if (_isSyncComm) MPI_Barrier(MPI_COMM_WORLD);
  _t[0][SmootherTime::Comm]   -= mpi_time();
  _ht.update_halo();
  _t[0][SmootherTime::Comm]   += mpi_time();
  _t[0][SmootherTime::Unpack] -= mpi_time();
  _ht.unpack_halo(p);
  _t[0][SmootherTime::Unpack] += mpi_time();

  return 0;
}


int SORRB::_smooth_bsp_hybrid(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
                              HaloArray& b, HaloArray& p)
{
  // range work on
  int rngs[6], tid=omp_get_thread_num();
  p.get_thread_range(tid, rngs);

  // red
  _t[tid][SmootherTime::Comp] -= omp_time();
  _smooth_color_range(rngs, Color::Red, _ptMesh->step(), rhox, rhoy, rhoz, b, p);
  _t[tid][SmootherTime::Comp] += omp_time();
  #pragma omp barrier
  _t[tid][SmootherTime::Pack] -= omp_time();
  _ht.pack_halo(p);
  _t[tid][SmootherTime::Pack] += omp_time();
  #pragma omp barrier
  #pragma omp master
  {
    if (_isSyncComm)  MPI_Barrier(MPI_COMM_WORLD);
    _t[tid][SmootherTime::Comm] -= omp_time();
    _ht.update_halo();
    _t[tid][SmootherTime::Comm] += omp_time();
  }
  #pragma omp barrier
  _t[tid][SmootherTime::Unpack] -= omp_time();
  _ht.unpack_halo(p);
  _t[tid][SmootherTime::Unpack] += omp_time();
  #pragma omp barrier

  // black
  _t[tid][SmootherTime::Comp] -= omp_time();
  _smooth_color_range(rngs, Color::Black, _ptMesh->step(), rhox, rhoy, rhoz, b, p);
  _t[tid][SmootherTime::Comp] += omp_time();
  #pragma omp barrier
  _t[tid][SmootherTime::Pack] -= omp_time();
  _ht.pack_halo(p);
  _t[tid][SmootherTime::Pack] += omp_time();
  #pragma omp barrier
  #pragma omp master
  {
    if (_isSyncComm)  MPI_Barrier(MPI_COMM_WORLD);
    _t[tid][SmootherTime::Comm] -= omp_time();
    _ht.update_halo();
    _t[tid][SmootherTime::Comm] += omp_time();
  }
  #pragma omp barrier
  _t[tid][SmootherTime::Unpack] -= omp_time();
  _ht.unpack_halo(p);
  _t[tid][SmootherTime::Unpack] += omp_time();
  #pragma omp barrier

  return 0;
}


int SORRB::_smooth_funneled(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
                            HaloArray& b, HaloArray& p)
{
  int tid = omp_get_thread_num();

  // exit if single thread
  if (_ptDd->num_thread() < 2) {
    if (_ptDd->is_root())
      std::cout << "Error: funneled only works with >1 threads" << std::endl;
    MPI_Finalize();
  }

  // red
  _t[tid][SmootherTime::Outer] -= omp_time();
  for (unsigned int i=0; i<_thrdOutRngs[tid].size(); i+=6)
    _smooth_color_range(&_thrdOutRngs[tid][i], Color::Red, _ptMesh->step(), rhox, rhoy, rhoz, b, p);
  _t[tid][SmootherTime::Outer] += omp_time();
  #pragma omp barrier
  _t[tid][SmootherTime::Pack] -= omp_time();
  _ht.pack_halo(p);
  _t[tid][SmootherTime::Pack] += omp_time();
  #pragma omp barrier
  if (tid == 0) {
    if (_isSyncComm)  MPI_Barrier(MPI_COMM_WORLD);
    _t[tid][SmootherTime::Comm] -= omp_time();
    _ht.update_halo();
    _t[tid][SmootherTime::Comm] += omp_time();
  }
  else {
    _t[tid][SmootherTime::Inner] -= omp_time();
    _smooth_color_range(&_thrdInRngs[tid][0], Color::Red, _ptMesh->step(), rhox, rhoy, rhoz, b, p);
    _t[tid][SmootherTime::Inner] += omp_time();
  }
  #pragma omp barrier
  _t[tid][SmootherTime::Unpack] -= omp_time();
  _ht.unpack_halo(p);
  _t[tid][SmootherTime::Unpack] += omp_time();
  #pragma omp barrier

  // black
  _t[tid][SmootherTime::Outer] -= omp_time();
  for (unsigned int i=0; i<_thrdOutRngs[tid].size(); i+=6)
    _smooth_color_range(&_thrdOutRngs[tid][i], Color::Black, _ptMesh->step(), rhox, rhoy, rhoz, b, p);
  _t[tid][SmootherTime::Outer] += omp_time();
  #pragma omp barrier
  _t[tid][SmootherTime::Pack] -= omp_time();
  _ht.pack_halo(p);
  _t[tid][SmootherTime::Pack] += omp_time();
  #pragma omp barrier
  if (tid == 0) {
    if (_isSyncComm)  MPI_Barrier(MPI_COMM_WORLD);
    _t[tid][SmootherTime::Comm] -= omp_time();
    _ht.update_halo();
    _t[tid][SmootherTime::Comm] += omp_time();
  }
  else {
    _t[tid][SmootherTime::Inner] -= omp_time();
    _smooth_color_range(&_thrdInRngs[tid][0], Color::Black, _ptMesh->step(), rhox, rhoy, rhoz, b, p);
    _t[tid][SmootherTime::Inner] += omp_time();
  }
  #pragma omp barrier
  _t[tid][SmootherTime::Unpack] -= omp_time();
  _ht.unpack_halo(p);
  _t[tid][SmootherTime::Unpack] += omp_time();
  #pragma omp barrier

  return 0;
}


int SORRB::_smooth_fused(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
                         HaloArray& b, HaloArray& p)
{
  // range work on
  int rngs[6], tid=omp_get_thread_num(), nh = p.num_halo()-1;

  while (nh > 0) {
    // red, disgard one halo
    p.get_thread_range_dh(tid, nh, rngs);
    _t[tid][SmootherTime::Comp] -= omp_time();
    _smooth_color_range(rngs, Color::Red, _ptMesh->step(), rhox, rhoy, rhoz, b, p);
    _t[tid][SmootherTime::Comp] += omp_time();  
    --nh;
    #pragma omp barrier
    // black, disgard one halo
    p.get_thread_range_dh(tid, nh, rngs);
    _t[tid][SmootherTime::Comp] -= omp_time();
    _smooth_color_range(rngs, Color::Black, _ptMesh->step(), rhox, rhoy, rhoz, b, p);
    _t[tid][SmootherTime::Comp] += omp_time(); 
    --nh;
    #pragma omp barrier
  }
  // pack
  _t[tid][SmootherTime::Pack] -= omp_time();
  _ht.pack_halo(p);
  _t[tid][SmootherTime::Pack] += omp_time();
  #pragma omp barrier
  //communication
  #pragma omp master
  {
    if (_isSyncComm)  MPI_Barrier(MPI_COMM_WORLD);
    _t[tid][SmootherTime::Comm] -= omp_time();
    _ht.update_halo();
    _t[tid][SmootherTime::Comm] += omp_time();
  }
  #pragma omp barrier
  //unpack
  _t[tid][SmootherTime::Unpack] -= omp_time();
  _ht.unpack_halo(p);
  _t[tid][SmootherTime::Unpack] += omp_time();
  #pragma omp barrier

  return 0;
}


int SORRB::_smooth_wavefront(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
                              HaloArray& b, HaloArray& p)
{
  int rngs[6], is = p.stride_i(), js = p.stride_j(), nh = p.num_halo();
  int tid = omp_get_thread_num();
  double h = _ptMesh->step();
  p.get_local_range(rngs);

  // wavefront computation
  _t[tid][SmootherTime::Comp] -= omp_get_wtime();
  for (int i=rngs[0]-nh+1; i<rngs[3]+nh-1; ++i) {
    for (int m=0; m<std::min(nh, (i-rngs[0]+nh+1)/2); ++m) {
      #pragma omp for
      for (int j=rngs[1]-nh+1+m; j<rngs[4]+nh-1-m; ++j) {
        for (int k=rngs[2]-nh+1+m; k<rngs[5]+nh-1-m; ++k) {
          if ((i-m+j+k+m) % 2 == 0) {
            int ijk = (i-m)*is + j*js + k;
            p[ijk] = ( p[ijk- 1]/rhoz[ijk-1]  + p[ijk+ 1]/rhoz[ijk] 
                     + p[ijk-js]/rhoy[ijk-js] + p[ijk+js]/rhoy[ijk]
                     + p[ijk-is]/rhox[ijk-is] + p[ijk+is]/rhox[ijk] - h*h*b[ijk])
                   / ( 1.0/rhoz[ijk-1] + 1.0/rhoz[ijk]    + 1.0/rhoy[ijk-js]
                     + 1.0/rhoy[ijk]   + 1.0/rhox[ijk-is] + 1.0/rhox[ijk]);
          }
        }
      }//end for j
    }//end for m
  }//end for i
  _t[tid][SmootherTime::Comp] += omp_get_wtime();
  // pack
  _t[tid][SmootherTime::Pack] -= omp_get_wtime();
  _ht.pack_halo(p);
  _t[tid][SmootherTime::Pack] += omp_get_wtime();
  #pragma omp barrier
  //communication
  #pragma omp master
  {
    if (_isSyncComm)  MPI_Barrier(MPI_COMM_WORLD);
    _t[tid][SmootherTime::Comm] -= omp_get_wtime();
    _ht.update_halo();
    _t[tid][SmootherTime::Comm] += omp_get_wtime();
  }
  #pragma omp barrier
  //unpack
  _t[tid][SmootherTime::Unpack] -= omp_get_wtime();
  _ht.unpack_halo(p);
  _t[tid][SmootherTime::Unpack] += omp_get_wtime();
  #pragma omp barrier

  return 0;
}


int SORRB::smooth_pipeline(HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
                           HaloArray& b, HaloArray& p, int nIter)
{
  int rngs[6], sizes[3], tid=omp_get_thread_num();
  _ptDd->get_local_size(sizes);
  int cutBegin = _hts[0].num_halo(), cutEnd = cutBegin + sizes[2]/_nPipe;

  // first pipe
  copy(_thrdInRngs[tid].begin(), _thrdInRngs[tid].begin()+6, rngs);
  if (tid != 0) {
    rngs[5] = cutEnd;
    _smooth_color_range(rngs, Color::Red, _ptMesh->step(), rhox, rhoy, rhoz, b, p);
  }
  #pragma omp barrier
  _hts[0].pack_halo(p);
  #pragma omp barrier

  // iterations inbetween
  for (int i=1; i<nIter*_nPipe*2; ++i) {
    Color c = (Color)(i / _nPipe % 2);
    int iPipe   = i % _nPipe;
    int im1Pipe = (i-1) % _nPipe;
    if (tid == 0) {
      _hts[im1Pipe].update_halo_chunk_begin();
      _hts[im1Pipe].update_halo_chunk_end();
    }
    else if (tid != 0) {
      rngs[2] = _hts[0].num_halo() + iPipe*sizes[2]/_nPipe;
      rngs[5] = rngs[2] + sizes[2]/_nPipe;
      _smooth_color_range(rngs, c, _ptMesh->step(), rhox, rhoy, rhoz, b, p);
    }
    #pragma omp barrier
    _hts[im1Pipe].unpack_halo(p);
    _hts[iPipe].pack_halo(p);
    #pragma omp barrier
  }

  // last pipe
  _hts[_nPipe-1].pack_halo(p);
  #pragma omp barrier
  if (tid == 0) {
    _hts[_nPipe].update_halo_chunk_begin();
    _hts[_nPipe].update_halo_chunk_end();
  }
  #pragma omp barrier
  _hts[_nPipe-1].unpack_halo(p);
  #pragma omp barrier

  return 0;
}


int SORRB::_analyze_time_bsp_mpi()
{
  double tAvgs[SMOOTHER_N_TIME], tMaxs[SMOOTHER_N_TIME];
  std::copy(_t[0], _t[0]+SMOOTHER_N_TIME, tAvgs);
  std::copy(_t[0], _t[0]+SMOOTHER_N_TIME, tMaxs);
  MPI_Allreduce(MPI_IN_PLACE, tAvgs, SMOOTHER_N_TIME, MPI_DOUBLE, MPI_SUM, _ptDd->comm());
  MPI_Allreduce(MPI_IN_PLACE, tMaxs, SMOOTHER_N_TIME, MPI_DOUBLE, MPI_MAX, _ptDd->comm());
  for (int i=0; i<SMOOTHER_N_TIME; ++i) 
    tAvgs[i] /= _ptDd->num_proc();

  if (_ptDd->is_root()) {
    std::cout << "Avg:       " << std::fixed << tAvgs[SmootherTime::Comp] << "\t"
              << tAvgs[SmootherTime::Pack] << "\t" 
              << tAvgs[SmootherTime::Comm] << "\t"
              << tAvgs[SmootherTime::Unpack] << "\t"
              << tAvgs[SmootherTime::Total] << std::endl;
    std::cout << "Max:       " << std::fixed << tMaxs[SmootherTime::Comp] << "\t"
              << tMaxs[SmootherTime::Pack] << "\t" 
              << tMaxs[SmootherTime::Comm] << "\t"
              << tMaxs[SmootherTime::Unpack] << "\t"
              << tMaxs[SmootherTime::Total] << std::endl;
  }

  struct {double v; int r;} ValRank;
  ValRank.v = _t[0][SmootherTime::Total];
  ValRank.r = _ptDd->rank();
  MPI_Allreduce(MPI_IN_PLACE, &ValRank, 1, MPI_DOUBLE_INT, MPI_MAXLOC, _ptDd->comm());
  if (_ptDd->rank() == ValRank.r)
    std::cout << "Max Proc : " << std::fixed << _t[0][SmootherTime::Comp] << "\t"
              << _t[0][SmootherTime::Pack] << "\t" 
              << _t[0][SmootherTime::Comm] << "\t"
              << _t[0][SmootherTime::Unpack] << "\t"
              << _t[0][SmootherTime::Total] << std::endl;
  
  return 0;
}


int SORRB::_analyze_time_bsp_hybrid()
{
  double tMaxs[SMOOTHER_N_TIME], tAvgs[SMOOTHER_N_TIME], imbls[SMOOTHER_N_TIME];
  double tThrdMaxs[SMOOTHER_N_TIME];
  std::copy(_t[0], _t[0]+SMOOTHER_N_TIME, tAvgs);
  std::copy(_t[0], _t[0]+SMOOTHER_N_TIME, imbls);

  struct {double v; int r;} ValRank;
  ValRank.r = _ptDd->rank();
  
  // use the max among threads as the time of that step, store it at averge time array
  // (max - min) as the imbalance
  for (int i=0; i<SMOOTHER_N_TIME; ++i) {
    for (int j=0; j<_ptDd->num_thread(); ++j) {
      imbls[i] = std::min(imbls[i],  _t[j][i]);
      tAvgs[i] = std::max(tAvgs[i],  _t[j][i]);
    }
    imbls[i]   = tAvgs[i] - imbls[i];
  }
  ValRank.v = tAvgs[SmootherTime::Total];
  std::copy(tAvgs, tAvgs+SMOOTHER_N_TIME, tMaxs);
  std::copy(tAvgs, tAvgs+SMOOTHER_N_TIME, tThrdMaxs);

  MPI_Allreduce(MPI_IN_PLACE, imbls, SMOOTHER_N_TIME, MPI_DOUBLE, MPI_MAX, _ptDd->comm());
  MPI_Allreduce(MPI_IN_PLACE, tAvgs, SMOOTHER_N_TIME, MPI_DOUBLE, MPI_SUM, _ptDd->comm());
  MPI_Allreduce(MPI_IN_PLACE, tMaxs, SMOOTHER_N_TIME, MPI_DOUBLE, MPI_MAX, _ptDd->comm());
  for (int i=0; i<SMOOTHER_N_TIME; ++i) 
    tAvgs[i] /= _ptDd->num_proc();

  if (_ptDd->is_root()) {
    std::cout << "Avg:       " << std::fixed << tAvgs[SmootherTime::Comp] << "\t"
              << tAvgs[SmootherTime::Pack] << "\t" 
              << tAvgs[SmootherTime::Comm] << "\t"
              << tAvgs[SmootherTime::Unpack] << "\t"
              << tAvgs[SmootherTime::Total] << std::endl;
    std::cout << "Max:       " << std::fixed << tMaxs[SmootherTime::Comp] << "\t"
              << tMaxs[SmootherTime::Pack] << "\t" 
              << tMaxs[SmootherTime::Comm] << "\t"
              << tMaxs[SmootherTime::Unpack] << "\t"
              << tMaxs[SmootherTime::Total] << std::endl;
    std::cout << "thrd imbl: " << std::fixed << imbls[SmootherTime::Comp] << "\t"
              << imbls[SmootherTime::Pack] << "\t" 
              << 0.0 << "\t"
              << imbls[SmootherTime::Unpack] << "\t"
              << imbls[SmootherTime::Total] << std::endl;
  }

  MPI_Allreduce(MPI_IN_PLACE, &ValRank, 1, MPI_DOUBLE_INT, MPI_MAXLOC, _ptDd->comm());
  if (_ptDd->rank() == ValRank.r)
    std::cout << "Max Proc : " << std::fixed << tThrdMaxs[SmootherTime::Comp] << "\t"
              << tThrdMaxs[SmootherTime::Pack] << "\t" 
              << tThrdMaxs[SmootherTime::Comm] << "\t"
              << tThrdMaxs[SmootherTime::Unpack] << "\t"
              << tThrdMaxs[SmootherTime::Total] << std::endl;

  return 0;
}


int SORRB::_analyze_time_funneled()
{
  double tMaxs[SMOOTHER_N_TIME], tAvgs[SMOOTHER_N_TIME], imbls[SMOOTHER_N_TIME];
  double tThrdMaxs[SMOOTHER_N_TIME];
  std::copy(_t[0], _t[0]+SMOOTHER_N_TIME, tAvgs);
  std::copy(_t[0], _t[0]+SMOOTHER_N_TIME, imbls);

  struct {double v; int r;} ValRank;
  ValRank.r = _ptDd->rank();
  
  // use the max among threads as the time of that step, store it at averge time array
  // (max - min) as the imbalance
  for (int i=0; i<SMOOTHER_N_TIME; ++i) {
    for (int j=0; j<_ptDd->num_thread(); ++j) {
      imbls[i] = std::min(imbls[i],  _t[j][i]);
      tAvgs[i] = std::max(tAvgs[i],  _t[j][i]);
    }
    imbls[i]   = tAvgs[i] - imbls[i];
  }
  // check the imbalance of inner separatly
  imbls[SmootherTime::Inner] = _t[1][SmootherTime::Inner];
  for (int j=1; j<_ptDd->num_thread(); ++j)
    imbls[SmootherTime::Inner] = std::min(_t[j][SmootherTime::Inner], imbls[SmootherTime::Inner]);
  imbls[SmootherTime::Inner] = tAvgs[SmootherTime::Inner] - imbls[SmootherTime::Inner];
  //
  ValRank.v = tAvgs[SmootherTime::Total];
  std::copy(tAvgs, tAvgs+SMOOTHER_N_TIME, tMaxs);
  std::copy(tAvgs, tAvgs+SMOOTHER_N_TIME, tThrdMaxs);

  MPI_Allreduce(MPI_IN_PLACE, imbls, SMOOTHER_N_TIME, MPI_DOUBLE, MPI_MAX, _ptDd->comm());
  MPI_Allreduce(MPI_IN_PLACE, tAvgs, SMOOTHER_N_TIME, MPI_DOUBLE, MPI_SUM, _ptDd->comm());
  MPI_Allreduce(MPI_IN_PLACE, tMaxs, SMOOTHER_N_TIME, MPI_DOUBLE, MPI_MAX, _ptDd->comm());
  for (int i=0; i<SMOOTHER_N_TIME; ++i) 
    tAvgs[i] /= _ptDd->num_proc();

  if (_ptDd->is_root()) {
    std::cout << "Avg:       " << std::fixed << tAvgs[SmootherTime::Outer] << "\t"
      << tAvgs[SmootherTime::Pack] << "\t" << tAvgs[SmootherTime::Comm] << "\t"
      << tAvgs[SmootherTime::Inner] << "\t" << tAvgs[SmootherTime::Unpack] << "\t"
      << tAvgs[SmootherTime::Total] << std::endl;
    std::cout << "Max:       " << std::fixed << tMaxs[SmootherTime::Outer] << "\t"
      << tMaxs[SmootherTime::Pack] << "\t" << tMaxs[SmootherTime::Comm] << "\t"
      << tMaxs[SmootherTime::Inner] << "\t" << tMaxs[SmootherTime::Unpack] << "\t"
      << tMaxs[SmootherTime::Total] << std::endl;
    std::cout << "thrd imbl: " << std::fixed << imbls[SmootherTime::Outer] << "\t"
      << imbls[SmootherTime::Pack] << "\t" << 0.0 << "\t"
      << imbls[SmootherTime::Inner] << "\t" << imbls[SmootherTime::Unpack] << "\t"
      << imbls[SmootherTime::Total] << std::endl;
  }

  MPI_Allreduce(MPI_IN_PLACE, &ValRank, 1, MPI_DOUBLE_INT, MPI_MAXLOC, _ptDd->comm());
  if (_ptDd->rank() == ValRank.r)
    std::cout << "Max Proc : " << std::fixed << tThrdMaxs[SmootherTime::Outer] << "\t"
      << tThrdMaxs[SmootherTime::Pack] << "\t" << tThrdMaxs[SmootherTime::Comm] << "\t"
      << tThrdMaxs[SmootherTime::Inner] << "\t" << tThrdMaxs[SmootherTime::Unpack] << "\t"
      << tThrdMaxs[SmootherTime::Total] << std::endl;

  return 0;
}


int SORRB::_smooth_range_fused_cb(const int rngs[6], const int cbRngs[6], const double h, 
      HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, HaloArray& b, HaloArray& p, double *cb)
{
  // stride
  int is  = p.stride_i(), js  = p.stride_j();
  int cbjs = _cbSizes[2],  cbis = cbjs * _cbSizes[1];
  int c = 0, nh = _ht.num_halo();;

  for (int i=cbRngs[0]; i<cbRngs[3]; ++i) {
    for (int j=cbRngs[1]; j<cbRngs[4]; ++j) {
      for (int k=cbRngs[2]; k<cbRngs[5]; ++k) {
        if (i+j+k % 2 == c) {
          int ijk   = i*is + j*js + k;
          int cbijk = (i-cbRngs[0])*cbis + (j-cbRngs[1])*cbjs + k-cbRngs[2];
           cb[cbijk] = ( p[ijk- 1]/rhoz[ijk-1]  + p[ijk+ 1]/rhoz[ijk] 
                        + p[ijk-js]/rhoy[ijk-js] + p[ijk+js]/rhoy[ijk]
                        + p[ijk-is]/rhox[ijk-is] + p[ijk+is]/rhox[ijk] - h*h*b[ijk])
                      / ( 1.0/rhoz[ijk-1] + 1.0/rhoz[ijk]    + 1.0/rhoy[ijk-js]
                        + 1.0/rhoy[ijk]   + 1.0/rhox[ijk-is] + 1.0/rhox[ijk]);
        }
      }
    }
  }

  for (int m=1; m<nh-1; ++m) {
    c = 1 - c;
    for (int i=cbRngs[0]+m; i<cbRngs[3]-m; ++i) {
      for (int j=cbRngs[1]+m; j<cbRngs[4]-m; ++j) {
        for (int k=cbRngs[2]+m; k<cbRngs[5]-m; ++k) {
          if (i+j+k % 2 == c) {
            int ijk   = i*is + j*js + k;
            int cbijk = (i-cbRngs[0])*cbis + (j-cbRngs[1])*cbjs + k-cbRngs[2];
             cb[cbijk] = ( cb[cbijk- 1]/rhoz[ijk-1]  + cb[cbijk+ 1]/rhoz[ijk] 
                          + cb[cbijk-js]/rhoy[ijk-js] + cb[cbijk+js]/rhoy[ijk]
                          + cb[cbijk-is]/rhox[ijk-is] + cb[cbijk+is]/rhox[ijk] - h*h*b[ijk])
                        / ( 1.0/rhoz[ijk-1] + 1.0/rhoz[ijk]    + 1.0/rhoy[ijk-js]
                          + 1.0/rhoy[ijk]   + 1.0/rhox[ijk-is] + 1.0/rhox[ijk]);
          }
        }// end k
      }// end j
    }// end i
  }// end m

  c = 1 - c;
  for (int i=cbRngs[0]+nh-1; i<cbRngs[3]-nh+1; ++i) {
    for (int j=cbRngs[1]+nh-1; j<cbRngs[4]-nh+1; ++j) {
      for (int k=cbRngs[2]+nh-1; k<cbRngs[5]-nh+1; ++k) {
        if ((i+j+k) % 2 != c) {
          int ijk = i*is + j*js + k;
          int cbijk = (i-cbRngs[0])*cbis + (j-cbRngs[1])*cbjs + k-cbRngs[2];
          _buf[ijk] = ( cb[cbijk- 1]/rhoz[ijk-1]  + cb[cbijk+ 1]/rhoz[ijk] 
                    + cb[cbijk-js]/rhoy[ijk-js] + cb[cbijk+js]/rhoy[ijk]
                    + cb[cbijk-is]/rhox[ijk-is] + cb[cbijk+is]/rhox[ijk] - h*h*b[ijk])
                  / ( 1.0/rhoz[ijk-1] + 1.0/rhoz[ijk]    + 1.0/rhoy[ijk-js]
                    + 1.0/rhoy[ijk]   + 1.0/rhox[ijk-is] + 1.0/rhox[ijk]);
        }
      }//end for k
    }//end for j
  }//end for i

  return 0;
}


int _smooth_color_range(const int rngs[6], const Color c, const double h, 
                        HaloArray& rhox, HaloArray& rhoy, HaloArray& rhoz, 
                        HaloArray& b, HaloArray& p)
{
  // stride
  int is = p.stride_i();
  int js = p.stride_j();

  if (c == Color::Red) {
    for (int i=rngs[0]; i<rngs[3]; ++i) {
      for (int j=rngs[1]; j<rngs[4]; ++j) {
        for (int k=rngs[2]; k<rngs[5]; ++k) {
          if ((i+j+k) % 2 == 0) {
            int ijk = i*is + j*js + k;
            p[ijk] = ( p[ijk- 1]/rhoz[ijk-1]  + p[ijk+ 1]/rhoz[ijk] 
                     + p[ijk-js]/rhoy[ijk-js] + p[ijk+js]/rhoy[ijk]
                     + p[ijk-is]/rhox[ijk-is] + p[ijk+is]/rhox[ijk] - h*h*b[ijk])
                   / ( 1.0/rhoz[ijk-1] + 1.0/rhoz[ijk]    + 1.0/rhoy[ijk-js]
                     + 1.0/rhoy[ijk]   + 1.0/rhox[ijk-is] + 1.0/rhox[ijk]);
          }
        }//end for k
      }//end for j
    }//end for i
  }
  else if (c == Color::Black) {
    for (int i=rngs[0]; i<rngs[3]; ++i) {
      for (int j=rngs[1]; j<rngs[4]; ++j) {
        for (int k=rngs[2]; k<rngs[5]; ++k) {
          if ((i+j+k) % 2 != 0) {
            int ijk = i*is + j*js + k;
            p[ijk] = ( p[ijk- 1]/rhoz[ijk-1]  + p[ijk+ 1]/rhoz[ijk] 
                     + p[ijk-js]/rhoy[ijk-js] + p[ijk+js]/rhoy[ijk]
                     + p[ijk-is]/rhox[ijk-is] + p[ijk+is]/rhox[ijk] - h*h*b[ijk])
                   / ( 1.0/rhoz[ijk-1] + 1.0/rhoz[ijk]    + 1.0/rhoy[ijk-js]
                     + 1.0/rhoy[ijk]   + 1.0/rhox[ijk-is] + 1.0/rhox[ijk]);
          }
        }//end for k
      }//end for j
    }//end for i
  }

  return 0;
}


int _smooth_color_range(const int rngs[6], const Color c, const double h,\
                        HaloArray& b, HaloArray& p)
{
  // stride
  int is = p.stride_i();
  int js = p.stride_j();

  if (c == Color::Red) {
    for (int i=rngs[0]; i<rngs[3]; ++i) {
      for (int j=rngs[1]; j<rngs[4]; ++j) {
        for (int k=rngs[2]; k<rngs[5]; ++k) {
          if ((i+j+k) % 2 == 0) {
            int ijk = i*is + j*js + k;
            p[ijk] = ( p[ijk- 1] + p[ijk+ 1] + p[ijk-js] + p[ijk+js]
                     + p[ijk-is] + p[ijk+is] - h*h*b[ijk]) / 6.0;
          }
        }//end for k
      }//end for j
    }//end for i
  }
  else if (c == Color::Black) {
    for (int i=rngs[0]; i<rngs[3]; ++i) {
      for (int j=rngs[1]; j<rngs[4]; ++j) {
        for (int k=rngs[2]; k<rngs[5]; ++k) {
          if ((i+j+k) % 2 != 0) {
            int ijk = i*is + j*js + k;
            p[ijk] = ( p[ijk- 1] + p[ijk+ 1] + p[ijk-js] + p[ijk+js]
                     + p[ijk-is] + p[ijk+is] - h*h*b[ijk]) / 6.0;
          }
        }//end for k
      }//end for j
    }//end for i
  }

  return 0;
}
