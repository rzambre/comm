#include "Solver.h"
#include <cmath>
#include <algorithm>

#ifdef PAPI
#include <papi.h>
#endif

Solver::Solver(DomainDecomp& dd, UniMesh& mesh, int nh):
  _ptDd(&dd),
  _hs(dd, nh, 1),
  _hts(NULL),
  _nHalo(nh),
  _nMargin(2),
  _isSyncComm(false)
{
  _h  =  mesh.step();
  vector<double> zeros(TIMER_MAX_ITEM, 0.0);
  _t.insert(_t.begin(), _ptDd->num_thread(), zeros);
#ifdef PAPI
  _llcm = new double [_ptDd->num_thread()];
  std::fill(_llcm, _llcm + _ptDd->num_thread(), 0.0);
#endif
}

Solver::~Solver()
{
#ifdef PAPI
  delete [] _llcm;
#endif
}

void Solver::set_from_option()
{
  bool isSet;
  _hs.set_from_option();
  get_option("synccomm", CmdOption::Bool, 1, &_isSyncComm, &isSet);
  get_option("nmargin",  CmdOption::Int,  1, &_nMargin,    &isSet);
}


int Solver::setup()
{
  int nThrd = _ptDd->num_thread();
  int nPipe = _hs.num_pipe(); 
  vector<int> cids;

  _hts = new HaloTransfer [nPipe];
  for (int i=0; i<nPipe; ++i) _hts[i].init(_ptDd, _nHalo);

  if (_ptDd->is_neighbor_star()) {
    _hs.setup_star();
    if (nPipe == 1) _hts[0].set_halo_range(_hs, 0, 6, nThrd);
  }
  else {
    _hs.setup_box();
    if (nPipe == 1) _hts[0].set_halo_range(_hs, 0, 26, nThrd);
  }

  if (nPipe > 1) {
    for (int p=0; p<nPipe; ++p)
      _hts[p].setup_pack_pipeline(p, _hs, nThrd-1);
    for (int p=0; p<nPipe-1; ++p)
      _hts[p].setup_unpack_pipeline(p, _hs, nThrd-1);
  }// end if

  return 0;
}


int Solver::setup_parallel_mode(int mode)
{
  _mode = mode;
  if (mode == SolverMode::Overlap) _setup_overlap_range();
  return 0;
}


int Solver::solve(int nIter)
{
  if (_mode == MPI_BSP) {
    _t[0][SolverTime::Total] -=mpi_time();
#ifdef PAPI
    int set = PAPI_NULL;
    long long  val = 0;
    PAPI_create_eventset(&set);
    PAPI_add_event(set, PAPI_DP_OPS);
    PAPI_start(set);
#endif
    _solve_mpi_bsp(nIter);
#ifdef PAPI
    PAPI_stop(set, &val);
    _llcm[0] = static_cast<double>(val);
#endif
    _t[0][SolverTime::Total] +=mpi_time();
#ifdef PAPI
    PAPI_cleanup_eventset(set);
    PAPI_destroy_eventset(&set);
    MPI_Allreduce(MPI_IN_PLACE, _llcm, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    _llcm[0] /= _ptDd->num_node();
    if (_ptDd->is_root()) std::cout << "papi flops: " << _llcm[0] << std::endl;
#endif
  }
  else {
    int tid = omp_get_thread_num();
#ifdef PAPI
    PAPI_register_thread();
    int set = PAPI_NULL;
    long long  val = 0;
    PAPI_create_eventset(&set);
    PAPI_add_event(set, PAPI_DP_OPS);
    PAPI_start(set);
#endif
    _t[tid][SolverTime::Total] -= omp_time();
    switch(_mode) {
      case Funneled: _solve_funneled(nIter);
      break;
      case Overlap: _solve_overlap(nIter);
      break;
      case Pipeline: _solve_pipeline(nIter);
      break;
      case DeepHalo: _solve_deephalo(nIter);
      break;
      case DeepHaloTile: _solve_deephalo_tile(nIter);
      break;
    }
    _t[tid][SolverTime::Total] += omp_time();
#ifdef PAPI
    PAPI_stop(set, &val);
    _llcm[tid] = static_cast<double>(val);
#endif

#ifdef PAPI
  PAPI_cleanup_eventset(set);
  PAPI_destroy_eventset(&set);
  PAPI_unregister_thread();
#pragma omp master
{
  for (int i=1; i<_ptDd->num_thread(); ++i) _llcm[0] += _llcm[i];
  MPI_Allreduce(MPI_IN_PLACE, _llcm, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  _llcm[0] /= _ptDd->num_node();
  if (_ptDd->is_root()) std::cout << "papi flops: " << _llcm[0] << std::endl;
}
#endif
  }

  return 0;
}


int Solver::analyze_time()
{
  switch (_mode) {
    case MPI_BSP:
    case Funneled:
    case Pipeline:
    case DeepHalo:
    case DeepHaloTile: _analyze_time_hybrid();
    break;
    case Overlap: _analyze_time_overlap();
    break;
  }

  return 0;
}


void Solver::clear_history()
{
  for (unsigned int i=0; i<_t.size(); ++i) 
   std::fill(_t[i].begin(), _t[i].end(), 0.0);
}


void Solver::copy_var(int i, double* var)
{
  int szs[3];
  _ptDd->get_local_size(szs);
  std::copy(_d[i], _d[i]+(szs[0]+2*_nHalo)*(szs[1]+2*_nHalo)*(szs[2]+2*_nHalo), var);
}


int Solver::_setup_overlap_range()
{
  // set range
  int rngs[6], inRngs[6], outRngs[6][6];
  _ptDd->get_local_size(rngs); 
  for (int i=0; i<3; ++i)  rngs[i+3] = rngs[i] + _nHalo;
  std::fill(rngs, rngs+3, _nHalo);

  // inner range
  for (int i=0; i<3; ++i) {
    inRngs[i]   = rngs[i]   + _nMargin;
    inRngs[i+3] = rngs[i+3] - _nMargin;
  }
  _setup_thread_inner_range(inRngs);

  // outer range
  // x-
  outRngs[0][0] = rngs[0];            outRngs[0][3] = rngs[0] + _nMargin;
  outRngs[0][1] = rngs[1];            outRngs[0][4] = rngs[4];
  outRngs[0][2] = rngs[2];            outRngs[0][5] = rngs[5];
  // y-
  outRngs[1][0] = rngs[0] + _nMargin; outRngs[1][3] = rngs[3] - _nMargin;
  outRngs[1][1] = rngs[1];            outRngs[1][4] = rngs[1] + _nMargin;
  outRngs[1][2] = rngs[2];            outRngs[1][5] = rngs[5];
  // z-
  outRngs[2][0] = rngs[0] + _nMargin; outRngs[2][3] = rngs[3] - _nMargin;
  outRngs[2][1] = rngs[1] + _nMargin; outRngs[2][4] = rngs[4] - _nMargin;
  outRngs[2][2] = rngs[2];            outRngs[2][5] = rngs[2] + _nMargin;
  // x+
  outRngs[3][0] = rngs[3] - _nMargin; outRngs[3][3] = rngs[3];
  outRngs[3][1] = rngs[1];            outRngs[3][4] = rngs[4];
  outRngs[3][2] = rngs[2];            outRngs[3][5] = rngs[5];
  // y+
  outRngs[4][0] = rngs[0] + _nMargin; outRngs[4][3] = rngs[3] - _nMargin;
  outRngs[4][1] = rngs[4] - _nMargin; outRngs[4][4] = rngs[4];
  outRngs[4][2] = rngs[2];            outRngs[4][5] = rngs[5];
  // z+
  outRngs[5][0] = rngs[0] + _nMargin; outRngs[5][3] = rngs[3] - _nMargin;
  outRngs[5][1] = rngs[1] + _nMargin; outRngs[5][4] = rngs[4] - _nMargin;
  outRngs[5][2] = rngs[5] - _nMargin; outRngs[5][5] = rngs[5];
  // divide among threads
  _setup_thread_outer_range(outRngs);

  return 0;
}


int Solver::_setup_thread_inner_range(int inRngs[6])
{
  int nThrd    = _ptDd->num_thread();
  int tRngs[6] = {0};

  // divide the inner domain with first nThrd-1 threads
  for (int tid=0; tid<nThrd-1; ++tid) {
    compute_thread_range(inRngs, nThrd-1, tid, tRngs);
    _thrdInRngs.emplace_back(tRngs, tRngs+6);
  }
  // last thread is not used in inner computation
  std::fill(tRngs, tRngs+6, 0);
  _thrdInRngs.emplace_back(tRngs, tRngs+6);

  return 0;
}


int Solver::_setup_thread_outer_range(int outRngs[6][6])
{
  int szs[3];
  _ptDd->get_local_size(szs);
  int nThrd        = _ptDd->num_thread();
  int totalOutCell = 2*( szs[1]*szs[2]*_nMargin 
                       + szs[2]*(szs[0] - 2*_nMargin)*_nMargin
                       + (szs[0]-2*_nMargin)*(szs[1]-2*_nMargin)*_nMargin);

  // init #cells per thread, handle cases where not evenszs[1] divided
  std::vector<int> nThrdCells(nThrd, totalOutCell/nThrd);
  for (int i=0; i<totalOutCell%nThrd; ++i)  ++nThrdCells[i];

  // init chunk range equal outRngs
  int chunkRngs[6][6];
  for (unsigned int i=0; i<6; ++i) 
    std::copy(outRngs[i], outRngs[i]+6, chunkRngs[i]);

  _thrdOutRngs.clear();

  // cut direction and normal area
  int cutDirs[6]    = {1, 0, 0, 1, 0, 0};
  int chunkAreas[6] = {szs[2]*_nMargin, szs[2]*_nMargin, _nMargin*(szs[1]-2*_nMargin),
                       szs[2]*_nMargin, szs[2]*_nMargin, _nMargin*(szs[1]-2*_nMargin)}; 

  // set thread's outer range
  // start from first outer chunk and init cut position
  int ic = 0; // index of chunk
  int chunkCutStart = chunkRngs[ic][cutDirs[ic]];
  //
  for (int tid=0; tid<nThrd; ++tid) {
    // add dummy vector
    std::vector<int> vecNull;
    _thrdOutRngs.push_back(vecNull);

    int thrdRoom = nThrdCells[tid];
    while (thrdRoom > 0) {
      // remainder volume of chunk
      int chunkVol = (chunkRngs[ic][cutDirs[ic]+3] - chunkCutStart) * chunkAreas[ic];
      // if chunk volume fits in thread room
      if (thrdRoom >= chunkVol) {
        thrdRoom -= chunkVol;
        // add the remainding chunk range to thread
        for (int i=0; i<6; ++i) {
          if (i == cutDirs[ic])
            _thrdOutRngs[tid].push_back(chunkCutStart);
          else
            _thrdOutRngs[tid].push_back(chunkRngs[ic][i]);
        }
        // move on to next chunk or terminates
        ++ic;
        if (ic == 6) break;
        // advance cut start pos
        chunkCutStart = chunkRngs[ic][cutDirs[ic]];
      }
      // chunk volume exceeds thread room
      else {
        int cutLen = static_cast<int>( round(static_cast<double>(thrdRoom) 
                                     / chunkAreas[ic]));
        // if thread room is small, note that the last thread's room will 
        // cover remaining chunk, so tid+1 will not overflow
        if (cutLen == 0) {
          nThrdCells[tid+1] += thrdRoom;
          break;
        }
        // append part of chunk to thread space
        thrdRoom -= cutLen * chunkAreas[ic];
        for (int i=0; i<6; ++i) {
          if (i == cutDirs[ic])  
            _thrdOutRngs[tid].push_back(chunkCutStart);
          else if (i == cutDirs[ic]+3) 
            _thrdOutRngs[tid].push_back(chunkCutStart + cutLen);
          else
            _thrdOutRngs[tid].push_back(chunkRngs[ic][i]);
        }
        // advance cut start pos
        chunkCutStart +=  cutLen;
      }
      // if current thread assigned more cells than average
      if (thrdRoom < 0) nThrdCells[tid+1] += thrdRoom;
    }// end while
  }// end for
  
  return 0;
}


int Solver::_analyze_time_hybrid()
{
  double tMaxs[TIMER_MAX_ITEM], tAvgs[TIMER_MAX_ITEM], imbls[TIMER_MAX_ITEM];
  double tThrdMaxs[TIMER_MAX_ITEM];
  std::copy(_t[0].begin(), _t[0].end(), tAvgs);
  std::copy(_t[0].begin(), _t[0].end(), imbls);

  struct {double v; int r;} ValRank;
  ValRank.r = _ptDd->rank();
  
  // use the max among threads as the time of that step, store it at averge time array
  // (max - min) as the imbalance
  for (int i=0; i<TIMER_MAX_ITEM; ++i) {
    for (int j=0; j<_ptDd->num_thread(); ++j) {
      imbls[i] = std::min(imbls[i],  _t[j][i]);
      tAvgs[i] = std::max(tAvgs[i],  _t[j][i]);
    }
    imbls[i]   = tAvgs[i] - imbls[i];
  }
  ValRank.v = tAvgs[SolverTime::Total];
  std::copy(tAvgs, tAvgs+TIMER_MAX_ITEM, tMaxs);
  std::copy(tAvgs, tAvgs+TIMER_MAX_ITEM, tThrdMaxs);

  MPI_Allreduce(MPI_IN_PLACE, imbls, TIMER_MAX_ITEM, MPI_DOUBLE, MPI_MAX, _ptDd->comm());
  MPI_Allreduce(MPI_IN_PLACE, tAvgs, TIMER_MAX_ITEM, MPI_DOUBLE, MPI_SUM, _ptDd->comm());
  MPI_Allreduce(MPI_IN_PLACE, tMaxs, TIMER_MAX_ITEM, MPI_DOUBLE, MPI_MAX, _ptDd->comm());
  for (int i=0; i<TIMER_MAX_ITEM; ++i) 
    tAvgs[i] /= _ptDd->num_proc();

  if (_ptDd->is_root()) {
    std::cout << "Avg:       " << std::fixed << tAvgs[SolverTime::Comp] << "\t"
              << tAvgs[SolverTime::Pack] << "\t" 
              << tAvgs[SolverTime::Comm] << "\t"
              << tAvgs[SolverTime::Unpack] << "\t"
              << tAvgs[SolverTime::Total] << std::endl;
    std::cout << "Max:       " << std::fixed << tMaxs[SolverTime::Comp] << "\t"
              << tMaxs[SolverTime::Pack] << "\t" 
              << tMaxs[SolverTime::Comm] << "\t"
              << tMaxs[SolverTime::Unpack] << "\t"
              << tMaxs[SolverTime::Total] << std::endl;
    std::cout << "thrd imbl: " << std::fixed << imbls[SolverTime::Comp] << "\t"
              << imbls[SolverTime::Pack] << "\t" 
              << 0.0 << "\t"
              << imbls[SolverTime::Unpack] << "\t"
              << imbls[SolverTime::Total] << std::endl;
  }
  MPI_Barrier(MPI_COMM_WORLD);

  MPI_Allreduce(MPI_IN_PLACE, &ValRank, 1, MPI_DOUBLE_INT, MPI_MAXLOC, _ptDd->comm());
  if (_ptDd->rank() == ValRank.r)
    std::cout << "Max Proc : " << std::fixed << tThrdMaxs[SolverTime::Comp] << "\t"
              << tThrdMaxs[SolverTime::Pack] << "\t" 
              << tThrdMaxs[SolverTime::Comm] << "\t"
              << tThrdMaxs[SolverTime::Unpack] << "\t"
              << tThrdMaxs[SolverTime::Total] << std::endl;

  return 0;
}


int Solver::_analyze_time_overlap()
{
  double tMaxs[TIMER_MAX_ITEM], tAvgs[TIMER_MAX_ITEM], imbls[TIMER_MAX_ITEM];
  double tThrdMaxs[TIMER_MAX_ITEM];
  std::copy(_t[0].begin(), _t[0].end(), tAvgs);
  std::copy(_t[0].begin(), _t[0].end(), imbls);

  struct {double v; int r;} ValRank;
  ValRank.r = _ptDd->rank();
  
  // use the max among threads as the time of that step, store it at averge time array
  // (max - min) as the imbalance
  for (int i=0; i<TIMER_MAX_ITEM; ++i) {
    for (int j=0; j<_ptDd->num_thread(); ++j) {
      imbls[i] = std::min(imbls[i],  _t[j][i]);
      tAvgs[i] = std::max(tAvgs[i],  _t[j][i]);
    }
    imbls[i]   = tAvgs[i] - imbls[i];
  }
  // check the imbalance of inner separatly
  imbls[SolverTime::Inner] = _t[1][SolverTime::Inner];
  for (int j=1; j<_ptDd->num_thread(); ++j)
    imbls[SolverTime::Inner] = std::min(_t[j][SolverTime::Inner], imbls[SolverTime::Inner]);
  imbls[SolverTime::Inner] = tAvgs[SolverTime::Inner] - imbls[SolverTime::Inner];
  //
  ValRank.v = tAvgs[SolverTime::Total];
  std::copy(tAvgs, tAvgs+TIMER_MAX_ITEM, tMaxs);
  std::copy(tAvgs, tAvgs+TIMER_MAX_ITEM, tThrdMaxs);

  MPI_Allreduce(MPI_IN_PLACE, imbls, TIMER_MAX_ITEM, MPI_DOUBLE, MPI_MAX, _ptDd->comm());
  MPI_Allreduce(MPI_IN_PLACE, tAvgs, TIMER_MAX_ITEM, MPI_DOUBLE, MPI_SUM, _ptDd->comm());
  MPI_Allreduce(MPI_IN_PLACE, tMaxs, TIMER_MAX_ITEM, MPI_DOUBLE, MPI_MAX, _ptDd->comm());
  for (int i=0; i<TIMER_MAX_ITEM; ++i) 
    tAvgs[i] /= _ptDd->num_proc();

  if (_ptDd->is_root()) {
    std::cout << "Avg:       " << std::fixed << tAvgs[SolverTime::Outer] << "\t"
      << tAvgs[SolverTime::Pack] << "\t" << tAvgs[SolverTime::Comm] << "\t"
      << tAvgs[SolverTime::Inner] << "\t" << tAvgs[SolverTime::Unpack] << "\t"
      << tAvgs[SolverTime::Total] << std::endl;
    std::cout << "Max:       " << std::fixed << tMaxs[SolverTime::Outer] << "\t"
      << tMaxs[SolverTime::Pack] << "\t" << tMaxs[SolverTime::Comm] << "\t"
      << tMaxs[SolverTime::Inner] << "\t" << tMaxs[SolverTime::Unpack] << "\t"
      << tMaxs[SolverTime::Total] << std::endl;
    std::cout << "thrd imbl: " << std::fixed << imbls[SolverTime::Outer] << "\t"
      << imbls[SolverTime::Pack] << "\t" << 0.0 << "\t"
      << imbls[SolverTime::Inner] << "\t" << imbls[SolverTime::Unpack] << "\t"
      << imbls[SolverTime::Total] << std::endl;
  }

  MPI_Allreduce(MPI_IN_PLACE, &ValRank, 1, MPI_DOUBLE_INT, MPI_MAXLOC, _ptDd->comm());
  if (_ptDd->rank() == ValRank.r)
    std::cout << "Max Proc : " << std::fixed << tThrdMaxs[SolverTime::Outer] << "\t"
      << tThrdMaxs[SolverTime::Pack] << "\t" << tThrdMaxs[SolverTime::Comm] << "\t"
      << tThrdMaxs[SolverTime::Inner] << "\t" << tThrdMaxs[SolverTime::Unpack] << "\t"
      << tThrdMaxs[SolverTime::Total] << std::endl;

  return 0;
}
