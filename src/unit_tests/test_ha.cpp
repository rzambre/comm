#include "sys.h"
#include "constants.h"
#include "DomainDecomp.h"
#include "HaloArray.h"

#include <iostream>
#include <mpi.h>
#include <omp.h>

int main(int argc, char* argv[])
{
  sys_init(argc, argv, MPI_THREAD_FUNNELED);

  int  n[3] = {64,64,64}, np[3] = {2, 2, 2}, nThrd = 4, nHalo = 2;
  bool isPrds[3] = {1, 1, 1};

  // create decomp
  DomainDecomp dd(n, np, isPrds, nThrd, MPI_COMM_WORLD, Neighbor::star);
  dd.set_from_option();
  dd.setup();
  // udpdate decomp info, maybe changed by cmd option
  nThrd = dd.num_thread();

  // create ha
  HaloArray ha(dd, nHalo, CommMethod::p2p);

  // init ha
  int sizes[3];
  int iStrd = ha.stride_i();
  int jStrd = ha.stride_j();
  ha.get_size(sizes);
  for (int i=nHalo; i<sizes[0]+nHalo; ++i) {
    for (int j=nHalo; j<sizes[1]+nHalo; ++j) {
      for (int k=nHalo; k<sizes[2]+nHalo; ++k) {
        ha.f[k+j*jStrd+i*iStrd] = 1.0 + dd.rank();
      }
    }
  } 

#pragma omp parallel num_threads(nThrd)
{
  ha.pack_face_halo();
  #pragma omp barrier
  #pragma omp master
  {
    ha.update_face_halo_begin();
    ha.update_face_halo_end();
  }
  #pragma omp barrier
  ha.unpack_face_halo();
}

  // check face values
  int    fNbrs[6], rngs[6];
  double err = 0.0;
  dd.get_info(DecompOpt::faces, fNbrs);
  for (int m=0; m<6; ++m) {
    ha.get_halo_range(m, rngs);
    for (int i=rngs[0]; i<rngs[3]; ++i) {
      for (int j=rngs[1]; j<rngs[4]; ++j) {
        for (int k=rngs[2]; k<rngs[5]; ++k) {
          err += abs(fNbrs[m] + 1.0 - ha.f[i*iStrd+j*jStrd+k]);
        }
      }
    }
  }

  MPI_Allreduce(MPI_IN_PLACE, &err, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  if (dd.is_root())
    std::cout << "Halo Error: " << err << std::endl;

  ha.free_mpi_resouce();

  sys_finalize();

  return 0;
}
