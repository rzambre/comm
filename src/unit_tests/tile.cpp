#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <cmath>
#include <omp.h>

const double PI = 4.0*atan(1.0);

int main(int argc, char* argv[])
{
  int N = 256, nThrds[3] = {1,1,1}, testType = 0, nh = 2, nRun = 64;

  if (argc == 7) {
    N         = atoi(argv[1]);
    nThrds[0] = atoi(argv[2]);
    nThrds[1] = atoi(argv[3]);
    nThrds[2] = atoi(argv[4]);
    testType  = atoi(argv[5]);
    nh        = atoi(argv[6]);
  }

  int    nt = nThrds[0] * nThrds[1] * nThrds[2];
  int    sz = (N+2*nh)*(N+2*nh)*(N+2*nh);
  double *p = new double [sz];
  double *a = new double [sz];
  double *b = new double [sz];
  double h  = 1.0 / N;
  double w0 = 1.0/3.0, w1 = 2.0/3.0, w2 = h*h;

  int cacheLen = 16;
  double *tTotal = new double [nt*cacheLen];
  double *tComp  = new double [nt*cacheLen];
  std::fill(tTotal, tTotal+nt*cacheLen, 0.0);
  std::fill(tComp,  tComp +nt*cacheLen, 0.0);

  std::fill(p, p+sz, 0.0); 
  std::fill(a, a+sz, 0.0); 

  int jStrd = N + 2*nh;
  int iStrd = jStrd * (N + 2*nh);

  for (int i=0; i<N+2*nh; ++i)
    for (int j=0; j<N+2*nh; ++j)
      for (int k=0; k<N+2*nh; ++k)
        b[i*iStrd+j*jStrd+k] = -12.0*PI*PI*sin(2*PI*(i-nh+0.5)*h) * sin(2*PI*(j-nh+0.5)*h) 
                             * sin(2*PI*(k-nh+0.5)*h);

#pragma omp parallel num_threads(nt)
{
  int tid = omp_get_thread_num();
  int kBegin, kEnd, jBegin, jEnd, iBegin, iEnd;

  switch (testType) {
    // baseline
  	case 0:
      iBegin = (N / nThrds[0]) * (tid / nThrds[1]) + nh;
      iEnd   = iBegin + N / nThrds[0];
      jBegin = (N / nThrds[1]) * (tid % nThrds[1]) + nh;
      jEnd   = jBegin + N / nThrds[1];
      tTotal[tid*cacheLen] -= omp_get_wtime();
      for (int m=0; m<nRun; ++m) {
        if (m % 2 == 0) {
          tComp[tid*cacheLen] -= omp_get_wtime();
          for (int i=iBegin; i<iEnd; ++i) {
            for (int j=jBegin; j<jEnd; ++j) {
              #pragma omp simd
              for (int k=nh; k<N+nh; ++k) {
                int ijk   = i * iStrd + j * jStrd + k;
                a[ijk] = w0 * (( p[ijk-1] + p[ijk+1] + p[ijk-jStrd] + p[ijk+jStrd] 
                               + p[ijk-iStrd] + p[ijk+iStrd])/6.0 - w2*b[ijk])
                       + w1 * p[ijk];
              }
            }
          }
          tComp[tid*cacheLen] += omp_get_wtime();
          #pragma omp barrier
        }// end if
        else {
          tComp[tid*cacheLen] -= omp_get_wtime();
          for (int i=iBegin; i<iEnd; ++i) {
            for (int j=jBegin; j<jEnd; ++j) {
              #pragma omp simd
              for (int k=nh; k<N+nh; ++k) {
                int ijk   = i * iStrd + j * jStrd + k;
                p[ijk] = w0 * (( a[ijk-1] + a[ijk+1] + a[ijk-jStrd] + a[ijk+jStrd] 
                               + a[ijk-iStrd] + a[ijk+iStrd])/6.0 - w2*b[ijk])
                       + w1 * a[ijk];
              }
            }
          }
          tComp[tid*cacheLen] += omp_get_wtime();
          #pragma omp barrier
        }// end else
      }// end for m
      tTotal[tid*cacheLen] += omp_get_wtime();
  	break; // case 0

    // fuse
    case 1:
      tTotal[tid*cacheLen] -= omp_get_wtime();
      for (int r=0; r<nRun/nh; ++r) {
        for (int m=0; m<nh; ++m) {
          // index range
          int tj = tid % nThrds[1], ti = tid / nThrds[1];
          jBegin = tj * ((N+2*(nh-m-1)) / nThrds[1]) + m+1 + std::min(tj, (N+2*(nh-m-1))%nThrds[1]);
          jEnd   = jBegin + (N+2*(nh-m-1))/nThrds[1] + (tj < ((N+2*(nh-m-1))%nThrds[1]) ? 1 : 0);
          iBegin = ti * ((N+2*(nh-m-1)) / nThrds[0]) + m+1 + std::min(ti, (N+2*(nh-m-1))%nThrds[0]);
          iEnd   = iBegin + (N+2*(nh-m-1))/nThrds[0] + (ti < ((N+2*(nh-m-1))%nThrds[0]) ? 1 : 0);
          if (m % 2 == 0) {
            tComp[tid*cacheLen] -= omp_get_wtime();
            for (int i=iBegin; i<iEnd; ++i) {
              int ij = i * iStrd + jBegin * jStrd;
              for (int j=jBegin; j<jEnd; ++j) {
                #pragma omp simd
                for (int k=m+1; k<N+2*nh-(m+1); ++k) {
                  int ijk   = ij + k;
                  a[ijk] = w0 * (( p[ijk-1] + p[ijk+1] + p[ijk-jStrd] + p[ijk+jStrd] 
                                 + p[ijk-iStrd] + p[ijk+iStrd])/6.0 - w2*b[ijk])
                         + w1 * p[ijk];
                }
                ij += jStrd;
              }
            }
            tComp[tid*cacheLen] += omp_get_wtime();
            #pragma omp barrier
          }// end if
          else {
            tComp[tid*cacheLen] -= omp_get_wtime();
            for (int i=iBegin; i<iEnd; ++i) {
              int ij = i * iStrd + jBegin * jStrd;
              for (int j=jBegin; j<jEnd; ++j) {
                #pragma omp simd
                for (int k=m+1; k<N+2*nh-(m+1); ++k) {
                  int ijk   = ij + k;
                  p[ijk] = w0 * (( a[ijk-1] + a[ijk+1] + a[ijk-jStrd] + a[ijk+jStrd] 
                                 + a[ijk-iStrd] + a[ijk+iStrd])/6.0 - w2*b[ijk])
                         + w1 * a[ijk];
                }
                ij += jStrd;
              }
            }
            tComp[tid*cacheLen] += omp_get_wtime();
            #pragma omp barrier
          }// end else
        }
      }// end for m
      tTotal[tid*cacheLen] += omp_get_wtime();
    break;

    // wavefront with sync
    case 2:
      tTotal[tid*cacheLen] -= omp_get_wtime();
      for (int r=0; r<nRun/nh; ++r) {
        for (int i=1; i<=N+nh-1+2*(nh-1); ++i) {
          tComp[tid*cacheLen] -= omp_get_wtime();
          for (int m=0; m<nh; ++m) {
            if (i-2*m >= m+1 && i-2*m <= N+2*nh-m-2) {// if i fit
              int ii   = (i-2*m-1) % 4 + (2*m/nh)*4; // plane in a
              if (m%2 == 0) { // p -> a
                #pragma omp for nowait
                for (int j=m+1; j<N+2*nh-(m+1); ++j) {
                  int ij  = (i-2*m)*iStrd + j * jStrd;
                  int iij = ii*iStrd + j*jStrd;
                  #pragma omp simd
                  for (int k=m+1; k<N+2*nh-(m+1); ++k) {
                    int ijk = ij + k;
                    a[iij+k] = w0 * (( p[ijk-1] + p[ijk+1] + p[ijk-jStrd] + p[ijk+jStrd] 
                                     + p[ijk-iStrd] + p[ijk+iStrd])/6.0 - w2*b[ijk])
                             + w1 * p[ijk];

                  }// end for k
                }// end for j
              }
              else {// a -> p
                int iim1 = (i-2*m+2) % 4 + (2*m/nh)*4;
                int iip1 = (i-2*m)   % 4 + (2*m/nh)*4;
                #pragma omp for nowait
                for (int j=m+1; j<N+2*nh-(m+1); ++j) {
                  int ij    = (i-2*m) * iStrd + j * jStrd;
                  int iij   =   ii * iStrd + j * jStrd;
                  int iim1j = iim1 * iStrd + j * jStrd;
                  int iip1j = iip1 * iStrd + j * jStrd;
                  #pragma omp simd
                  for (int k=m+1; k<N+2*nh-(m+1); ++k) {
                    int iijk = iij + k;
                    p[ij+k] = w0 * (( a[iijk-1] + a[iijk+1] + a[iijk-jStrd] + a[iijk+jStrd] 
                                    + a[iim1j+k] + a[iip1j+k])/6.0 - w2*b[ij+k])
                            + w1 * a[iijk];

                  }// end for k
                }// end else
              }// end else
            }// end if i fit
          }// end for m
          tComp[tid*cacheLen] += omp_get_wtime();
          #pragma omp barrier
        }// end for i
      }//end for r
      tTotal[tid*cacheLen] += omp_get_wtime();
    break;

    // thread separate wavefornt
    case 3:
      kBegin = (N / nThrds[2]) * (tid % nThrds[2]) + nh;
      kEnd   = kBegin + N / nThrds[2];
      jBegin = (N / nThrds[1]) * (tid / nThrds[2]) + nh;
      jEnd   = jBegin + N / nThrds[1];
      int jStrdL = kEnd - kBegin + 2*nh;
      int iStrdL = jStrdL * (jEnd - jBegin + 2*nh);
      double *c  = new double [3*(nh-1) * iStrdL];

      tTotal[tid*cacheLen] -= omp_get_wtime();
      for (int r=0; r<nRun/nh; ++r) {
        tComp[tid*cacheLen] -= omp_get_wtime();
        for (int i=1; i<=N+2*nh-2; ++i) {
          // p -> c
          int ij  = i * iStrd + (jBegin-nh+1) * jStrd;
          int ijL = (i-1)%3 * iStrdL + jStrdL;
          for (int j=jBegin-nh+1; j<jEnd+nh-1; ++j) {
            #pragma omp simd
            for (int k=kBegin-nh+1; k<kEnd+nh-1; ++k) {
              int ijk  = ij + k;
              c[ijL+k-kBegin+nh]
                = w0 * (( p[ijk-1] + p[ijk+1] + p[ijk-jStrd] + p[ijk+jStrd] 
                        + p[ijk-iStrd] + p[ijk+iStrd])/6.0 - w2*b[ijk])
                + w1 * p[ijk];
            }
            ij  += jStrd;
            ijL += jStrdL;
          }
          // c -> c
          for (int m=1; m<nh-1; ++m) {
            if (i-m >= m+1 && i-m <= N+2*nh-m-2) {
              int ijL   = ((i-m-1)%3 + (m-1)*3) * iStrdL + (m+1)*jStrdL;
              int im1jL = ((i-m+1)%3 + (m-1)*3) * iStrdL + (m+1)*jStrdL;
              int ip1jL = ((i-m  )%3 + (m-1)*3) * iStrdL + (m+1)*jStrdL;
              int ijLTo = ijL + 3*iStrdL;
              int ij    = (i-m)*iStrd + (jBegin-nh+m+1)*jStrd;
              for (int j=m+1; j<jEnd-jBegin+2*nh-(m+1); ++j) {
                #pragma omp simd
                for (int k=m+1; k<kEnd-kBegin+2*nh-(m+1); ++k) {
                  int ijkL = ijL + k;
                  c[ijLTo+k] = w0 * (( c[ijkL-1] + c[ijkL+1] + c[ijkL-jStrdL] + c[ijkL+jStrdL] 
                                     + c[im1jL+k] + c[ip1jL+k])/6.0 - w2*b[ij+k+kBegin-nh])
                             + w1 * c[ijkL];
                }// end for k
                ijL   += jStrdL;
                im1jL += jStrdL;
                ip1jL += jStrdL;
                ijLTo += jStrdL;
                ij    += jStrd;
              }// end for j
            }// end if 
          }// end for m
          // c -> a
          if (i-nh+1 >= nh && i-nh+1 <= N+nh-1) {
            int ij    = (i-nh+1)*iStrd + jBegin*jStrd;
            int ijL   = ((i-nh  )%3 + (nh-2)*3)*iStrdL + nh*jStrdL;
            int im1jL = ((i-nh+2)%3 + (nh-2)*3)*iStrdL + nh*jStrdL;
            int ip1jL = ((i-nh+1)%3 + (nh-2)*3)*iStrdL + nh*jStrdL;
            for (int j=nh; j<jEnd-jBegin+nh; ++j) {
              #pragma omp simd
              for (int k=nh; k<kEnd-kBegin+nh; ++k) {
                int ijkL = ijL + k;
                int ijk  = ij  + k + kBegin - nh;
                a[ijk] = w0 * (( c[ijkL-1] + c[ijkL+1] + c[ijkL-jStrdL] + c[ijkL+jStrdL]
                               + c[im1jL+k] + c[ip1jL+k])/6.0 - w2*b[ijk])
                       + w1 * c[ijkL];
              }// end for k
              ij    += jStrd;
              ijL   += jStrdL;
              im1jL += jStrdL;
              ip1jL += jStrdL;
            }// end for j
          }// end if
        }// end for i
        tComp[tid*cacheLen] += omp_get_wtime();
        #pragma omp barrier
        #pragma omp single
        std::swap(p, a);
      }// end for r
      tTotal[tid*cacheLen] += omp_get_wtime();

      delete [] c;
    break; // case 3
    }
}

  double tmax[2] = {0.0, 0.0}, tavg[2] = {0.0, 0.0}, tmin[2] = {10.0, 10.0};
  for (int i=0; i<nt; ++i) {
    tmax[0] = std::max(tComp[i*cacheLen], tmax[0]);
    tmax[1] = std::max(tTotal[i*cacheLen], tmax[1]);
    tavg[0] += tComp[i*cacheLen];
    tavg[1] += tTotal[i*cacheLen];
    tmin[0] = std::min(tComp[i*cacheLen], tmin[0]);
    tmin[1] = std::min(tTotal[i*cacheLen], tmin[1]);
  }
  tavg[0] /= nt;
  tavg[1] /= nt;
  std::cout << "Comp " << tavg[0] << " " << tmax[0] << " " << tmin[0] << std::endl; 
  std::cout << "Total " << tavg[1] << " " << tmax[1] << " " << tmin[1] << std::endl; 

  double avg = 0.0;
  for (int i=nh; i<N+nh; ++i)
    for (int j=nh; j<N+nh; ++j)
      for (int k=nh; k<N+nh; ++k)
        avg += fabs(p[i*iStrd + j*jStrd + k]);
  avg = avg/N/N/N;
  std::cout << "avg p: " << avg << std::endl;

  delete [] p;
  delete [] a;
  delete [] b;
  delete [] tTotal;
  delete [] tComp;

  return 0;
}
