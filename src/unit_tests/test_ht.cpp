#include "sys.h"
#include "constants.h"
#include "DomainDecomp.h"
#include "HaloTransfer.h"

#include <iostream>
#include <algorithm>
#include <mpi.h>
#include <omp.h>

using namespace std;

int main(int argc, char* argv[])
{
  sys_init(argc, argv, MPI_THREAD_FUNNELED);

  int  n[3] = {64,64,64}, np[3] = {2, 2, 2}, nThrd = 4, nHalo = 2;
  bool isPrds[3] = {1, 1, 1}, isSet;

  // create decomp
  DomainDecomp dd(n, np, isPrds, nThrd, MPI_COMM_WORLD, Neighbor::star);
  dd.set_from_option();
  dd.setup();
  // udpdate decomp info, maybe changed by cmd option
  nThrd = dd.num_thread();

  // halo stroe
  HaloStore hs(dd, nHalo, 1);
  hs.set_from_option();
  if (dd.is_neighbor_star())  
    hs.setup_star();
  else
    hs.setup_box();
  hs.debug_view();

  // create ha
  int szs[3];
  dd.get_local_size(szs);
  double **d = new double* [hs.nVar];
  for (int i=0; i<hs.nVar; ++i) 
    d[i] = new double [(szs[0]+2*nHalo)*(szs[1]+2*nHalo)*(szs[2]+2*nHalo)];

  // init ha
  int jStrd = szs[2] + 2*nHalo;
  int iStrd = jStrd * (szs[1] + 2*nHalo);
  for (int v=0; v<hs.nVar; ++v)
  for (int i=nHalo; i<szs[0]+nHalo; ++i)
    for (int j=nHalo; j<szs[1]+nHalo; ++j)
      for (int k=nHalo; k<szs[2]+nHalo; ++k)
        d[v][k+j*jStrd+i*iStrd] = 1.0 + v + dd.rank();

  // halo transfer facility
  int nPipe = hs.num_pipe();
  HaloTransfer* hts = new HaloTransfer [nPipe];
  for (int i=0; i<nPipe; ++i) hts[i].init(&dd, nHalo);
  if (nPipe == 1) {
    if (dd.is_neighbor_star())
      hts[0].set_halo_range(hs, 0, 6, nThrd);
    else
      hts[0].set_halo_range(hs, 0, 26, nThrd);
  }
  else { // nPile > 1
    for (int p=0; p<nPipe; ++p) {
      hts[p].setup_pack_pipeline(p, hs, nThrd-1);
    } for (int p=0; p<nPipe-1; ++p) {
      hts[p].setup_unpack_pipeline(p, hs, nThrd-1);
    }
  }// end if

  for (int i=0; i<nPipe; ++i) hts[i].debug_view();

  // vs marks the variable used in comm, in current test all are used
  vector<int> vs;
  for (int i=0; i<hs.nVar; ++i) vs.push_back(i);

  // test
  if (nPipe == 1) {
  #pragma omp parallel num_threads(nThrd)
  {
    int tid = omp_get_thread_num();
    hts[0].pack_halo(d, vs, hs);
    #pragma omp barrier
    if (tid == nThrd-1) hts[0].update_halo(hs);
    #pragma omp barrier
    hts[0].unpack_halo(d, vs, hs);
    #pragma omp barrier
  }// end parallel
  }// end if
  else { // nPipe > 1
    int szs[3];
    dd.get_global_size(szs);
    int bufSize = 2*nHalo*(szs[0]*szs[1] + szs[1]*szs[2] + szs[2]*szs[0]);
  #pragma omp parallel num_threads(nThrd)
  {
    int tid = omp_get_thread_num();
//    for (int i=0; i<nPipe; ++i) hts[i].pack_subhalo(d, vs, hs);
//    #pragma omp barrier
//    if (tid == nThrd-1)
//      for (int i=0; i<nPipe; ++i) hts[i].update_halo(hs);
//    #pragma omp barrier
//    for (int i=0; i<nPipe-1; ++i) hts[i].unpack_subhalo(d, vs, hs);
    for (int i=0; i<nPipe; ++i) {
      if (tid == nThrd-1) {
        if (i >= 1) hts[i-1].update_halo(hs);
      }
      else {
        hts[i].pack_subhalo(d, vs, hs);
      }
      #pragma omp barrier
    }
    if (tid == nThrd-1) hts[nPipe-1].update_halo(hs);
    #pragma omp barrier
    for (int i=0; i<nPipe; ++i) {
      if (tid < nThrd-1) hts[i].unpack_subhalo(d, vs, hs);
    }
  }// end parallel
  }// end if

  // check face values
  int    rngs[6];
  double err = 0.0;

  for (unsigned int c=0; c<hs.chunks.size(); c+=6)
    for (int v=0; v<hs.nVar; ++v)
      for (int i=hs.chunks[c]; i<hs.chunks[c+3]; ++i)
        for (int j=hs.chunks[c+1]; j<hs.chunks[c+4]; ++j)
          for (int k=hs.chunks[c+2]; k<hs.chunks[c+5]; ++k) {
            err += fabs(hs.toRanks[c/6]+1.0 + v - d[v][i*iStrd+j*jStrd+k]);
//            if (dd.is_root() && fabs(hs.toRanks[c/6]+1.0 + v - d[v][i*iStrd+j*jStrd+k])>0.0)
//              std::cout << i << " " << j << " " << k << " "  << v << " "
//                        << hs.toRanks[c/6]+1.0 + v << " " << d[v][i*iStrd+j*jStrd+k] << "\n";
          }

  MPI_Allreduce(MPI_IN_PLACE, &err, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  if (dd.is_root())  std::cout << "Halo Error: " << err << std::endl;

  for (int i=0; i<hs.nVar; ++i) delete [] d[i];
  delete [] d;

  sys_finalize();

  return 0;
}
