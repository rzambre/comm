#include "sys.h"
#include "constants.h"
#include "DomainDecomp.h"
#include "HaloArray.h"
#include "HaloTransfer.h"

#include <iostream>
#include <algorithm>
#include <mpi.h>
#include <omp.h>

int main(int argc, char* argv[])
{
  sys_init(argc, argv, MPI_THREAD_FUNNELED);

  int  n[3] = {64,64,64}, np[3] = {2, 2, 2}, nThrd = 4, nHalo = 2, nRun = 32;
  bool isPrds[3] = {1, 1, 1};

  // create decomp
  DomainDecomp dd(n, np, isPrds, nThrd, MPI_COMM_WORLD, Neighbor::star);
  dd.set_from_option();
  dd.setup();
  // udpdate decomp info, maybe changed by cmd option
  nThrd = dd.num_thread();

  // create ha
  HaloArray ha(dd, nHalo, CommMethod::p2p);

  // init ha
  int sizes[3];
  int iStrd = ha.stride_i();
  int jStrd = ha.stride_j();
  ha.get_size(sizes);
  for (int i=nHalo; i<sizes[0]+nHalo; ++i) {
    for (int j=nHalo; j<sizes[1]+nHalo; ++j) {
      for (int k=nHalo; k<sizes[2]+nHalo; ++k) {
        ha.f[k+j*jStrd+i*iStrd] = 1.0 + dd.rank();
      }
    }
  } 

  // halo transfer facility
  HaloTransfer ht(dd, nHalo);
  //ht.debug_view();

  // arrays to save time
  int CacheSize = 8;
  double *tPack0   = new double [nThrd*CacheSize];
  double *tPack1   = new double [nThrd*CacheSize];
  double *tUnpack0 = new double [nThrd*CacheSize];
  double *tUnpack1 = new double [nThrd*CacheSize];
  std::fill(tPack0,   tPack0+nThrd*CacheSize, 0.0);
  std::fill(tPack1,   tPack1+nThrd*CacheSize, 0.0);
  std::fill(tUnpack0, tUnpack0+nThrd*CacheSize, 0.0);
  std::fill(tUnpack1, tUnpack1+nThrd*CacheSize, 0.0);

#pragma omp parallel num_threads(nThrd)
  {
    int tid = omp_get_thread_num();
    for (int i=0; i<nRun; ++i) {
      tPack0[tid*CacheSize] -= omp_get_wtime();
      ha.pack_face_halo();
      tPack0[tid*CacheSize] += omp_get_wtime();
      #pragma omp barrier
    //  #pragma omp master
    //  ha.update_face_halo();
      #pragma omp barrier
      tUnpack0[tid*CacheSize] -= omp_get_wtime();
      ha.unpack_face_halo();
      tUnpack0[tid*CacheSize] += omp_get_wtime();
    }
  }


#pragma omp parallel num_threads(nThrd)
  {
    int tid = omp_get_thread_num();
    for (int i=0; i<nRun; ++i) {
      tPack1[tid*CacheSize] -= omp_get_wtime();
      ht.pack_halo(ha);
      tPack1[tid*CacheSize] += omp_get_wtime();
      #pragma omp barrier
    //  #pragma omp master
    //  ht.update_halo();
      #pragma omp barrier
      tUnpack1[tid*CacheSize] -= omp_get_wtime();
      ht.unpack_halo(ha);
      tUnpack1[tid*CacheSize] += omp_get_wtime();
    }
  }

  // check face values
  int    rngs[6];
  double err = 0.0;

  if (dd.is_neighbor_star()) {
    Halo faces[] = {FaceXm, FaceYm, FaceZm, FaceXp, FaceYp, FaceZp};
    for (int m=0; m<6; ++m) {
      ht.get_halo_chunk_range(faces[m], rngs);
      for (int i=rngs[0]; i<rngs[3]; ++i) {
        for (int j=rngs[1]; j<rngs[4]; ++j) {
          for (int k=rngs[2]; k<rngs[5]; ++k) {
            err += abs(dd.neighbor((int)faces[m]) + 1.0 - ha.f[i*iStrd+j*jStrd+k]);
          }
        }
      }
    }//end for m
  }//end if star
  else {
    for (int m=0; m<26; ++m) {
      ht.get_halo_chunk_range(static_cast<Halo>(m), rngs);
      for (int i=rngs[0]; i<rngs[3]; ++i) {
        for (int j=rngs[1]; j<rngs[4]; ++j) {
          for (int k=rngs[2]; k<rngs[5]; ++k) {
            err += abs(dd.neighbor(m) + 1.0 - ha.f[i*iStrd+j*jStrd+k]);
          }
        }
      }
    }//end for m
  }

 std::cout << "max pack0: " << *std::max_element(tPack0, tPack0+nThrd*CacheSize) << std::endl;
 std::cout << "max pack1: " << *std::max_element(tPack1, tPack1+nThrd*CacheSize) << std::endl;
 std::cout << "max unpack0: " << *std::max_element(tUnpack0, tUnpack0+nThrd*CacheSize) << std::endl;
 std::cout << "max unpack1: " << *std::max_element(tUnpack1, tUnpack1+nThrd*CacheSize) << std::endl;

 for (int i=0; i<nThrd; ++i)
   std::cout << tPack0[i*CacheSize] << "\t" << tPack1[i*CacheSize] << "\t" 
             << tUnpack0[i*CacheSize] << "\t" << tUnpack1[i*CacheSize] << std::endl;

  sys_finalize();

  return 0;
}
