#include "sys.h"
#include "constants.h"
#include "DomainDecomp.h"
#include "HaloArray.h"
#include "Smoother.h"
#include "SORRB.h"

#include <iostream>
#include <mpi.h>
#include <omp.h>

int main(int argc, char* argv[])
{
  sys_init(argc, argv, MPI_THREAD_FUNNELED);

  int  n[3] = {64,64,64}, np[3] = {2, 2, 2}, nThrd = 4, nHalo = 2;
  bool isPrds[3] = {1, 1, 1};

  // decomp
  DomainDecomp dd(n, np, isPrds, nThrd, MPI_COMM_WORLD, Neighbor::star);
  dd.set_from_option();
  dd.setup();

  // mesh
  UniMesh mesh(dd);
  mesh.view();

  // halo array
  HaloArray p(dd, nHalo, CommMethod::p2p);
  int rngs[6];
  p.get_local_range(rngs);

  //solver setup range
  SORRB smoother(dd, mesh);
  smoother.setup_overlap_range(rngs);
  smoother.setup_funneled_inner_range();
  smoother.setup_funneled_outer_range();
  smoother.debug_view();

  sys_finalize();
  return 0;
}
