add_executable(test_decomp.exe test_decomp.cpp)
target_link_libraries(test_decomp.exe solver)

#add_executable(test_ha.exe test_ha.cpp)
#target_link_libraries(test_ha.exe solver)

#add_executable(test_funneled_range.exe test_funneled_range.cpp)
#target_link_libraries(test_funneled_range.exe solver)

add_executable(test_ht.exe test_ht.cpp)
target_link_libraries(test_ht.exe solver)

#add_executable(perf_copy.exe perf_copy.cpp)
#target_link_libraries(perf_copy.exe solver)
