#include "sys.h"
#include "DomainDecomp.h"
#include "WJSolver.h"

#include <iostream>
#include <cmath>
#include <mpi.h>
#include <omp.h>

int main(int argc, char* argv[])
{
  sys_init(argc, argv, MPI_THREAD_FUNNELED);

  int  n[3] = {64,64,64}, np[3] = {2, 2, 2}, nThrd = 4, nHalo = 2;
  bool isPrds[3] = {1, 1, 1};
  int  nIter = 32;
  bool isSet;

  get_option("niter", CmdOption::Int, 1, &nIter, &isSet);
  get_option("nhalo", CmdOption::Int, 1, &nHalo, &isSet);

  // decomp
  DomainDecomp dd(n, np, isPrds, nThrd, MPI_COMM_WORLD, Neighbor::star);
  dd.set_from_option();
  dd.setup();
  // udpdate decomp info, maybe changed by cmd option
  nThrd = dd.num_thread();

  // mesh
  UniMesh mesh(dd);
  mesh.view();

  // solver
  WJSolver sol(dd, mesh, nHalo);
  sol.set_from_option();
  sol.setup();
  sol.init();

  // save results
  int sizes[3];
  dd.get_local_size(sizes);
  double *p0 = new double [(sizes[0]+2*nHalo)*(sizes[1]+2*nHalo)*(sizes[2]+2*nHalo)];
  double *p1 = new double [(sizes[0]+2*nHalo)*(sizes[1]+2*nHalo)*(sizes[2]+2*nHalo)];

  // solver with hybrid
  sol.setup_parallel_mode(SolverMode::Funneled);
  MPI_Barrier(MPI_COMM_WORLD);
  #pragma omp parallel num_threads(nThrd)
    sol.solve(nIter);
  sol.copy_var(0, p0);
  sol.nullify();

  // solve with another
  sol.setup_parallel_mode(SolverMode::DeepHaloTile);
  MPI_Barrier(MPI_COMM_WORLD);
  #pragma omp parallel num_threads(nThrd)
    sol.solve(nIter);
  sol.copy_var(0, p1);

  // compare the value from two models
  double errl0 = 0.0, errl1 = 0.0;
  int    jStrd = sizes[2] + 2*nHalo, iStrd = jStrd * (sizes[1]+2*nHalo);
  for (int i=nHalo; i<sizes[0]+nHalo; ++i) {
    for (int j=nHalo; j<sizes[1]+nHalo; ++j) {
      for (int k=nHalo; k<sizes[2]+nHalo; ++k) {
        errl0  = std::max(fabs(p0[i*iStrd+j*jStrd+k] - p1[i*iStrd+j*jStrd+k]), errl0);
        errl1 += fabs(p0[i*iStrd+j*jStrd+k] - p1[i*iStrd+j*jStrd+k]);
//        if (fabs(p0[i*iStrd+j*jStrd+k] - p1[i*iStrd+j*jStrd+k]) > 1.0e-8 && dd.is_root())
//          std::cout << i << " " << j << " " << k << " "
//            << p0[i*iStrd+j*jStrd+k] << " " << p1[i*iStrd+j*jStrd+k] << std::endl;
      }
    }
  }
  MPI_Allreduce(MPI_IN_PLACE, &errl0, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE, &errl1, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  if (dd.is_root())
    std::cout << "Error: " << errl0 << " " << errl1 << std::endl;

  delete [] p0; 
  delete [] p1; 

  sys_finalize();
  return 0;
}
