#include "sys.h"
#include "constants.h"
#include "DomainDecomp.h"
#include "Solver.h"
#include "WJSolver.h"

#include <iostream>
#include <mpi.h>
#include <omp.h>

#ifdef PAPI
#include <papi.h>
#endif

int main(int argc, char* argv[])
{
  sys_init(argc, argv, MPI_THREAD_FUNNELED);

  int  n[3] = {64,64,64}, np[3] = {2, 2, 2}, nThrd = 4, nHalo = 2;
  bool isPrds[3] = {1, 1, 1};
  int  nIter = 16, nWarm = 32;
  bool isSet;
  int  testType = 0;

  get_option("niter", CmdOption::Int, 1, &nIter,    &isSet);
  get_option("nwarm", CmdOption::Int, 1, &nWarm,    &isSet);
  get_option("test",  CmdOption::Int, 1, &testType, &isSet);
  get_option("nhalo", CmdOption::Int, 1, &nHalo,    &isSet);

#ifdef PAPI
  int ierr;
  ierr = PAPI_library_init(PAPI_VER_CURRENT);
  if (ierr != PAPI_VER_CURRENT) {
    std::cout << "ERROR: init lib" << std::endl;
    exit(-1);
  }
  if (testType != 0) {
    ierr = PAPI_thread_init((unsigned long(*)(void))(omp_get_thread_num)); 
    test_fail(ierr, "thread init");
  }
#endif

  // decomp
  DomainDecomp dd(n, np, isPrds, nThrd, MPI_COMM_WORLD, Neighbor::star);
  dd.set_from_option();
  dd.setup();
  // udpdate decomp info, maybe changed by cmd option
  nThrd = dd.num_thread();

  // mesh
  UniMesh mesh(dd);
  mesh.view();

  // solver
  WJSolver sol(dd, mesh, nHalo);
  sol.set_from_option();
  sol.setup();
  sol.init();

  if (testType == 0) {
    sol.setup_parallel_mode(SolverMode::MPI_BSP);
    sol.solve(nWarm);
    sol .clear_history();
    MPI_Barrier(MPI_COMM_WORLD);
    sol.solve(nIter);
    sol.analyze_time();
  }
  else {
    sol.setup_parallel_mode(static_cast<SolverMode>(testType));
    #pragma omp parallel num_threads(nThrd)
    {
      sol.solve(nWarm);
      sol.clear_history();
      #pragma omp single 
        MPI_Barrier(MPI_COMM_WORLD);
      sol.solve(nIter);
    }
    sol.analyze_time();
  }

  // some dummy computation, just to fool the compiler that p is useful
  int szs[3];
  dd.get_local_size(szs);
  int jStrd = szs[2] + 2*nHalo, iStrd = jStrd * (szs[1] + 2*nHalo);
  double *p = new double [(szs[0]+2*nHalo) * (szs[1]+2*nHalo) * (szs[2]+2*nHalo)];
  sol.copy_var(0, p);
  //
  double err = 0.0;
  for (int i=nHalo; i<szs[0]+nHalo; ++i)
    for (int j=nHalo; j<szs[1]+nHalo; ++j)
      for (int k=nHalo; k<szs[2]+nHalo; ++k)
        err += fabs(p[i*iStrd+j*jStrd+k]);

  MPI_Allreduce(MPI_IN_PLACE, &err, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  if (dd.is_root())
    std::cout << "sum: " << err << std::endl;

  delete [] p;

  sys_finalize();
  return 0;
}
