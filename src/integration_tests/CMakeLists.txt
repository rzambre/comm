add_executable(test_solver.exe test_solver.cpp)
target_link_libraries(test_solver.exe solver)
#
#add_executable(perf_bsp_busywait.exe perf_bsp_busywait.cpp)
#target_link_libraries(perf_bsp_busywait.exe solver)

add_executable(perf_solver.exe perf_solver.cpp)
target_link_libraries(perf_solver.exe solver)
