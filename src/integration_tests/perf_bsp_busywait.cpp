#include "sys.h"
#include "constants.h"
#include "DomainDecomp.h"
#include "HaloArray.h"
#include "Utils.h"
#include "Timer.h"

#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <cstdlib>
#include <ctime>
#include <mpi.h>
#include <omp.h>

int main(int argc, char* argv[])
{
  sys_init(argc, argv, MPI_THREAD_FUNNELED);

  int         n[3] = {64,64,64}, np[3] = {2, 2, 2}, nThrd = 4, nHalo = 2;
  bool        isPrds[3] = {1, 1, 1}, isSet, isSync = false;
  int         nIter = 16, testType = 0, imblncRatio = 0;
  double      tWait = 0.001;
  std::string fTime, fImblnc;

  get_option("niter",     CmdOption::Int,    1, &nIter,       &isSet);
  get_option("test",      CmdOption::Int,    1, &testType,    &isSet);
  get_option("twait",     CmdOption::Double, 1, &tWait,       &isSet);
  get_option("imbalance", CmdOption::Int,    1, &imblncRatio, &isSet);
  get_option("ftime",     CmdOption::Str,    1, &fTime,       &isSet);
  get_option("fimbalance", CmdOption::Str,  1, &fImblnc,     &isSet);
  get_option("sync",      CmdOption::Bool,  1, &isSync,     &isSet);

  // decomp
  DomainDecomp dd(n, np, isPrds, nThrd, MPI_COMM_WORLD, Neighbor::star);
  dd.set_from_option();
  dd.setup();
  // udpdate decomp info, maybe changed by cmd option
  nThrd = dd.num_thread();
  int nProc = dd.num_proc();

  HaloArray p(dd, nHalo, CommMethod::p2p);

  Timer timer(nThrd);

  // set up computation time, tWait * (1-imblance, 1+imbalance)
  // output file to contain imbalance percent of all procs
  srand(time(NULL) + dd.rank());
  int percent = rand() % (2*imblncRatio) - imblncRatio;
  if (imblncRatio > 0) {
    tWait *= 1.0 + 0.01 * percent;
    int *imblncs = new int [nProc];
    MPI_Gather(&percent, 1, MPI_INT, imblncs, nProc, MPI_INT, 0, MPI_COMM_WORLD);
    if (dd.is_root()) {
      std::ofstream fout(fImblnc);
      for (int i=0; i<nProc; ++i) fout << imblncs[i] << std::endl;
      fout.close();      
    }
  }

  switch (testType) {
    // flat mpi, one process per core
    case 0:
      for (int m=0; m<nIter; ++m) {
        timer.mark("comp");
        busy_wait(tWait);
        if (isSync)  MPI_Barrier(MPI_COMM_WORLD);
        timer.mark("comm");
        p.update_face_halo();
        timer.mark("total");
      }
      break;
    // MPI+OpenMP, master thread do the comp and comm
    // 6 messages in serial
    case 1:
    #pragma omp parallel num_threads(nThrd)
    {
      for (int m=0; m<nIter; ++m) {
        #pragma omp master
        {
          timer.mark("comp");
          busy_wait(tWait);
          timer.mark("comm");
          p.update_face_halo();
          timer.mark("total");
        }
      }
    }// end omp parallel
      break;
    case 2:
    #pragma omp parallel num_threads(nThrd)
    {
      int tid = omp_get_thread_num();
      for (int m=0; m<nIter; ++m) {
        timer.mark(tid, "comp");
        busy_wait(tWait);
        #pragma omp master
        { if (isSync) MPI_Barrier(MPI_COMM_WORLD); }
        #pragma omp barrier
        #pragma omp master
        {
          timer.mark("comm");
          p.update_face_halo();
          timer.mark("comm");
        }
        #pragma omp barrier
        timer.mark(tid, "total");
      }// end for
    }// end omp parallel
      break;
  }//end swtich

  // output communication duration, communication start times' different
  // and total time
  Times tCommAvgs(nIter, 0.0),  tCommMins(nIter, 0.0),  tCommMaxs(nIter, 0.0);
  Times tTotalAvgs(nIter, 0.0), tTotalMins(nIter, 0.0), tTotalMaxs(nIter, 0.0);
  Times tpCommMins(nIter, 0.0), tpCommMaxs(nIter, 0.0), commStartDiffs(nIter, 0.0);
  Times thrdImblnc(nIter, 0.0);
  if (testType == 0 || testType == 1) {
    // communication duration
    for (int i=0; i<nIter; ++i)  tCommAvgs[i]  = timer["total"][i] - timer["comm"][i];
    MPI_Allreduce(&tCommAvgs[0], &tCommMins[0], nIter, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
    MPI_Allreduce(&tCommAvgs[0], &tCommMaxs[0], nIter, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    MPI_Allreduce(MPI_IN_PLACE,  &tCommAvgs[0], nIter, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    for (int i=0; i<nIter; ++i)  tCommAvgs[i]  /= nProc;
    // total duration
    for (int i=0; i<nIter; ++i)  tTotalAvgs[i] = timer["total"][i] - timer["comp"][i];
    MPI_Allreduce(&tTotalAvgs[0], &tTotalMins[0], nIter, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
    MPI_Allreduce(&tTotalAvgs[0], &tTotalMaxs[0], nIter, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    MPI_Allreduce(MPI_IN_PLACE,   &tTotalAvgs[0], nIter, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    for (int i=0; i<nIter; ++i)  tTotalAvgs[i]  /= nProc;
    // start time difference
    MPI_Allreduce(&timer["comm"][0], &tpCommMins[0], nIter, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
    MPI_Allreduce(&timer["comm"][0], &tpCommMaxs[0], nIter, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    for (int i=0; i<nIter; ++i)  commStartDiffs[i]  = tpCommMaxs[i] - tpCommMins[i];
  }
  else if (testType == 2) {
    // communication duration
    for (int i=0; i<nIter; ++i)  tCommAvgs[i] = timer["comm"][2*i+1] - timer["comm"][2*i];
    MPI_Allreduce(&tCommAvgs[0], &tCommMins[0], nIter, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
    MPI_Allreduce(&tCommAvgs[0], &tCommMaxs[0], nIter, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    MPI_Allreduce(MPI_IN_PLACE,  &tCommAvgs[0], nIter, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    for (int i=0; i<nIter; ++i)  tCommAvgs[i]  /= nProc;
    // total duration
    /// avg among threads
    for (int i=0; i<nIter; ++i) {
        tTotalMins[i] = 10000.0;
      for (int j=0; j<nThrd; ++j) {
        tTotalAvgs[i] += timer.at(j, "total", i) - timer.at(j, "comp", i);
        tTotalMaxs[i]  = std::max(tTotalMaxs[i], timer.at(j, "total", i) - timer.at(j, "comp", i));
        tTotalMins[i]  = std::min(tTotalMins[i], timer.at(j, "total", i) - timer.at(j, "comp", i));
      }
      thrdImblnc[i]  = tTotalMaxs[i] - tTotalMins[i];
      tTotalAvgs[i] /= nThrd;
    }
    /// collect among processes
    MPI_Allreduce(&tTotalAvgs[0], &tTotalMins[0], nIter, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
    MPI_Allreduce(&tTotalAvgs[0], &tTotalMaxs[0], nIter, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    MPI_Allreduce(MPI_IN_PLACE,   &tTotalAvgs[0], nIter, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(MPI_IN_PLACE,   &thrdImblnc[0], nIter, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    for (int i=0; i<nIter; ++i)  tTotalAvgs[i]  /= nProc;
    for (int i=0; i<nIter; ++i)  thrdImblnc[i]  /= tTotalAvgs[i];
    // start time difference
    // At first, use commstartDiffs to store the tpoint where communication starts
    for (int i=0; i<nIter; ++i)  commStartDiffs[i] = timer["comm"][2*i];
    MPI_Allreduce(&commStartDiffs[0], &tpCommMins[0], nIter, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
    MPI_Allreduce(&commStartDiffs[0], &tpCommMaxs[0], nIter, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    for (int i=0; i<nIter; ++i)  commStartDiffs[i]  = tpCommMaxs[i] - tpCommMins[i];
  }

  // output comp, comm time
  if (dd.is_root()) {
    std::ofstream fout(fTime);
    fout.precision(12);
    for (int i=0; i<nIter; ++i)
      fout << std::fixed
           << tCommAvgs[i]  << "\t" << tCommMins[i]  << "\t" << tCommMaxs[i]  << "\t"
           << tTotalAvgs[i] << "\t" << tTotalMins[i] << "\t" << tTotalMaxs[i] << "\t"
           << thrdImblnc[i] << "\t" << commStartDiffs[i] << std::endl;
    fout.close();
  }

  sys_finalize();
  return 0;
}
